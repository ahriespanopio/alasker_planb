MyFirstApp::Application.routes.draw do
  devise_for :users, :controllers => { :registrations => 'registrations' }

  get "admin/retrainclassifier"
  get "admin/makecategories"
  get "admin/makesituations"
  get "admin/index"
  get "admin/makesomeplanswithstats"
  get "admin/buildclusterlist"

  get "categories/index"
  get "categories/show"
  get "categories/edit"
  get "categories/create"
  get "categories/update"
  get "categories/destroy"

  get "help/index"
  get "help/about"
  get "help/terms"
  get "help/privacy"

  # get '/chatroom' => 'chats#room', :as => :chat
  # post '/new_message' => 'chats#new_message', :as => :new_message

  match '/help' => 'help#index'

  resources :mobile_session, :only => [:create, :destroy]
  match '/mobile/login' => 'mobile_session#create'
  match '/mobile/logout' => 'mobile_session#destroy'

  match '/auth/:provider/callback' => 'authentications#create'

  match "/auth/failure" => redirect("/")

  resources :me, :only => [:index, :show]
  match '/me/test' => 'me#test'
  match 'me/follow/:id' => 'me#follow'
  match 'me/unfollow/:id' => 'me#unfollow'
  match '/me/contact/:id' => 'me#contact'
  match '/me/messages/:id' => 'me#messages'
  match '/me/send_message/:id' => 'me#send_message', :as => 'send_message'
  match '/me/nearby/:id' => 'me#find_nearby_people', :as => 'nearby'

  resources :answers, :only => [:index]
  resources :badges

  resources :authentications

  resources :notifications, :only => [:getLatest, :getLatestCount] do
    collection do
      get 'getLatestCount'
      get 'getLatest'
    end
  end

  resources :notes, :only => [:create, :update, :destroy]
  #get "notes/index"
  #get "notes/show"
  #get "notes/create"
  #get "notes/update"
  #get "notes/new"

  resources :situations do
    resources :plans, :only => [:index]
    resources :users, :only => [:index]
    resources :stats, :only => [:index]
    resources :notes, :only => [:index]

    get 'complete_after_sign_in'

    collection do
      #post 'rate'
      get 'getWhatKeywords'
      get 'getWhereKeywords'
      get 'getWhenKeywords'
      post 'start'
      post 'next'
      get 'listby'
      get 'updatecategory'
      get 'linkaplan'
      get 'getLatest'
    end

    member do
      post 'rate'
      get 'rate'
      get 'vote_up'
      get 'vote_down'
      get 'quickshow'
    end

  end

  resources :admin

  resources :categories do
    resources :situations

    collection do
      get 'findrelated'
    end
  end

  get "home/index"
  get "home/error404"
 
  resources :plans do
    get 'complete_after_sign_in'
    
    member do
      post 'rate'
      get 'rate'
      get 'vote_up'
      get 'vote_down'
    end

  resources :users
    collection do
      get 'listby'
      get 'listplansby'
      get 'search'
      get 'updatecategory'
    end
  end

  resources :messages
  post '/messages/new' => 'messages#new', :as => :new_message

  resources :chats
  
  resources :filters

  root :to => "home#index"

  match "*path", :to => "application#routing_error"
  
end

