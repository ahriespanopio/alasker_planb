# Use this hook to configure merit parameters
Merit.setup do |config|
  # Check rules on each request or in background
  # config.checks_on_each_request = true

  # Define ORM. Could be :active_record (default) and :mongo_mapper and :mongoid
  config.orm = :active_record
  #config.orm = :mongoid
end

 Badge.create!({
   :id => 1,
   :name => 'newbie',
   :description => 'Ask a question more than once',
   :image => '/css/images/badges/newbee-smll.png'
   }
)


Badge.create!({
   :id => 2,
   :name => 'frequent-user',
   :description => 'Ask and post questions more than 5 times',
   :image => '/css/images/badges/frequent-user-smll.png'
  }
)


Badge.create!({
  :id => 3,
   :name => 'good-samaritan',
   :description => 'Link or update answers for the 10th time',
   :image => '/css/images/badges/good-samaritan-smll.png'
  }
)


Badge.create!({
  :id => 4,
  :name => 'expert',
  :description => 'Post a question or link an answer for the 15th time',
  :image => '/css/images/badges/expert-smll.png'
  }
)

if Category.table_exists?

 @badge_total = Badge.count
 @categories = Category.all
 @i = 1

 @categories.each do |category|
   Badge.create!({
    :id => @badge_total + @i,
    :name => category.name.gsub(" ", "-").downcase + "-pro",
    :description => category.name + " Pro",
    :image => '/css/images/badges/pro-smll.png'
   
   })
 @i += 1

 end
 
end