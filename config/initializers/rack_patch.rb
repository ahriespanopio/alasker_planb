require 'rack'

Rack::Request.class_eval do
    def ssl?
     scheme == 'https'
    end
end