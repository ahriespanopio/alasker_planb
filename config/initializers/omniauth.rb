Rails.application.config.middleware.use OmniAuth::Builder do
  
  if Rails.env.production?
    provider :twitter, 'lXQI9ccg9xN7e6N5JVYCTQ', 'C3mP7RPwvbZTu9ggsSQKgctkyUDDFoxDg6Cko8Ys'
  	provider :facebook, '154728984618224', '9e632fdc99c2694a71fc34435c59d3ef', {:client_options => {:ssl => {:ca_file => "#{Rails.root}/config/ca-bundle.crt"}}}
  elsif Rails.env.development?
    provider :twitter, '4UHAtClCWeWSfxOfYAWsA', 'nocydBs7o17i9FyuSbyULfh7FeEyhHaC5JxGTu5E'
    provider :facebook, '227937397332856', 'a9c12ae5eb93e84bb3463a5c67a9cade', {:client_options => {:ssl => {:ca_file => "#{Rails.root}/config/ca-bundle.crt"}}}
    #provider :twitter, 'SAZvaJKWkf2uKof6478NDg', 'P6s1Urnx6dcNi7VY2yclbUEJEuh93FoYtXGT88Nns'
    #provider :facebook, '291358667645197', '92a2d6c59583bfa09813b96e0eccc040', {:client_options => {:ssl => {:ca_file => "#{Rails.root}/config/ca-bundle.crt"}}}	
  else # test or staging (in heroku)
    provider :twitter, 'eawRB79R3zMw7HMB2ZM00w', 'hUYlUz4M8g6BLbjmq6iuDRgKmAuryOeDw3Ge45BnEs'
    provider :facebook, '222716371191879', '6ffdda53d0cced61fffd6cb3f49c1821', {:client_options => {:ssl => {:ca_file => "#{Rails.root}/config/ca-bundle.crt"}}}
  end

  OmniAuth.config.on_failure = Proc.new { |env|
    OmniAuth::FailureEndpoint.new(env).redirect_to_failure
  }
end