# Load the rails application
require File.expand_path('../application', __FILE__)

require 'chronic'
require 'uri_validator'
# Initialize the rails application
MyFirstApp::Application.initialize!

