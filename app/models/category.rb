class Category < ActiveRecord::Base
	has_many  :subcategories, :class_name => "Category"
	belongs_to :maincategory, :class_name => "Category", :foreign_key => "maincategory_id"
  has_many :situations
  
  has_many :filters, :through => :filter_categories_associations
  has_many :filter_categories_associations
  
  has_and_belongs_to_many :situations

	validates :name, :presence => true, :length => { :minimum => 3, :maximum => 20 }

	validates_uniqueness_of :name, :scope => :category_id

  attr_accessible :name
end