class Message < ActiveRecord::Base
  belongs_to :sender, :class_name=>'User', :foreign_key=>'sender_id'
  belongs_to :receiver, :class_name=>'User', :foreign_key=>'receiver_id'

  validates :receiver_id, :sender_id, :content, :presence => true, :on => :create

  default_scope order('created_at DESC').limit(5)

  def self.find_user(m)
    User.find(m).username
  end

end
