class FilterCategoriesAssociation < ActiveRecord::Base
  attr_accessible :filter_id, :category_id
  belongs_to :filter
  belongs_to :category
end
