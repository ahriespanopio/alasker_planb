# 5 stars is a common ranking use case. They are not given at specified
# actions like badges, you should define a cron job to test if ranks are to be
# granted.
#
# +set_rank+ accepts:
# * :+level+ ranking level (greater is better)
# * :+to+ model or scope to check if new rankings apply
# * :+level_name+ attribute name (default is empty and results in 'level'
#   attribute, if set it's appended like 'level_#{level_name}')

module Merit
  class RankRules
    include Merit::RankRulesMethods

    def initialize
      set_rank :level => 1, :to => User do |user|
         user.score > 1 && user.score <=249
      end

      set_rank :level => 2, :to => User do |user|
          user.score > 250 && user.score <=999
      end

      set_rank :level => 3, :to => User do |user|
         user.score > 1000 && user.score <= 2499
      end

       set_rank :level => 4, :to => User do |user|
         user.score > 2500 && user.score <= 4999
      end

      set_rank :level => 5, :to => User do |user|
         user.score > 4999
      end
      
    end
  end
end
