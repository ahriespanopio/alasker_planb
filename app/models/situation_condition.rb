class SituationCondition < ActiveRecord::Base
  belongs_to :situation, :class_name => "Situation", :foreign_key => "situation_id"

  attr_accessible :whendate, :whenkeyword, :where, :what, :situation_id
end
