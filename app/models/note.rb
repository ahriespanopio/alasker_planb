class Note < ActiveRecord::Base
  #set_table_name 'notes'

  belongs_to :user
  belongs_to :situation
  belongs_to :notification

  validates :content, :length => { :maximum => 140 }
end