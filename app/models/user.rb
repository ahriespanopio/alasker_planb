class User < ActiveRecord::Base
  #include PublicActivity::Model
  #tracked

  has_merit

  before_validation :smart_add_url_protocol
  include Gravtastic
  gravtastic :size => 50
  has_many :plans
  has_many :situations
  has_many :authentications
  has_many :notifications
  has_many :relationships, :foreign_key => "follower_id", :dependent => :destroy
  has_many :followed_users, :through => :relationships, :source => :followed
  has_many :reverse_relationships, :foreign_key => "followed_id",
                                   :class_name =>  "Relationship",
                                   :dependent =>   :destroy
  has_many :followers, :through => :reverse_relationships, :source => :follower
  has_many :sent_messages, :class_name => "Message", :foreign_key => "sender_id"
  has_many :received_messages, :class_name => "Message", :foreign_key => "receiver_id"
  has_many :filters

  # Include default devise modules. Others available are:
  # :token_authenticatable, :lockable, :timeoutable :activatable  :omniauthable

  devise :database_authenticatable, :registerable, #:confirmable,
         :recoverable, :rememberable, :trackable, #:validatable,
         :token_authenticatable, :authentication_keys => [:login]
  attr_accessor :login

  # Setup accessible (or protected) attributes for your model

  attr_accessible :username, :email, :password, :password_confirmation, :about, :online_status,
                  :admin_enabled, :website, :location, :login, :location_lat, :location_lng, :image,:use_uploadedPic

  mount_uploader :image, ImageUploader

  geocoded_by :location
  reverse_geocoded_by :location_lat, :location_lng, :address => :location
  after_validation :geocode, :reverse_geocode, :if => :location_changed?

  validates :username, :uniqueness => true, :length => { :minimum => 5, :maximum => 15 }, :format => /^[a-zA-Z0-9]*([_]?[a-zA-Z0-9])*$/i

  validates_presence_of   :email, :if => :email_required?
  validates_uniqueness_of :email, :allow_blank => true, :if => :email_changed?
  validates :email, :format => /^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,6})$/i, :if => :email_changed?

  validates_presence_of   :password, :on => :create, :if => :password_required?
  validates_confirmation_of   :password, :on => :create, :if => :password_required?
  validates_length_of :password, :maximum => 20

  validates :website, :uri => { :format => /^(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?$/ix }, :allow_blank => true

  validates :location, :length => { :maximum => 100 }

  def apply_omniauth(omniauth)
    authentications.build(:provider => omniauth['provider'], :uid => omniauth['uid'])
  end

  def password_required?
    authentications.empty? || !persisted? || !password.nil? || !password_confirmation.nil?
  end

  def email_required?
    true
  end

  def avatar_url
    if self.nil?
      "http://testing.alasker.com/css/images/avatar.jpg"
    else
      userpic = Authentication.find_by_user_id_and_provider(self.id, "facebook")
      if userpic
        "https://graph.facebook.com/" << userpic.uid << "/picture"
      else
         userpic = Authentication.find_by_user_id_and_provider(self.id, "twitter")
         if userpic
           "http://api.twitter.com/1/users/profile_image/" << userpic.uid
         else
           "http://testing.alasker.com/css/images/avatar.jpg"
         end
      end
    end
  end

  ajaxful_rater

  make_voter

  def destroy
    # Also delete authentications when a user account is deleted
    Authentication.destroy_all(:user_id => self.id)
    super
  end

  def following?(other_user)
    relationships.find_by_followed_id(other_user.id)
  end

  def follow!(other_user)
    relationships.create!(:followed_id => other_user.id)
  end

  def unfollow!(other_user)
    relationships.find_by_followed_id(other_user.id).delete
  end

  def followed_users(user_id)
    #User.find(:all, :joins=>" INNER JOIN relationships", :conditions => ["relationships.follower_id = ? AND users.id = relationships.followed_id", user_id] )
    User.find(:all, :joins=>" INNER JOIN relationships ON relationships.follower_id = #{user_id.id} AND users.id = relationships.followed_id")
  end

  def smart_add_url_protocol
    if !self.website.blank?
      unless self.website[/^http:\/\//] || self.website[/^https:\/\//]
        self.website = 'http://' + self.website
      end
    end

  end

  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions).first
    end

  end
end

