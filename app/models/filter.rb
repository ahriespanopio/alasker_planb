class Filter < ActiveRecord::Base
  belongs_to :user, :foreign_key => "user_id"
  has_many :categories, :through => :filter_categories_associations
  has_many :filter_categories_associations

  attr_accessible :what, :when_from, :when_to, :location, :location_lat, :location_lng, 
                  :category, :by_category, :by_location, :distance, :apply_filter

  attr_reader :category

  validates_presence_of :what, :location, :when_from, :when_to
  validates_uniqueness_of :what, :length => { :minimum => 5 }

  validates_numericality_of :distance, 
    :length => { :maximum => 4, :minimum => 1},
    :format => /\d+/
  
  geocoded_by :location
    reverse_geocoded_by :location_lat, :location_lng, :address => :location
    after_validation :geocode, :reverse_geocode, :if => :location_changed?

  def category=(ids)
    self.category_ids = ids.split(",")
  end

end