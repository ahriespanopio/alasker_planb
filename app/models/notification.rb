class Notification < ActiveRecord::Base

  validates :recipient_id, :event, :notification_type, :presence => true

  default_scope :order => 'notifications.created_at DESC', :limit => 10

  belongs_to :sender, :class_name => "User", :foreign_key => "sender_id"
  belongs_to :recipient, :class_name => "User", :foreign_key => "recipient_id"
  belongs_to :situation, :class_name => "Situation", :foreign_key => "situation_id"

  attr_accessible :sender, :recipient, :sender_id, :recipient_id, :situation_id, :note_id, 
                  :notification_type, :event, :expires_at, :plan_b_id, :unseen, :event_target

  EVENT = {
    :joined_Alasker         => 1,
    :posted_a_question      => 2,
    :answered_a_question    => 3,

    :updated_a_question     => 4,
    :edited_an_answer       => 5,
    :updated_a_profile      => 6,

    :earned_new_badge       => 7,
    :followed               => 8,
    :unfollowed             => 9,
    
    :question_deleted     => 10,   
    :answer_deleted       => 11,
    :user_profile_deleted => 12,
    
    :chatted              => 13,
    :updated_a_note       => 14,
    :note_deleted         => 15,

    :messaged   => 16

    # :joined_Alasker => 1,
    # :posted         => 2,
    # :answered       => 3,
    # :updated        => 4,
    # :edited         => 5,
    
    # :earned         => 7,
    # :followed       => 8,
    # :shared         => 9,
    # :tweeted        => 10,
    # :upvoted        => 11,
    # :downvoted      => 12,

    # :sent           => 16
  }

  EVENT_TARGET = {
    :own_profile    => 1,
    :users_question => 2,
    :own_answer     => 3,
    :user           => 4,
    :the_name_badge => 5,
    :user_a_message => 6,
    :user_a_note    => 7
  }

  NOTIFICATION_TYPE = {
    :urgent => 1,
    :normal => 2
  }

  def self.eventSender(e)
      unless e.sender_id == e.recipient_id || e.sender_id.nil?
        @sender = User.find(e.sender_id)
        @sender.username
      else
        "You"
      end
  end

  def self.eventRecipient(e, currentUserID)
    @recipient = User.find(e.recipient_id)
    r = e.recipient_id
    s = e.sender_id
      if r == s || s.nil?
        if e.event == 3
          "your"
        else
          ""
        end
      else
        if e.event == 3
          if (r!=s && r != currentUserID)
            " #{@recipient.username}'s"
          elsif r.nil?
            "someone's"
          else
            " your"
          end
        else
          if (r!=s && r != currentUserID)
            @recipient.username
          else
            "you"
          end
        end
      end
  end

  def self.content(event, recipient)
    case event
      when 1
        " joined Alasker"
      when 2
        " posted a " # question
      when 3
        " answered" # question
      when 4
        " updated a " # question
      when 5
        " edited an " # answer
      when 6
        " updated some profile info"
      when 7
        " earned a badge"
      when 8
        " followed"
      when 13
        " chatted"
      when 16
        " sent a message to"
      else
        ""
    end
  end

end