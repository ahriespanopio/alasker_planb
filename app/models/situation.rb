class Situation < ActiveRecord::Base
	#include PublicActivity::Model
	#tracked :owner => proc { |controller, model| controller.current_user }

	belongs_to :category
	belongs_to :user
	belongs_to :stat
	belongs_to :plan
  belongs_to :situationcondition, :class_name => "SituationCondition", :foreign_key => "situationcondition_id"

	accepts_nested_attributes_for :plan, :update_only => true
	accepts_nested_attributes_for :situationcondition, :update_only => true
	
	has_and_belongs_to_many :categories  

	has_many :planbs, :class_name => 'PlanB'
 	has_many :plans, :through => :planbs

  has_many :notes
  has_many :notifications

	ajaxful_rateable :stars => 10, :dimensions => [:bestsituation]

	validates :content, :presence => true, :length => { :minimum => 5 }

	validates_uniqueness_of :content

	make_voteable

	attr_accessor :situation_id, :situationcondition_id

	#validates_associated :category, :on => :create
	#validates_presence_of :category_id

  geocoded_by :address
  reverse_geocoded_by :location_lat, :location_lng
  after_validation :geocode, :reverse_geocode, :if => :address_changed?

  default_scope order('created_at DESC').limit(10)
end

