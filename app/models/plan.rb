class Plan < ActiveRecord::Base
	belongs_to :situation, :class_name => "Situation"
	belongs_to :category

 	has_many :planbs, :class_name => "PlanB"
  has_many :situations, :through => :planbs
  has_many :notifications, :through => :planbs

  belongs_to :plancondition, :class_name => "PlanCondition", :foreign_key => "plancondition_id"
  accepts_nested_attributes_for :plancondition

	belongs_to :user
	belongs_to :stat

  ajaxful_rateable :stars => 10, :dimensions => [:bestplan]

	validates :content, :presence => true, :length => { :minimum => 5 }

	validates_uniqueness_of :content, :scope => :id

  attr_accessor :new_situation_id, :planB_keywords, :plan_condition_what, :plan_condition_where, :plan_condition_when
  attr_accessor :plan_id
  make_voteable

	#validates_associated :situation, :on => :create
	#validates_presence_of :situation_id

end

