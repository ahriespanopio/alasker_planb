class PlanB < ActiveRecord::Base
  belongs_to :situation
  belongs_to :plan
  has_many :notifications

  validates_uniqueness_of :situation_id, :scope => [:situation_id, :plan_id]
  validates_uniqueness_of :plan_id, :scope => [:situation_id, :plan_id]

  attr_accessible  :plan_b_id
  #validates_presence_of :plan_id

end