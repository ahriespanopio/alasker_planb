class RelationshipObserver < ActiveRecord::Observer
  def after_create(relationship)
    # Notification.create(
    #   :sender_id => relationship.follower_id,
    #   :recipient_id => relationship.followed_id,
    #   :situation_id => nil,
    #   :note_id => nil,
    #   :plan_b_id => nil,
    #   :notification_type => Notification::NOTIFICATION_TYPE[:normal], 
    #   :event => Notification::EVENT[:followed],
    #   :event_target => Notification::EVENT_TARGET[:user],
    #   :expires_at => relationship.created_at + 3.days
    # )
    Notification.create(:sender_id => relationship.follower_id, :recipient_id => relationship.followed_id,
                      :notification_type => Notification::NOTIFICATION_TYPE[:normal], 
                      :event => Notification::EVENT[:followed],
                      :expires_at => relationship.created_at + 3.days)
  end

  def before_destroy(relationship)
    Notification.destroy_all "sender_id = relationship.follower_id AND recipient_id = relationship.followed_id AND event = 8"
  end
end