class UserObserver < ActiveRecord::Observer
  def after_create(user)
    if user.email?
      UserMailer.welcome_email(user).deliver
      Notification.create(
        :sender_id => nil,
        :recipient_id => user.id,
        :situation_id => nil,
        :note_id => nil,
        :plan_b_id => nil,
        :notification_type => Notification::NOTIFICATION_TYPE[:normal], 
        :event => Notification::EVENT[:joined_Alasker],
        :expires_at => user.created_at + 3.days
      )
    end
  end

  def after_update(user)
    if user.email
      if user.password && user.sign_in_count > 1
        UserMailer.change_password(user).deliver
      end
    end
  end
end