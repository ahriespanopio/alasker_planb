module PlansHelper
  include LayoutHelper

  def format_situations(situations)
    result = Array.new

    situations.each do |s|
      result << format_situation(s)
    end

    return { :items => result, :count => result.length }
  end

  def format_situation(situation)
    user = nil
    if situation.user
      user = situation.user
    end

    result = situation.attributes
    result[:user_name] = user_name(user)
    result[:user_url] = user_url(user)
    result[:avatar] = avatar_url(user)

    return result
  end

  def format_plans(plans)
    result = Array.new

    plans.each do |s|
      result << format_plan(s)
    end

    return result
  end

  def format_plan(plan)
    user = nil
    if plan.user
      user = plan.user
    end

    result = plan.attributes
    result[:user_name] = user_name(user)
    result[:user_url] = user_url(user)
    result[:avatar] = avatar_url(user)

    return result
  end
end
