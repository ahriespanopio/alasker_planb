# These helper methods can be called in your template to set variables to be used in the layout
# This module should be included in all views globally,
# to do so you may need to add this line to your ApplicationController
#   helper :layout
module LayoutHelper
  def title(page_title, show_title = true)
    content_for(:title) { h(page_title.to_s) }
    @show_title = show_title
  end

  def show_title?
    @show_title
  end

  def stylesheet(*args)
    content_for(:head) { stylesheet_link_tag(*args) }
  end

  def javascript(*args)
    content_for(:head) { javascript_include_tag(*args) }
  end
  
  # this function returns an image url for the given user
  # priority: facebook, twitter, gravatar, default
  def avatar_url(user)
    if user.nil?
      root_url + "css/images/avatar.jpg"
    else
      if user.use_uploadedPic == 1 && !user.image.url.nil?
        user.image.url
      elsif user.use_uploadedPic == 1 && user.image.url.nil?
        root_url + "css/images/avatar.jpg"
      else
        userpic = Authentication.find_by_user_id_and_provider(user.id, "facebook")
        if userpic
          "https://graph.facebook.com/" << userpic.uid << "/picture"
        else
          userpic = Authentication.find_by_user_id_and_provider(user.id, "twitter")
          if userpic
            "http://api.twitter.com/1/users/profile_image/" << userpic.uid    
          else
            user.gravatar_url :default => root_url + "css/images/avatar.jpg"
          end
        end
      end
    end
  end
  
  # returns the user name with link to user's situations/plans or "guest" and no link if no user
  def by_user(user)
    if user.nil?
      "guest"
    else
      link_to user.username, "/me/#{user.id.to_s}", :class => "link"
    end
  end
  
  # returns the user's avatar with link to user's situations/plans or default avatar and no link if no user
  def avatar_link_to_user(user)
    if user.nil?
      image_tag avatar_url(nil), :alt => "", :height=>50, :width=>50, :align=>'middle'
    else
      link_to (image_tag avatar_url(user), :alt => "", :height=>50, :width=>50, :align=>'middle'), "/me/#{user.id.to_s}"
    end    
  end

  def user_url(user)
    if user.nil?
      "#"
    else
      "/me/#{user.id.to_s}"
    end
  end

  def user_name(user)
    if user.nil?
      "guest"
    else
      user.username
    end
  end

end
