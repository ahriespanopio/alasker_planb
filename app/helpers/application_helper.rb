module ApplicationHelper
  
  def google_analytics_js

    if (request.path_parameters[:controller] == "situations") or       
       (request.path_parameters[:controller] == "categories") or
       (request.path_parameters[:controller] == "help") or
       (request.path_parameters[:controller] == "badges") or
       (request.path_parameters[:controller] == "plans") or
       (request.path_parameters[:controller] == "me") or
       (request.path_parameters[:controller] == "home")
        
      content_tag(:script, :type => 'text/javascript') do
      "
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-36081820-1']);
        _gaq.push(['_setDomainName', 'alasker.com']);
        _gaq.push(['_setAllowLinker', true]);
        _gaq.push(['_trackPageview']);

        (function() {
          var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
          ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
          var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
      "
      end if Rails.env.production?
    end
  end

  def nav_selected(link,link1)
    @class = "selected"
     if (request.path_parameters[:controller] == link || request.path_parameters[:controller] == link1)
       return @class
     end
  end

  # bitly the Q&A path to be shared/tweet
  def bitly_sharetweet(url)
    begin 
      bitly ||= begin
      Bitly.use_api_version_3
      Bitly.new('goalasker', 'R_606bc156a9abe36dd166c1fddba81813')
      end
    bitly.shorten(url)
    rescue
      return nil
    end
  end

  def avatar_url(user)
    if user.nil?
      root_url + "css/images/avatar.jpg"
    else
      userpic = Authentication.find_by_user_id_and_provider(user.id, "facebook")
      if userpic
        "https://graph.facebook.com/" << userpic.uid << "/picture"
      else
         userpic = Authentication.find_by_user_id_and_provider(user.id, "twitter")
         if userpic
           "http://api.twitter.com/1/users/profile_image/" << userpic.uid
         else
           user.gravatar_url :default => root_url + "css/images/avatar.jpg"
         end
      end
    end
  end

  def unseen_notif
    n = Notification.where("unseen = 1 AND 
                            recipient_id = ? AND 
                            sender_id IS NOT NULL 
                            AND sender_id != recipient_id", 
                            current_user.id).count
    n == 0 ? "" : "Notification [#{n}]"
  end

  def get_onliners
    User.where("last_activity > ? ", 15.minutes.ago.to_s(:db))
  end

  def get_nearbyers(user, distance=20)
    User.near(user.location, distance, :order => :distance)
  end

  def filter_nearby(user, f)
      if f.apply_filter == true && f.by_location == 1
        Situation.near(f.location, f.distance, :order => :distance)
      else
        Situation.near(user.location, f.distance, :order => :distance)
      end
  end

  def filter_categories(ids, f)
    if f.apply_filter == true && f.by_category == 1
      Situation.where(:category_id => ids)
    else
      Situation.all
    end
  end

  def find_filter(u)
    Filter.where(user_id: u.id).first
  end

  def find_cats(f)
    FilterCategoriesAssociation.where(:filter_id => f.id).map(&:category_id)
  end

  def broadcast(channel, &block)
    chat = {:channel => channel, :data => capture(&block)} #, :ext => {:auth_token => FAYE_TOKEN}
    uri = URI.parse("http://alaskerredis.herokuapp.com/faye")
    Net::HTTP.post_form(uri, :chat => chat.to_json)
  end
end