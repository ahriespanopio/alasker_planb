module BadgesHelper

  def get_next_badge
      @badge_id = Badge.find {|p| p.id == @badge.id + 1}
      if @badge_id.length > 0
         link_to image_tag("/css/images/arrow_right.png", :border=> "0"),
                           "/badges/#{@badge.id.to_i + 1}", :class => "badge_click_right"
     end
  end

  def get_previous_badge
      @badge_id = Badge.find {|p| p.id == @badge.id - 1}
      if @badge_id.length > 0
         link_to image_tag("/css/images/arrow_left.png", :border=> "0"),
                           "/badges/#{@badge.id.to_i - 1}", :class => "badge_click_left"
     end
  end

  def current_user_has_badge?
    if user_signed_in?
      @badge_id = Badge.find{current_user.sash.badge_ids[@badge.id - 1]}
    
       if @badge_id.length > 0
        return true
       else
         return false
       end
    end
     
  end

  def number_of_pro_users
    @total_users = 0
    for i in 0..@badge.count - 4
      @total_users = @total_users + number_of_users(i + 5);
    end
    return @total_users
  end
  
  def number_of_users(badge_id)

   @users = User.find_by_sql("SELECT * FROM `users`
                              INNER JOIN sashes 
                              ON users.sash_id = sashes.id 
                              JOIN badges_sashes 
                              ON sashes.id = badges_sashes.sash_id
                              WHERE badges_sashes.badge_id = #{badge_id.to_i}"
                            )
    return @users.count
  end

end