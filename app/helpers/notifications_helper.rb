module NotificationsHelper
  include LayoutHelper

  def prepare_notifications(notifications)
    result = Array.new

    notifications.each do |s|
      result << prepare_notification(s)
    end

    return { :items => result, :count => result.length }
  end

  def prepare_notification(notification)
    sender = nil

    if notification.sender
      sender = notification.sender
    end

    result = notification.attributes
    result[:sender_name] = user_name(sender)
    result[:sender_url] = user_url(sender)
    result[:avatar] = avatar_url(sender)
    result[:question] = {}
    result[:answer] = {}
    result[:comment] = {}
    result[:content] = ""

    unless notification.situation_id.nil?
      situation = Situation.find_by_id(notification.situation_id)
      result[:question] = situation.attributes
      result[:content] = situation.content
    end

    unless notification.note_id.nil?
      note = Note.find_by_id(notification.note_id)
      result[:comment] = note.attributes
      result[:content] = note.content
    end

    unless notification.plan_b_id.nil?
      planb = PlanB.find_by_id(notification.plan_b_id)
      if planb
        situation = Situation.find_by_id(planb.situation_id)
        plan = Plan.find_by_id(planb.plan_id)
        result[:question] = situation.attributes
        result[:answer] = plan.attributes
        result[:content] = plan.content
      end
    end

    return result
  end
end
