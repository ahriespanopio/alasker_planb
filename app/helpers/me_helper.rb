module MeHelper

  def get_level_percent
    @total = (@user.level / 5) * 100
    @total = number_to_percentage(@total)
    return @total
  end

end
