module CategoriesHelper
  def format_map_element(situation) 
    user = nil
    if situation.user
      user = situation.user
    end

    category = 'None'
    if situation.category
      category = situation.category.name
    end

    package = { :content => situation.content , 
                :when => format_when_date(situation.situationcondition), 
                :category => category , :lat => situation.location_lat, 
                :lng => situation.location_lng, :user_name => user_name(user), 
                :user_url => user_url(user), :avatar => avatar_url(user) 
              }

    return package
  end 
end
