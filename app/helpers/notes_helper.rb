module NotesHelper
  include LayoutHelper

  def prepare_notes(notes)
    result = Array.new

    notes.each do |note|
      result << prepare_note(note)
    end

    return { :items => result, :count => result.length }
  end

  def prepare_note(note)
    sender = nil

    if note.user
      sender = note.user
    end

    result = note.attributes
    result[:sender_name] = user_name(sender)
    result[:sender_url] = user_url(sender)
    result[:avatar] = avatar_url(sender)

    return result
  end
end
