class UserMailer < ActionMailer::Base
  helper :application

  default :from => "goalasker@gmail.com"

  # send welcome email after signing up
  def welcome_email(user)
    @user = user
    @url  = "http://" + default_url_options[:host] + "/users/sign_in"
    begin 
      mail(:to => user.email, 
      		 :subject => "[alasker] Welcome to alasker, \
                        #{user.username}!") do |format|
              format.text
              format.html
            end
     rescue
     end
  end

  # send user messages by email
  def send_message(user, current_user, body)
  	@recipient = user
  	@sender    = current_user
    @message   = body
    @url       = "http://" + default_url_options[:host] + "/me/#{@sender.id}"
    mail(:to => @recipient.email,
    		 :subject => "[alasker] You have a new message, #{@recipient.username}!")
  end

  # send notification to asker after creating a question
  def add_question
  end

  # send notification to asker after new answer is posted
  def add_answer(the_user, the_situation, the_plan, the_responder)
    @recipient = the_user.username
    @response  = the_plan
    @question  = the_situation

    if the_responder.nil? ? @responder = 'Someone' : @responder = the_responder.username ; end

    @url = "http://" + default_url_options[:host] + "/plans/#{@response.id}"
    begin
    mail(:to => the_user.email,
         :subject => "[alasker] A quick response for you, \
                      #{@recipient}!") do |format|
            format.text
            format.html
          end
          
    rescue
    end
  end

  # notify by email when user is followed
  def follow_user(user, current_user)
    @recipient = user
    @sender = current_user
    @url = "http://" + default_url_options[:host] + "/me/#{@sender.id}"
    
    mail(:to => @recipient.email,
         :subject => "[alasker] #{@recipient.username}, you have a new follower!")
  end

  # notify by email when new badge is earned
  def earn_badge(current_user, badge)
    @recipient = current_user
    @user_badge = badge
    @url = "http://" + default_url_options[:host] + "/badges/#{@user_badge.id}"
    
    mail(:to => @recipient.email,
         :subject => "[alasker] #{@recipient.username}, you earned a badge!")
  end

  # notify by email when note is added
  def add_note(note)
    situation = note.situation
    @recipient = situation.user.username
    @question  = situation
    @responder  = "anonymous"
    @responder = note.user.username unless note.user.nil?
    @note = note
    @url = "http://" + default_url_options[:host] + "/situations/#{@question.id}/notes"


      mail(:to => note.situation.user.email,
           :subject => "[alasker] A quick comment for you, \
           #{@recipient}!") do |format|
        format.text
        format.html
      end
  end

  # send password change confirmation
  def change_password(user)
    @user = user
    @url  = "http://" + default_url_options[:host] + "/users/sign_in"
    begin 
      mail(:to => user.email, 
           :subject => "[alasker] Your password was recently changed, \
                        #{user.username}") do |format|
              format.html
            end
     rescue
     end
  end

  # send notification to all online users for new questions
  def notify_onliners(asker, onliners, question)
    @recipient = onliners
    @question  = question
    @url       = "http://" + default_url_options[:host] + "/situations/#{@question.id}"
    
    if asker.nil? ? @sender = 'Someone' : @sender = asker.username; end

    mail(:bcc => @recipient.map(&:email),
         :subject => "[alasker] New question posted by #{@sender}!") do |format|
              format.html
          end
  end

  # notify individual users of personal activities
  def send_user_notification
  end

  # notify individual users of public activities
  def send_allusers_notification
  end

end