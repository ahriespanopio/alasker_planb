class MeController < ApplicationController
  include ApplicationHelper
  before_filter :find_user, :only => [:show, :follow, :unfollow, :send_message, :contact, :messages, :find_nearby_people]

  def find_user
    @user = User.find(params[:id])
  end

  def index
    @user = current_user
    @users = User.where("username LIKE ?", "%#{params[:q]}%")
    user_last_activity(@user)
    unless @user.nil?
      selectOnUser
      @followed_users = @user.followed_users(@user)

      respond_to do |format|
        format.html { render :action => "show", :layout => "layouts/application2",
                      :locals => {:user_page => true} }
        format.json { render :json => @users.map{|c| [c.username, c.id]} }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  def show
    unless @user.nil?
      selectOnUser(@user)
      @followed_users = @user.followed_users(@user)

      respond_to do |format|
        format.html { render :action => "show",
          :layout => "layouts/application2" ,
          :locals => {:user_page => true} }
        format.json { render :json => @user,
          :methods => [:avatar_url] }
      end
    end
  end

  def selectOnUser(user = current_user)
    @situations = Situation.where("situations.user_id = ?", user.id)
    @plans = Plan.where("plans.user_id = ?", user.id)
    @notifications = Notification.where('recipient_id != sender_id AND recipient_id = ?', user.id)
    @activities    = Notification.where('(recipient_id = ? AND sender_id IS NULL) OR sender_id = ?', user.id, user.id)
  end

  def test
    render :layout => nil
  end

  def follow
    unless @user.nil?
      unless current_user.nil?
        current_user.follow!(@user)
        UserMailer.follow_user(@user, current_user).deliver
        # @activities = PublicActivity::Activity.all
        redirect_to(:back)
      else
        redirect_to '/users/sign_in'
      end
    end
  end

  def unfollow
    unless @user.nil?
      unless current_user.nil?
        current_user.unfollow!(@user)
        # @activities = PublicActivity::Activity.all
        redirect_to(:back)
      else
        redirect_to '/users/sign_in'
      end
    end
  end

  def send_message
    unless @user.nil?
      unless current_user.nil?
        body = params[:send_message][:content]
        subj = params[:send_message][:subject]
        unless body.empty?
          UserMailer.send_message(@user, current_user, body.strip).deliver

          Notification.create(:sender_id => current_user.id ,:recipient_id => @user.id,
                              :situation_id => nil, :plan_b_id => nil,
                              :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                              :event => Notification::EVENT[:messaged],
                              :expires_at => DateTime.now + 3.days)

          @message = Message.create(
            :sender_id => current_user.id, 
            :receiver_id => @user.id, 
            :subject => subj,
            :content => body,
            :label => "inbox" )

          msg = @message.clone
          msg.label = "sent"
          msg.save

          flash[:notice] = "Message sent successfully!"
          redirect_to me_path
        else
          render :action => "contact", :layout => "layouts/application2"
          flash[:notice] = "You did not write anything."
        end
      else
        redirect_to '/users/sign_in'
      end
    end
  end

  def contact
    unless @user.nil?
      unless current_user.nil?
        render :action => "contact", :layout => "layouts/application2"
      else
        redirect_to '/users/sign_in'
      end
    end
  end

  def messages
    unless @user.nil?
      unless current_user.nil?
        render :action => "messages", :layout => "layouts/application2"
      else
        redirect_to '/users/sign_in'
      end
    end
  end

  def find_nearby_people
    unless current_user.nil?
      @nearby_people = get_nearbyers(current_user)

      respond_to do |format|
        format.html { render :action => "find_nearby_people",
          :layout => "layouts/application2" }
        format.json { render :json => @nearby_people }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

end
