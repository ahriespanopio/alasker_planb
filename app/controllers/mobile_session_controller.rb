class MobileSessionController < ApplicationController
  #before_filter :authenticate_user!, :except => [:create, :destroy]
  before_filter :ensure_params_exist
  respond_to :json

  def create
    resource = nil
    require_password = true

    if params[:user_login][:auth_token] && !params[:user_login][:auth_token].blank?
      resource = User.where(:authentication_token=>params[:user_login][:auth_token]).first
      require_password = false
    elsif params[:user_login][:provider].blank?
      resource = User.find_for_database_authentication(:login => params[:user_login][:login])
    else
      authentication = Authentication.where("provider = ? and uid = ?", params[:user_login][:provider], params[:user_login][:uid]).first
      if authentication
        resource = authentication.user
        require_password = false
      end
    end
    return invalid_login_attempt unless resource

    can_signin = true
    if require_password
      can_signin = resource.valid_password?(params[:user_login][:password])
    end

    if can_signin
      sign_in(:user, resource)
      resource.ensure_authentication_token!
      render :json=> {:success=>true, :username=>resource.username, :email=>resource.email, :auth_token=>resource.authentication_token}
      return
    end
    invalid_login_attempt
  end

  def destroy
    resource = User.find_for_database_authentication(:login => params[:user_login][:login])
    resource.authentication_token = nil
    resource.save
    render :json=> {:success=>true}
  end

  protected
  def ensure_params_exist
    return unless params[:user_login].blank?
    render :json=>{:success=>false, :message=>"missing user_login parameter"}, :status=>422
  end

  def invalid_login_attempt
    render :json=> {:success=>false, :message=>"Invalid credentials"}, :status=>401
  end
end
