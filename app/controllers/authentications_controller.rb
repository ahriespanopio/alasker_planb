class AuthenticationsController < ApplicationController
  respond_to :json

  def index
    @authentications = current_user.authentications if current_user
    render :action => "index", :layout => "layouts/application2" 
  end

  def create
    #debugger

    if params[:format] =~ /(js|json)/
      omniauth = params[:auth]

      authentication = Authentication.where("provider = ? and uid = ?", omniauth['provider'], omniauth['uid']).first
      if authentication
         user = authentication.user
         sign_in_and_respond(user)
      else
        user = User.new(omniauth[:info])
        user.apply_omniauth(omniauth)
        if user.save
          sign_in_and_respond(user)
        else
          session[:omniauth] = omniauth.except('extra')
          render :json=> { :authenticated=>false, :errors=>user.errors, :auth=>omniauth }
        end
      end
    else
      omniauth = request.env["omniauth.auth"]

      authentication = Authentication.where("provider = ? and uid = ?", omniauth['provider'], omniauth['uid']).first
      if authentication
        flash[:notice] = "Signed in successfully."
        sign_in_and_redirect(:user, authentication.user)
      elsif current_user
        current_user.authentications.create(:provider => omniauth['provider'], :uid => omniauth['uid'])
        flash[:notice] = "Added your " << omniauth['provider'].titleize << " account."
        redirect_to authentications_url
      else
        user = User.new
        user.apply_omniauth(omniauth)
        if user.save
          flash[:notice] = "Signed in successfully."
          sign_in_and_redirect(:user, user)
        else
          session[:omniauth] = omniauth.except('extra')
          redirect_to new_user_registration_url
        end
      end
    end
  end

  def destroy
    @authentication = current_user.authentications.find(params[:id])
    provider = @authentication.provider.titleize
    current_user.destroy
    @authentication.destroy
    flash[:notice] = "Removed your " << provider << " account."
    redirect_to authentications_url
  end

protected
  def sign_in_and_respond(resource)
    sign_in(:user, resource)
    resource.ensure_authentication_token!
    render :json=> {:authenticated=>true, :username=>resource.username, :email=>resource.email, :auth_token=>resource.authentication_token}
  end
end

