class HomeController < ApplicationController

  def index
    user_last_activity(current_user)

    #testing for domain issue
    @host_name = "<!-- nil -->"
    if !headers["host"].nil?
      @host_name = "<!--"+ headers["host"]+ " -->"
    end


    if params[:page].nil?
      @stat = Stat.order("viewcount").last

      if !@stat.nil?
        @situation_top = @stat.situation
      end

      @categories = Category.all
      @situation_latest_1 = Situation.first
      @situation_latest_2 = Situation.offset(1).first
      @situation_latest_3 = Situation.offset(2).first
      @situation_fan = Situation.first

      @rates = Rate.all

      if !@rates.nil? && !@situations.nil?
        @situation_popular = Situation.joins("left join rates on rates.rateable_id = situations.id " +
                                              "and rates.rateable_type = 'Situation'").select('distinct(situations.id), situations.*').order('stars DESC').first
      end

      @situations = Situation.all

      render :action => "index", :layout => "layouts/application2", :locals => {:big_answer_askbar => true}
    else
      @situations = Situation.paginate(:page => params[:page], :per_page => 10)

      if !@situations.nil? and @situations.length > 0
        render :partial => "situations/pagedlist", :layout => false
      else
        render :nothing => true, :status => 200, :content_type => 'text/html'
      end

      #render :json => {:html => html, :count => @situations.length}
    end

  end

  def error404
    render :layout => "layouts/application2", :locals => {:content_wide => true}
  end

end

