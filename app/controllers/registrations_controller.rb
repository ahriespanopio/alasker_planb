class RegistrationsController < Devise::RegistrationsController


  def create
    respond_to do |format|
      format.html do
        super
        session[:omniauth] = nil unless @user.new_record?
      end
      format.json do
        build_resource

        if resource.save
          sign_in(:user, resource)
          resource.ensure_authentication_token!
          render :json=> {:success=>true, :username=>resource.username, :email=>resource.email, :auth_token=>resource.authentication_token}
        else
          clean_up_passwords(resource)
          respond_with_navigational(resource) { render_with_scope :new }
        end

        session[:omniauth] = nil
      end
    end
  end

  private
  def build_resource(*args) 
    super  
    if session[:omniauth]  
      @user.apply_omniauth(session[:omniauth])
      info = session[:omniauth]['info']
      if !info['email'].nil? && !info['email'].to_s.empty?
        @user.email = info['email']
      end
      if @user.username.nil? || @user.username.to_s.empty?
        @user.username = info['nickname']
      end
      @user.valid?
      user_last_activity(@user)
    end  
  end
end  