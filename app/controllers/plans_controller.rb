class PlansController < ApplicationController
  include PlansHelper
  respond_to :json
  # GET /plans
  # GET /plans.xml
  def index
    #    @plans = Plan.all
    #    @situations = Situation.joins(:plans).select('distinct(situations.id), situations.*').order('situations.created_at DESC')

    if !params[:searchstring].nil?
      @escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')
    end
    @categories = Category.all

    if params[:format] =~ /(js|json)/
      #respond_with(@plans = Plan.all)
      if params[:situation_id]
        plans = Plan.find_all_by_situation_id(params[:situation_id])
        render :json => format_plans(plans)
      end

    else
      render :action => "index", :layout => "layouts/application2"
    end
  end

  def listby

    #
    # Default Limiting Setup
    #

    default_limit = 5

    if params[:limit] && Integer(params[:limit]) > 0
      default_limit = Integer(params[:limit])
    end

    #
    # Join Situations with the correct the tables, such as which ones are expiring, category, new, etc.
    #

    if params[:by] == '1'
      @situations = Situation.joins(:plans).joins('left join stats on stats.situation_id = situations.id').select('distinct(situations.id), situations.*')
    elsif params[:by] == '2'
      @situations = Situation.joins(:plans).joins('left join stats on stats.situation_id = situations.id').select('distinct(situations.id), situations.*')
    elsif  params[:by] == '0'
      @situations = Situation.joins('left join stats on stats.situation_id = situations.id').joins('left join plans on plans.situation_id = situations.id')
      .select('distinct(situations.id), situations.*').where('plans.id IS NULL')
    elsif params[:home] == '0'
      @situations = Situation.limit(default_limit)
      params[:sort] = 'newest'
    elsif params[:home] == '1'
      @situations = Situation.joins('left join stats on stats.situation_id = situations.id').joins('left join situation_conditions on situation_conditions.situation_id = situations.id')
      .select('distinct(situations.id), situations.*').where('expires is null OR expires > ?', Time.now)
      params[:sort] = 'expiring'
    elsif params[:home] == '2'
      params[:sort] = 'newest'
      @situations = Situation.all
    elsif params[:home] == '3'
      params[:sort] = 'popular'
      @situations = Situation.all
    else
      @situations = Situation.joins('left join stats on stats.situation_id = situations.id').joins('left join plans on plans.situation_id = situations.id')
      .select('distinct(situations.id), situations.*').where('plans.id IS NULL')
    end

    if params[:category_id] && !params[:category_id].nil?
      #@situations = @situations.where(:category_id => params[:category_id])
      @situations = Situation
      .joins('left join categories_situations on categories_situations.situation_id = situations.id')
      .select('distinct(situations.id), situations.*')
      .where('situations.category_id = ? or categories_situations.category_id = ?', params[:category_id], params[:category_id])
    end

    #
    # Handle Search Strings here
    #

    if  !params[:searchstring].nil?
      escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')
      if !escaped_searchstring.blank? && !escaped_searchstring.empty?
        #@situations = @situations.where("MATCH(content,keywords) AGAINST (?)", escaped_searchstring)
        @situations = @situations.basic_search({content: escaped_searchstring, keywords: escaped_searchstring}, false)
      end
    end

    #
    # Now do the correct sort ordering here
    #

    if params[:sort] == 'newest'
      @situations = @situations.order('situations.created_at DESC')
    elsif params[:sort] == 'expiring'
      #@situations = @situations.order('(SELECT situation_conditions.whendate FROM situation_conditions WHERE situation_conditions.situation_id = situations.id LIMIT 1)')
      @situations = @situations.select('situation_conditions.whendate').order('situation_conditions.whendate')
    #elsif params[:sort] == 'popular'
      #@situations = @situations.order('(SELECT stats.viewcount FROM stats WHERE stats.situation_id = situations.id LIMIT 1) DESC')
    elsif params[:sort] == 'mostplans'
      #@situations = @situations.order('(SELECT COUNT(plans.id) FROM plans WHERE plans.situation_id = situations.id LIMIT 1) DESC')
      @situations = @situations.select('COUNT(plans.id) as plans_count').group('situations.id').order('plans_count DESC')
    else
      #@situations = @situations.order('(SELECT stats.viewcount FROM stats WHERE stats.situation_id = situations.id LIMIT 1) DESC')
      @situations = @situations.select('stats.viewcount').order('stats.viewcount DESC')
    end

    #
    # Format the results here correctly
    #

    if params[:paginate] == '1'
      @situations = @situations.paginate(:page => params[:page], :per_page => default_limit)
      @paging_on = true
    end

    if params[:format] =~ /(js|json)/
      # respond_with(@situations)
      package = format_situations(@situations.flatten)
      render :json =>  package
    else
      if params[:newlayout] == '1'
        render :partial => "situations/situationlist", :layout => false, :locals => {:showtitle => false, :showplans => true, :paging_on => @paging_on}
      else
        render :layout => false
      end
    end
  end

  # GET /plans/1
  # GET /plans/1.xml
  def show
    session.delete("redirect")

    @plan = Plan.find(params[:id])
    response = @plan.attributes

    if !(@plan.plancondition.nil?)
      response[:plancondition] = @plan.plancondition
    end

    if @plan.situations.any?
      response[:situations] = @plan.situations
    end

    @categories = Category.all
    track(@plan)

    respond_to do |format|
      format.html { render :action => "show", :layout => "layouts/application2", :locals => {:big_answer_askbar => false} }
      format.json { render :json=> response }
    end

  end

  def search
    escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')
    #@plans = Plan.where("MATCH(content,keywords) AGAINST (?)", escaped_searchstring).all
    @plans = Plan.basic_search({content: escaped_searchstring, keywords: escaped_searchstring}, false)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @plan }
    end
  end

  def listplansby

    default_limit = 20

    if params[:limit] && Integer(params[:limit]) > 0
      default_limit = Integer(params[:limit])
    end

    if (params[:situationsearch])
      escaped_searchstring = params[:situationsearch].gsub('%','\%').gsub('_', '\_')
      #@plans = Plan.where("MATCH(content,keywords) AGAINST (?)", escaped_searchstring).limit(default_limit)
      @plans = Plan.basic_search({content: escaped_searchstring, keywords: escaped_searchstring}, false).limit(default_limit)
    else
      @plans = Plan.limit(default_limit)
    end

    if params[:newlayout] == '1'

      if @plans.nil?
          @plans = Plan.all
      end

     if params[:sort] == 'popular'
        @plans = @plans.order('(SELECT stats.viewcount FROM stats WHERE plans.stat_id = stats.id LIMIT 1) DESC')
     else
        @plans = @plans.order('plans.created_at DESC')
     end



      #if params[:paginate] == '1'
      #  @plans = Plan.paginate(:page => params[:page], :per_page => default_limit)
      #  @paging_on = true
      #end

      render :partial => "plans/planlist2", :layout => false, :locals => {:paging_on => false}
    else
      render :layout => false
    end
  end

  def track(plan)

    if (!plan || plan.nil?)
      return
    end

    @stat = plan.stat
    if (!@stat || @stat.nil?)
      @stat = Stat.new
      @stat.plan = plan
      @stat.viewcount = 0
      @stat.votecount = 0

      plan.stat = @stat
      plan.save
    end

    @stat.viewcount += 1
    @stat.save

  end

  # GET /plans/new
  # GET /plans/new.xml
  def new
    @plan = Plan.new
    @plan.language = request.headers["accept-language"]

    if params[:situation_id]
      @plan.situation_id = params[:situation_id]
    end

    respond_to do |format|

      format.html { render :action => "new", :layout => "layouts/application2" }
      format.xml  { render :xml => @plan }
    end
  end

  # GET /plans/1/edit
  def edit
    @plan = Plan.find(params[:id])

    @user = current_user
    if !@user || @user != @plan.user
      redirect_to root_url, :alert => 'You are not allowed to edit this answer.'
      return
    end

    track(@plan)

    render :action => "edit", :layout => "layouts/application2"
  end

  # POST /plans
  # POST /plans.xml
  # POST /plans.json
  def create
    session.delete("redirect")

    @plan = Plan.new(params[:plan])

    if !@plan.content.nil?
      @plan.content = @plan.content.strip
      if @plan.content.length > 0
        @plan.content = @plan.content.slice(0,1).capitalize + @plan.content.slice(1..-1)
      end
    end

    @plan.user = current_user
    user_last_activity(@plan.user)
    @plan.situation_id = @plan.new_situation_id

    @plancondition = PlanCondition.new(params[:plancondition])

    if (@plancondition)
      @plancondition.plan = @plan
      if @plancondition.whendate.blank?
        @plancondition.whendate = DateTime.now.advance(:days => 7)
      end
      @plancondition.save
      @plan.plancondition_id = @plancondition.id
    end


    if (@plan.new_situation_id && @plan.new_situation_id != '')
      @planB = PlanB.new
      @planB.situation = Situation.find(@plan.new_situation_id)
      @planB.plan = @plan
      @planB.keywords = @plan.planB_keywords
    else
      @planB = nil
    end

    this_user_id = (current_user == nil) ? 0 : current_user.id

    # badge category pro
    @result = Plan.find_by_sql("SELECT situations.category_id, COUNT( * ) AS plan_total
                                FROM plans INNER JOIN plan_bs ON plans.id = plan_bs.plan_id
                                JOIN situations ON situations.id = plan_bs.situation_id
                                WHERE plans.user_id = #{this_user_id}
                                GROUP BY situations.category_id
                                HAVING COUNT( * ) = 4")

  @result.each do |cat|
    if cat.category_id.to_i != 0
      @catname = Category.find(cat.category_id.to_i).name
      @catname = @catname.gsub(" ", "-").downcase + "-pro"
      @badge = Badge.find {|b| b.name == @catname}
      @badge.each do |b|
        Badge.find(b.id).grant_to(current_user)
      end
    end
  end

  #if (current_user.nil?)
  #  session[:new_plan] = @plan.id
  #  print 'new_plan: '
  #  puts session[:new_plan]
  #end

  respond_to do |format|
    if @plan.save && (@planB.nil? || @planB.save)  && (@plancondition.nil? || @plancondition.save)
      if params[:redirect] && !params[:redirect].blank?
        situation_id = params[:plan][:new_situation_id]
        session[:redirect] = { :path => "/plans/#{@plan.id}/complete_after_sign_in", :situation_id => situation_id }
        format.html { redirect_to params[:redirect] }
      else
        if !@planB.nil? && !@planB.situation.nil? && !@planB.situation.user.nil?
          @user_asker = @planB.situation.user
          @situation = @planB.situation
          @responder = current_user
          UserMailer.add_answer(@user_asker, @situation, @plan, @responder).deliver

          asker = @planB.situation.user.nil? ? nil : @user_asker.id
          responder = current_user.nil? ? nil : @responder.id
        end

        if (!@planB.nil?)
          Notification.create(:sender => @plan.user ,:recipient => (@planB.situation.nil? ? @plan.user : @planB.situation.user),
                              :situation_id => @plan.situation.id, :plan_b_id => @plan.id,
                              :notification_type => Notification::NOTIFICATION_TYPE[:urgent],
                              :event => Notification::EVENT[:answered_a_question],
                              :expires_at => DateTime.now + 3.days,
                              :unseen => 1)

        end

        format.html { redirect_to( ( @planB.nil? ? @plan : @planB.situation), :notice => 'Answer was successfully created.') }
        format.xml  { render :xml => @plan, :status => :created, :location => @plan }
        format.json { render :json => @plan }
      end
    else
      format.html { render :action => "new" , :layout => "layouts/application2"}
      format.xml  { render :xml => @plan.errors, :status => :unprocessable_entity }
      format.json { render :json => @plan.errors, :status => :unprocessable_entity }
    end
  end
  end

  # GET /plans/1/complete_after_sign_in
  def complete_after_sign_in
    situation = Situation.find(session[:redirect][:situation_id])
    situation_id =  session[:redirect][:situation_id]

    plan = Plan.find(params[:plan_id])
    plan.user = current_user
    plan.save
    user_last_activity(plan.user)

    unless situation.user.nil?
      UserMailer.add_answer(situation.user, situation, plan, plan.user).deliver
    end

    situation_asker = situation.user.nil? ? nil : situation.user.id
    plan_user = plan.user.nil? ? nil : plan.user.id

    Notification.create(:sender_id => plan_user, :recipient_id => situation_asker,
                        :situation_id => plan.situation.id, :plan_b_id => plan.id,
                        :notification_type => Notification::NOTIFICATION_TYPE[:urgent],
                        :event => Notification::EVENT[:answered_a_question],
                        :expires_at => DateTime.now + 3.days)

    redirect_to "/situations/#{situation_id}", :notice => 'Answer was successfully created.'
  end

  # PUT /plans/1
  # PUT /plans/1.xml
  def update
    @plan = Plan.find(params[:id])

    track(@plan)

    unless current_user.nil?
      User.increment_counter :update_plans, current_user.id
      user_last_activity(current_user)
    end

    respond_to do |format|
      if @plan.update_attributes(params[:plan])
        # Notification.create(
        #   :sender_id => @plan.user.id,
        #   :recipient_id => @plan.situation.user.id,
        #   :situation_id => @plan.situation.id,
        #   :note_id => nil,
        #   :plan_b_id => @plan.id,
        #   :notification_type => Notification::NOTIFICATION_TYPE[:normal],
        #   :event => Notification::EVENT[:edited],
        #   :event_target => Notification::EVENT_TARGET[:own_answer],
        #   :expires_at => @plan.created_at + 3.days
        # )

        if (!@plan.nil? && !@plan.situation.nil?)
          Notification.create(:sender_id => @plan.user.id, :recipient_id => @plan.user.id,
                              :situation_id => @plan.situation.id, :plan_b_id => @plan.id,
                              :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                              :event => Notification::EVENT[:edited_an_answer],
                              :expires_at => DateTime.now + 3.days)
        end


        format.html { redirect_to(@plan, :notice => 'Answer was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @plan.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /plans/1
  # DELETE /plans/1.xml
  def destroy
    @plan = Plan.find(params[:id])

    @user = current_user
    if !@user || (@user != @plan.user && !@user.admin_enabled)
      redirect_to root_url, :alert => 'You are not allowed to delete this answer.'
      return
    end

    @plan.destroy

    respond_to do |format|
      format.html { redirect_to(plans_url) }
      format.xml  { head :ok }
    end
  end

  def rate
    @plan = Plan.find(params[:id])
    @plan.rate(params[:stars], current_user) # or any user instance

    render :text => "OK"
  end

  def vote_up
    @plan = Plan.find(params[:id])
    if (!current_user.nil? && !@plan.nil?)
      current_user.up_vote!(@plan)
    end
    render :text => @plan.votes unless @plan.nil?
  end

  def vote_down
    @plan = Plan.find(params[:id])
    if (!current_user.nil? && !@plan.nil?)
      current_user.down_vote!(@plan)
    end
    render :text => @plan.votes unless @plan.nil?
  end

  def updatecategory
    @plan = Plan.find(params[:id])
    @category = Category.find(params[:category_id])
    if (@plan && @category)
      @plan.category = @category
      @plan.save
    end
    render :text => "OK"
  end

end

