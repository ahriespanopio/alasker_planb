class BadgesController < ApplicationController

  def index
      @badge = Badge.all 
      render :action => "index", :layout => "layouts/application2"
  end

  def show
      @badge = Badge.find(params[:id].to_i)
      @user = User.find_by_sql("SELECT users.id, users.username, users.location, users.website, users.email, users.use_uploadedPic, users.image FROM `users`INNER JOIN sashes ON users.sash_id = sashes.id JOIN badges_sashes On sashes.id = badges_sashes.sash_id where badges_sashes.badge_id = #{params[:id].to_i}")
      render :action => "show", :layout => "layouts/application2", :locals => {:content_wide => true}
  end


end
