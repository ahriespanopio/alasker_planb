class ApplicationController < ActionController::Base
  # include PublicActivity::StoreController
  config.relative_url_root = ""
  before_filter :check_redirect_key, :set_access_control_headers, :default_faye_url_options
  rescue_from ActiveRecord::RecordNotFound, :with => :rescue_action_in_public
  rescue_from ActionController::RoutingError, :with => :rescue_action_in_public
  rescue_from Ambry::NotFoundError, :with => :rescue_action_in_public

  def after_sign_in_path_for(resource)
    if session[:redirect].nil?
      stored_location_for(resource) || signed_in_root_path(resource)
    else
      session[:redirect][:path]
    end
  end

  def routing_error
    raise ActionController::RoutingError.new(params[:path])
  end

  def default_url_options
    if Rails.env.production?
      {:host => "hidden-badlands-6875.herokuapp.com"}
    elsif Rails.env.development?
      {:host => "alasker.local:3000"}
    else
      {:host => "testing.alasker.com"}
    end
  end

  def default_faye_url_options
    if Rails.env.production?
      @faye_host = "http://alaskerfaye.herokuapp.com"
    elsif Rails.env.development?
      @faye_host = "http://alasker.local:9292"
    else
      @faye_host = "http://alaskerfaye.herokuapp.com"
    end
  end

  def set_access_control_headers
	   headers["Access-Control-Allow-Origin"]  = "*"
     headers["Access-Control-Allow-Methods"] = %w{GET POST PUT DELETE}.join(",")
     headers["Access-Control-Allow-Headers"] = %w{Origin Accept Content-Type X-Requested-With X-CSRF-Token}.join(",")
     head(:ok) if request.request_method == "OPTIONS"
  end

  def check_redirect_key

    if Rails.env.production? && !headers["host"].nil? && "alasker.com".casecmp(headers["host"]) == 0
      redirect_to "http://www.alasker.com"
    end

    if request[:access_key] != 'p1an6' && cookies[:access_key] != 'p1an6' &&
       request[:access_key] != 'sandy70' && cookies[:access_key] != 'sandy70'
       #redirect_to "/404.html"
       redirect_to "http://signup.alasker.com"
    elsif request[:access_key] == 'p1an6'
      cookies.permanent[:access_key] = 'p1an6'
      if Rails.env.production?
        #cookies.permanent[:access_key] = {:value => 'p1an6', :domain => ".alasker.com"}
        cookies.permanent[:access_key] = {:value => 'p1an6'}
      end
    elsif request[:access_key] == 'sandy70'
      cookies.permanent[:access_key] = 'sandy70'
      if Rails.env.production?
        #cookies.permanent[:access_key] = {:value => 'sandy70', :domain => ".alasker.com"}
        cookies.permanent[:access_key] = {:value => 'sandy70'}
      end
    end
  end

  helper :all, :layout
  protect_from_forgery

  #helper_method :current_user
  #private

  #def current_user_session
  #  return @current_user_session if defined?(@current_user_session)
  #  @current_user_session = UserSession.find
  #end

  #def current_user
  #  return @current_user if defined?(@current_user)
  #  @current_user = current_user_session && current_user_session.record
  #end

  def omniauth
  # render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
    render :file => "/public/404.html", :status => 404, :layout => false
  end

  private

  # handles 404 when a record is not found.
  def rescue_action_in_public(exception)
    case exception
    when ActiveRecord::RecordNotFound, ActionController::UnknownAction, ActionController::RoutingError, Ambry::NotFoundError
       redirect_to :controller => 'home', :action => 'error404'
    else
      super
    end
  end

  def user_last_activity(alasker_user)
    unless alasker_user.nil?
      alasker_user.last_activity = DateTime.now
      alasker_user.save
    end
  end

  def protect_against_forgery?
    unless request.format.json?
      super
    end
  end

end
