class NotificationsController < ApplicationController
  respond_to :json
  include NotificationsHelper

  def getLatestCount
    unseen = 0

    if current_user
      #unseen = Notification.where("recipient_id = ? and event in (3, 8, 6)", current_user.id).count()
      # 3 - answered a question
      # 8 - followed a user
      # 16 - messaged a user
      unseen = Notification.count(:all,
                                  :conditions => ["recipient_id = ? and event in (3, 8, 13, 16) and unseen", current_user.id])
    end

    render :json => { :count => unseen }
  end

  def getLatest
    pivot = 'top'
    delta = "null"
    limit = 15

    unless params[:pivot].nil?
      pivot = params[:pivot]
    end

    unless params[:delta].nil?
      delta = params[:delta]
    end

    unless params[:limit].nil?
      limit = params[:limit]
    end

    unless current_user.nil?

      if delta.eql? "null"
        notifications = Notification.where("recipient_id = ? and sender_id <> ? and event in (3, 8, 13, 16)", current_user.id, current_user.id)
                          .order("created_at DESC")
                          .limit(limit)
      else
        mysql_date = Time.parse(delta).utc.to_s(:db)
        mysql_date = Time.parse(mysql_date).strftime("%Y-%m-%d %H:%M:%S")

        if pivot.eql? "top"
          notifications = Notification.where("recipient_id = ? and sender_id <> ? and event in (3, 8, 13, 16) and created_at > ?", current_user.id, current_user.id, mysql_date)
                          .order('created_at DESC')
                          .limit(limit)
        else
          notifications = Notification.where("recipient_id = ? and sender_id <> ? and event in (3, 8, 13, 16) and created_at < ?", current_user.id, current_user.id, mysql_date)
                            .order('created_at DESC')
                            .limit(limit)
        end
      end

      render :json => prepare_notifications(notifications)
    else
      render :json =>  { :items => 0, :count => 0 }
    end
  end

end
