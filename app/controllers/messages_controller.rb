class MessagesController < ApplicationController
  # GET /messages
  # GET /messages.xml
  def index
    if current_user
      @messages = Message.all
      @inbox = Message.where(:receiver_id => current_user.id, :label => "inbox")
      @sent = Message.where(:sender_id => current_user.id, :label => "sent")
      @unread = @inbox.where(:read => false)

      respond_to do |format|
        format.html { render :action => "index", 
                             :layout => "layouts/application2"
                    } # index.html.erb
        # format.json  { render :json => @messages }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /messages/1
  # GET /messages/1.xml
  def show
    @message = Message.find(params[:id])
    @sender = Message.find_user(@message.sender_id)
    @receiver = Message.find_user(@message.receiver_id)

    if current_user
      if current_user.id == @message.receiver_id && @message.label == "inbox"
        @message.read = true
        @message.save
      end

      respond_to do |format|
        format.html { render :action => "show", 
                             :layout => "layouts/application2"
                    } # show.html.erb
        format.json  { render :json => @message }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /messages/new
  # GET /messages/new.xml
  def new
    if current_user
      @message = Message.new

      respond_to do |format|
        format.html { render :action => "new", 
                             :layout => "layouts/application2"
                    } # new.html.erb
        format.json  { render :json => @message }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /messages/1/edit
  def edit
    if current_user
      @message = Message.find(params[:id])
    else
      redirect_to '/users/sign_in'
    end
  end

  # POST /messages
  # POST /messages.xml
  def create
    @message = Message.new(params[:message])
    @message.label = "sent"

    msg = @message.clone
    msg.label = "inbox"
    msg.save

    @user = User.find(@message.receiver_id)

    respond_to do |format|
      if @message.save
        UserMailer.send_message(@user, current_user, @message.content.strip).deliver

        Notification.create(:sender_id => current_user.id ,:recipient_id => @user.id,
                            :situation_id => nil, :plan_b_id => nil,
                            :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                            :event => Notification::EVENT[:messaged],
                            :expires_at => DateTime.now + 3.days)

        format.html { redirect_to(@message, :notice => 'Message was successfully sent.') }
        format.xml  { render :xml => @message, :status => :created, :location => @message }
      else
        format.html { render :action => "new", :layout => "layouts/application2" }
        format.xml  { render :xml => @message.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /messages/1
  # PUT /messages/1.xml
  def update
    @message = Message.find(params[:id])

    respond_to do |format|
      if @message.update_attributes(params[:message])
        format.html { redirect_to(@message, :notice => 'Message was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit", :layout => "layouts/application2" }
        format.xml  { render :xml => @message.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /messages/1
  # DELETE /messages/1.xml
  def destroy
    @message = Message.find(params[:id])
    @message.destroy

    respond_to do |format|
      format.html { redirect_to(messages_url, flash[:notice] = "Message deleted!") }
      format.xml  { head :ok }
    end
  end

end