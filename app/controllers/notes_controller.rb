class NotesController < ApplicationController
  include NotesHelper
  respond_to :json

  # GET /situations/1/notes.json
  def index
    #notes = Note.find_all_by_situation_id(params[:situation_id])
    #render :json => prepare_notes(notes.flatten())

    delta = "null"

    unless params[:delta].nil?
      delta = params[:delta]
    end

    if delta.eql? "null"
      notes = Note.find_all_by_situation_id(params[:situation_id])
    else
      mysql_date = Time.parse(delta).utc.to_s(:db)
      mysql_date = Time.parse(mysql_date).strftime("%Y-%m-%d %H:%M:%S")
      notes = Note.where(:situation_id => params[:situation_id])
      .where("created_at > ?", mysql_date)
    end

    if notes.length > 0
      render :json => prepare_notes(notes.flatten())
    else
      render :json => { :items => {}, :count => 0 }
    end
  end


  # POST /notes.json
  def create
    new_note = Note.new(params[:note])

    new_note.user = current_user

    @situation = new_note.situation

    receiver = @situation.user_id

    if new_note.save
      unless current_user.nil?
        Notification.create(:note_id => new_note.id, :sender_id => current_user.id, :recipient_id => receiver,
                            :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                            :event => Notification::EVENT[:chatted],
                            :expires_at => DateTime.now + 3.days)

        UserMailer.add_note(new_note).deliver
      end

      render :json => new_note
    else
      render :json => new_note.errors, :status => :unprocessable_entity
    end
  end

  # PUT /notes/1.json
  def update
    note = Note.find_by_id(params[:id])

    if note.update_attributes(params[:note])
      render :json => note
    else
      render :json => note.errors, :status => :unprocessable_entity
    end
  end

  # DELETE /notes/1.json
  def destroy
    note = Note.find_by_id(params[:id])

    if !note.nil? and note.destroy
      respond :json => {:success => true}
    else
      respond :json => {:success => false}
    end

  end
end
