require 'json'

class CategoriesController < ApplicationController
  respond_to :json
  add_breadcrumb "All", :categories_path

  # GET /categories
  # GET /categories.xml
  # GET /categories.json

  def index
    user_last_activity(current_user)
    @categories = Category.where("is_private = false AND name LIKE ?", "%#{params[:q]}%")

    respond_to do |format|
      format.html { render :action => "index", :layout => "layouts/application2" }
      format.json { render :json => @categories.map(&:attributes)}
    end
  end

  # GET /categories/1
  # GET /categories/1.xml
  def show
    @category = Category.find(params[:id])
    @categories = Category.where('is_private = false')

    add_breadcrumb @category.name, @category

    if params[:format] =~ /(js|json)/
      respond_with(@category )
    else
      render :action => "show", :layout => "layouts/application2"
    end

  end

  def search
    escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')
    @categories = Category.where("content LIKE ?", "%" + escaped_searchstring + "%" ).all
    @category = Category.find_by_content(params[:searchstring])

    if ! @category
      @category = Category.new(params[:category])
    end

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @category }
    end
  end

  #this should get moved out of categories controller
  def findrelated

    @categories = Category.where('is_private = false')
    escaped_searchstring = ''
    @situations = Situation.joins("inner join situation_conditions on situation_conditions.situation_id = situations.id")
    @relatedwhere = nil
    @whendate = nil

    if params[:search_whendate] && !params[:search_whendate].nil?
      escaped_searchstring = params[:search_whendate].gsub('%','\%').gsub('_', '\_')
      @situations = @situations.where('DATE(situation_conditions.whendate) = DATE(?)', DateTime.parse(escaped_searchstring).to_s(:db))
      @whendate = escaped_searchstring
    elsif params[:search_when] && !params[:search_when].nil?
      escaped_searchstring = params[:search_when].gsub('%','\%').gsub('_', '\_')
      @situations = @situations.select('distinct(situations.id), situations.*').where('situation_conditions.whenkeyword LIKE ?', "%" + escaped_searchstring + "%")
      #@situations = SituationCondition.where(
      #    "whenkeyword LIKE ?",
      #  "%" + escaped_searchstring + "%" ).all

    elsif params[:search_where] && !params[:search_where].nil?
      escaped_searchstring = params[:search_where].gsub('%','\%').gsub('_', '\_')
      @situations = @situations.select('distinct(situations.id), situations.*').where('situation_conditions.where LIKE ?', "%" + escaped_searchstring + "%")
      @relatedwhere = params[:search_where]
    elsif params[:search_what] && !params[:search_what].nil?
      escaped_searchstring = params[:search_what].gsub('%','\%').gsub('_', '\_')
      @situations = @situations.select('distinct(situations.id), situations.*').where('situation_conditions.what LIKE ?', "%" + escaped_searchstring + "%")

    else
      @situations = nil
    end

    #add_breadcrumb '| ' + escaped_searchstring + ' |', @situations
    render :action => "findrelated", :layout => "layouts/application2"
  end

  # GET /categories/new
  # GET /categories/new.xml
  def new
    @category = Category.new
    @category.language = request.headers["accept-language"]

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @category }
    end
  end

  # GET /categories/1/edit
  def edit
    @category = Category.find(params[:id])
  end

  # POST /categories
  # POST /categories.xml
  def create
    @category = Category.new(params[:category])

    respond_to do |format|
      if @category.save
        format.html { redirect_to(@category, :notice => 'Category was successfully created.') }
        format.xml  { render :xml => @category, :status => :created, :location => @category }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /categories/1
  # PUT /categories/1.xml
  def update
    @category = Category.find(params[:id])

    respond_to do |format|
      if @category.update_attributes(params[:category])
        format.html { redirect_to(@category, :notice => 'Category was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @category.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /categories/1
  # DELETE /categories/1.xml
  def destroy
    @category = Category.find(params[:id])
    @category.destroy

    respond_to do |format|
      format.html { redirect_to(categories_url) }
      format.xml  { head :ok }
    end
  end

end

