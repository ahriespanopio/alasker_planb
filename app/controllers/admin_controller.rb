#require 'classifier'
#require 'madeleine'
require 'rubygems'
require 'k_means'
require 'geocoder'

class AdminController < ApplicationController
  def index
  	@user = current_user
  	if !@user || !@user.admin_enabled
      redirect_to :root, :notice => "ACCESS DENIED! Please contact the Alasker administrators."
    else
      render(:action => "index", :layout => "layouts/application2")
  	end

  end

  R = 6378.1370

  def convertcordinates(coordinates, element)
    lat = coordinates[0]
    lon = coordinates[1]
    x = R * Math.cos(lat) * Math.cos(lon)
    y = R * Math.cos(lat) * Math.sin(lon)
    z = R * Math.sin(lat)

    element.push(x)
    element.push(y)
    element.push(z)
  end

  def buildclusterlist

    @counter = 0
    @errorvalue = ''

    @situations = Situation.all
    data = Array.new()
    index = Array.new()

    @situations.each do |situation|
      element = Array.new()
      element.push((situation.created_at - DateTime.now).to_f)
      element.push((situation.updated_at - DateTime.now).to_f)
      if !situation.situationcondition.nil? && !situation.situationcondition.where.nil? && !situation.situationcondition.where.empty?
        coordinates = Geocoder.coordinates(situation.situationcondition.where)
        convertcordinates(coordinates, element)
      else
        convertcordinates([Random.rand(0-90),Random.rand(0-90)], element)
      end

      data.push(element)
      index.push(situation.id)
    end

    if (params[:k] && !params[:k].nil?)
      k = params[:k].to_i
    else
      k = 5
    end

    if (params[:n] && !params[:n].nil?)
      n = params[:n].to_i
    else
      n = 20
    end

    @errorvalue = data.to_s.truncate(2000)

    n.times do
      @kmeans = KMeans.new(data, :centroids => k)
    end

    groupindex = 0
    SituationCluster.delete_all
    @kmeans.view.each do |node|
      node.each do |i|
        situation_id = index[i]
        situation = Situation.find_by_id(situation_id)
        situationcluster = SituationCluster.new
        situationcluster.centroid = groupindex
        situationcluster.name = situation.content.truncate(30)
        situationcluster.created = situation.created_at
        situationcluster.situation = situation
        situationcluster.save
        @counter += 1
      end
      groupindex += 1
    end

    render(:action => "buildclusterlist", :layout => "layouts/application2")
  end

  def makecategories
  	@user = current_user

	  @category = Category.find_by_name("Aid")
	  if @category
        @category = Category.find_by_name("alasker")
        
        @category = Category.new
        @category.language = "en-us"
        @category.name = "Totally random"
        @category.save
      
        @category = Category.new
        @category.language = "en-us"
        @category.name = "alasker"
        @category.save
	    
        
	  end 

=begin
	  @category = Category.find_by_name("Entertainment")

	  if ! @category

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Entertainment"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Travel"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Health"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Computers"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Cooking"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Everyday"
	    @category.save
	  end

	  @category = Category.find_by_name("Beauty & Style")

	  if ! @category

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Beauty & Style"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Cars"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Electronics"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Sports"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Business"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Everyday"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Games"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Pets"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Politics"
	    @category.save

	    @category = Category.new
	    @category.language = "en-us"
	    @category.name = "Science"
	    @category.save
    end
=end

  @category = Category.new
  @category.language = "en-us"
  @category.name = "Aid"
  @category.save

  @category = Category.new
  @category.language = "en-us"
  @category.name = "Cars"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Food"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Getting around"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Health"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Insurance"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Money"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Pets"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Repairs"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Shopping"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Sports"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Technology"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Travel"
  @category.save
  
  @category = Category.new
  @category.language = "en-us"
  @category.name = "Volunteering"
  @category.save
  
  if !@user || !@user.admin_enabled
      redirect_to :root, :notice => "ACCESS DENIED! Please contact the Alasker administrators."
    else
      redirect_to(:action => "index", :layout => "layouts/application2", :notice => 'Categories successfully created.')
    end

  end

  def retrainclassifier

    ensure_situation_for_each_category()
    @counter = 0
    @categorycounter = 0
    @situations = Situation.all
    @categories = Category.all
    category_array = Array.new

    @categories.each do |category|
      if category != nil && category.name != nil
        # seems to break on spaces and & symbol
        categoryname = strip_category_spaces(category.name)
        category_array.push categoryname
        @categorycounter += 1
      end
    end

    @errorvalue = ''
    if (@categorycounter > 0)
      #b= Classifier::Bayes.new "Entertainment", "Cooking", "Electronics", "Pets", "Travel", "Everyday", "Sports", "Politics", "Health", "Beauty", "Business", "Science", "Computers", "Cars", "Games"
      b = Classifier::Bayes.new
      category_array.each do |categoryname|
       b.add_category categoryname
      end
      m = SnapshotMadeleine.new("bayes_data") {
        b
      }

      @situations.each do |situation|
        if !situation.nil?  && !situation.category.nil? && !situation.category.name.empty? && !situation.content.nil?
          categoryname = strip_category_spaces(situation.category.name)
          begin
            if (category_array.include?(categoryname))
              m.system.train categoryname, situation.content
              @counter += 1
            end
          rescue
        end
      end
    end

  m.take_snapshot
  end

render(:action => "retrainclassifier", :layout => "layouts/application2")
end



  def makesituations

  	@user = current_user
  	if !@user || !@user.admin_enabled
      redirect_to :root, :notice => "ACCESS DENIED! Please contact the Alasker administrators."
  	end

	  @situation = Situation.find_by_content("You're going to a birthday party to bring a gift!")
	  @category = Category.find_by_name("Entertainment")

	  if !@situation && @category

	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "Where do I recharge my phone near Murray Hill?"
	    @situation.category = Category.find_by_name("Technology")
	    @situation.save

	@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 2
    	@stat.save


	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "How do I flush my toilet when the water shuts off?"
	    @situation.category = Category.find_by_name("Repairs")
	    @situation.keywords = "travel;passport"
	    @situation.save

	@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 5
    	@stat.save


	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "You have a flight that gets canceled, what to do?"
	    @situation.category = Category.find_by_name("Travel")
	    @situation.keywords = "travel;cancel"
	    @situation.save

	@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 10
    	@stat.save

	  redirect_to(:action => "index", :layout => "layouts/application2", :notice => 'Situations successfully created.')
    end
  end

  def makesomeplanswithstats
  	@user = current_user
  	if !@user || !@user.admin_enabled
      redirect_to :root, :notice => "ACCESS DENIED! Please contact the Alasker administrators."
    end

	@situation = Situation.find_by_content("You're walking down the street, and you find a 20 dollar bill")
	@category = Category.find_by_name("Entertainment")

	if !@situation && @category

	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "You're walking down the street, and you find a 20 dollar bill"
	    @situation.category = Category.find_by_name("Everyday")
	    @situation.keywords = ""
	    @situation.save

		@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 900
    	@stat.save

	    	@plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.content = "You look around to see if anyone else has just walked by the vicentity."
		    @plan.situation = @situation
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 20
	    	@stat.save


		    @plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.situation = @situation
		    @plan.content = "You look to see if there is any other material near the bill that would link it back to the person."
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 30
	    	@stat.save


		    @plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.situation = @situation
		    @plan.content = "You give up and keep it."
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 100
	    	@stat.save


	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "You're out sailing and the wind dies.  The boat does not have a motor."
	    @situation.category = Category.find_by_name("Sports")
	    @situation.keywords = ""
	    @situation.save

		@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 2200
    	@stat.save

	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "I am sailing and the wind dies.  The boat does not have a motor."
	    @situation.category = Category.find_by_name("Sports")
	    @situation.keywords = "motor"
	    @situation.save

		@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 2200
    	@stat.save

	    	@plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.content = "If the water is warm, you can start swimming and get to shore."
		    @plan.situation = @situation
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 20
	    	@stat.save

	    	@plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.content = "You should signal someone either using flag signals or by radio"
		    @plan.situation = @situation
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 30
	    	@stat.save

	    @situation = Situation.new
	    @situation.language = "en-us"
	    @situation.content = "I am in a boat, and the boat is starting to take on water."
	    @situation.category = Category.find_by_name("Sports")
	    @situation.keywords = ""
	    @situation.save

		@stat = Stat.new
    	@stat.situation = @situation
    	@stat.viewcount = 2200
    	@stat.save

	    	@plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.content = "If you can identify the leak, try to plug the holes with bits of wood and fabric"
		    @plan.situation = @situation
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 4
	    	@stat.save

	    	@plan = Plan.new
	    	@plan.language = "en-us"
		    @plan.content = "Look for a raft, and make sure to get on life jackets"
		    @plan.situation = @situation
		    @plan.save

			@stat = Stat.new
	    	@stat.plan = @plan
	    	@stat.viewcount = 4
	    	@stat.save
	   end

  redirect_to(:action => "index", :layout => "layouts/application2", :notice => 'Plans and Stats successfully created.')

  end

  def strip_category_spaces(category)
     categoryname = category.gsub(" & ", "")
     return categoryname
  end

  # create situations for every category to ensure proper training
  def ensure_situation_for_each_category

   @situation = Situation.find_by_content("I'm out looking for a new movie, but ran into a good film about stars.")


   if !@situation
      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "I'm out looking for a new movie, but ran into a good film about stars."
      @situation.category = Category.find_by_name("Entertainment")
      @situation.keywords = ""
      @situation.save

      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "You are stuck waiting for a flight, but can't get to the airport.  Even the taxis and buses seem stuck."
      @situation.category = Category.find_by_name("Travel")
      @situation.keywords = ""
      @situation.save

      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "I am looking for a doctor to help me because I am sick, but I can't seem to find one here."
      @situation.category = Category.find_by_name("Health")
      @situation.keywords = ""
      @situation.save

      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "My computer just hung.  I am running Windows and can't seem to quit Excel."
      @situation.category = Category.find_by_name("Computers")
      @situation.keywords = ""
      @situation.save

      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "I am baking bread and the forgot to buy yeast."
      @situation.category = Category.find_by_name("Cooking")
      @situation.keywords = ""
      @situation.save

      @situation = Situation.new
      @situation.language = "en-us"
      @situation.content = "I am date with a girl, but forgot my wallet."
      @situation.category = Category.find_by_name("Everyday")
      @situation.keywords = ""
      @situation.save

      @category = Category.find_by_name("Beauty & Style")

      if @category

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I ran out lipstick but I have perfume."
        @situation.category = Category.find_by_name("Beauty & Style")
        @situation.keywords = ""
        @situation.save


        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "My car stopped working due to running out of gas. Need to find a way to bleed the car."
        @situation.category = Category.find_by_name("Cars")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I have a party, but my hi-fi just stopped work.  I think it's related to the power voltage."
        @situation.category = Category.find_by_name("Electronics")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I am playing full back in a soccer game, and the opposing team is playing a strong offense."
        @situation.category = Category.find_by_name("Sports")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "My business needs to take a bank load.  But no bank will give us credit. "
        @situation.category = Category.find_by_name("Business")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I reached level 6 of World of Warcraft.  But I can't reach the next level."
        @situation.category = Category.find_by_name("Games")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "My fish and cat are sick."
        @situation.category = Category.find_by_name("Pets")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I want to become president of my school class, but there is another guy that is more popular."
        @situation.category = Category.find_by_name("Politics")
        @situation.keywords = ""
        @situation.save

        @situation = Situation.new
        @situation.language = "en-us"
        @situation.content = "I need to find a PH 3 solution, but can't seem to make one."
        @situation.category = Category.find_by_name("Science")
        @situation.keywords = ""
        @situation.save

       end
     end
   end

end

