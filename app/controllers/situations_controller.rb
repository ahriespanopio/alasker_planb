require 'net/http'
require 'rubygems'
require 'xmlsimple'
require 'chronic'

class SituationsController < ApplicationController
  include PlansHelper
  include ApplicationHelper
  respond_to :json

  # GET /situations/getLatest.json
  def getLatest
    delta = "null"
    limit = 15

    unless current_user.nil?
      filters = find_filter(current_user)
      # nearby_people = get_nearbyers(current_user, 10)

      if filters
      ### get questions with saved locations near the current_user (distance in kilometers)
      filter_by_location = filter_nearby(current_user, filters)

      ### get category_ids associated with the filter_id
      find_category_ids = find_cats(filters)

      ### get questions under the find_category list
      filter_by_category = filter_categories(find_category_ids, filters)
      end
    end

    unless params[:delta].nil?
      delta = params[:delta]
    end

    unless params[:limit].nil?
      limit = params[:limit]
    end

    if delta.eql? "null"
      if (filters.blank? || filters.nil?) || filters.apply_filter == false ||
        (filters.by_location == 0 && filters.by_category == 0)
        situations = Situation.limit(limit)
      else
        unless current_user.nil?
          if filters.by_location == 1
            situations ||= filter_by_location
            situations.limit(limit) if situations.count > 20
          end
          if filters.by_category == 1
            situations ||= filter_by_category
            situations.limit(limit) if situations.count > 20
          end
        end
      end
    else
      mysql_date = Time.parse(delta).utc.to_s(:db)
      mysql_date = Time.parse(mysql_date).strftime("%Y-%m-%d %H:%M:%S")
      situations = Situation.where("created_at < ?", mysql_date).limit(limit)
    end

    package = format_situations(situations)

    respond_to do |format|
      format.html { render :partial => "situations/situationlist",
                           :layout => false,
                           :locals => { :showtitle => false, :showplans => true, :paging_on => false }
                  }
      format.json { render :json => package }
    end
  end

  # GET /situations
  # GET /situations.xml

  def index
    @categories = Category.all
    # @activities = PublicActivity::Activity.all

    respond_to do |format|
      format.html { render :action => "index",
                           :layout => "layouts/application2",
                           :locals => {:big_answer_askbar => false, :situation_answer_askbar => false}
                  }
      #format.json { respond_with(@situations) }
      format.json {
          if !params[:situationsearch].nil?
            #  @escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')
            content = params[:situationsearch].strip
            #start search here as well
            #@situations = Situation.where("MATCH(content,keywords) AGAINST (?)", content)
            @situations = Situation.basic_search({content: content, keywords: content}, false)
          else
            @situations = Situation.all
          end

          render :json => @situations
        }
    end
  end

  def parse_sentence(sentence = "This is a default sentence")
    @sentence = sentence
    @dates = get_date_phrases(@sentence.split(" "))
  end

  def get_date_phrases(word_array)
    num_of_words = 1;
    date = Array.new
    date_phrases = Array.new
    while num_of_words < word_array.length+1  do
      starting_word = 0
      while starting_word < word_array.length do
        phrase = get_phrase(word_array, starting_word, num_of_words)
        starting_word += 1
        parsed_date = "#{Chronic.parse(phrase, :now => Time.mktime(1000))}"
        #parsed_date = Chronic.parse(phrase, :now => Time.mktime(1000))
        if parsed_date != "" and !date.include?(parsed_date)
          # puts "[#{phrase}] : #{Chronic.parse(phrase)} index: #{date.index(parsed_date)}"
          date.push(parsed_date);
          date_phrases.push(phrase)
        end
      end
      num_of_words +=1
    end
    date_phrases[date_phrases.length-1]
  end

  def get_phrase(word_array, starting_word = 0, num_of_words = 0)
    sub_array = word_array[starting_word, [num_of_words, word_array.length].min]
    return sub_array.join(" ")
  end

  def getWhenKeywords
    new_situationcondition_when = ''
    if !params[:searchstring].nil?
      escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')

      new_situationcondition_when = get_date_phrases(escaped_searchstring.split(" "))
    end

    if new_situationcondition_when.length <= 1
      new_situationcondition_when = ''
    end

    render :text => new_situationcondition_when
  end

  def getWhatKeywords
    new_situationcondition_what = ''

    if !params[:searchstring].nil?
      escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')

      gateway = 'http://api.zemanta.com/services/rest/0.0/'
      res = Net::HTTP.post_form(URI.parse(gateway),
                                {
        'method'=>'zemanta.suggest',
        'api_key'=> 'm3jfjkp8oyibq4vgbz43fzqq',
        'text'=> escaped_searchstring,
        'format' => 'xml'
      })

      new_situationcondition_what = ''
      wordCount = 0
      data = XmlSimple.xml_in(res.body)
      data['keywords'][0]['keyword'].each { |v|
        if wordCount < 5
          if new_situationcondition_what != ''
            new_situationcondition_what << ', '
          end
          new_situationcondition_what << v['name'].join
        end
        wordCount += 1
      }

    end

    render :text => new_situationcondition_what
  end

  def getWhereKeywords
    new_situationcondition_where = ''

    begin
      new_situationcondition_where = request.location.city + ', ' + request.location.state_code + ', ' + request.location.postal_code
      if new_situationcondition_where == ', , '
        new_situationcondition_where = ''
      end
    rescue
    end

    if !params[:searchstring].nil?  && !params[:searchstring].empty?

      escaped_searchstring = params[:searchstring].gsub('%','\%').gsub('_', '\_')

      # Create an AlchemyAPI object.
      alchemyObj = AlchemyAPI.new()
      alchemyObj.setAPIKey('4bc070ce3a62f8da38aa60a47b8f99f5c94dc2d9')

      result = alchemyObj.TextGetRankedNamedEntities(escaped_searchstring)
      data = XmlSimple.xml_in(result, { 'KeyAttr' => 'name' })

      if !data['entities'].nil? && !data['entities'][0].nil? && !data['entities'][0]['entity'].nil?
        data['entities'][0]['entity'].each do |entity|
          if new_situationcondition_where != ''
            new_situationcondition_where << ', '
          end
          new_situationcondition_where << entity['text'].first.to_s
        end
      end

    end

    render :text => new_situationcondition_where
  end

  def next
    @situation = Situation.new(params[:situation])
    @situation.language = request.headers["accept-language"]

    if @situation.language == "" || @situation.language.nil?
      @situation.language = "en-us"
    end

    @situation.content = @situation.content.strip
    if @situation.content.length > 0
      @situation.content = @situation.content.slice(0,1).capitalize + @situation.content.slice(1..-1)
    end
    @situation.user = current_user

    #begin
    #   m = SnapshotMadeleine.new("bayes_data") {
    #        Classifier::Bayes.new "business", "sample category"
    #   }

    #  m.system.train_business "here are some good words. I hope you love them"
    #  m.system.train_sample_category "here are some bad words, I hate you"
    #      m.take_snapshot
    #   	@situation.category = Category.find_by_name(add_category_spaces(m.system.classify(@situation.content)))
    #rescue

    #end

    escaped_searchstring = @situation.content.gsub('%','\%').gsub('_', '\_')
    #@plans = Plan.select('*').where("MATCH(content,keywords) AGAINST (?)", escaped_searchstring).all
    @plans = Plan.basic_search({content: escaped_searchstring, keywords: escaped_searchstring}, false)

    #@situationcondition =  @situation.build_situationcondition
    #@situationcondition.whendate = DateTime.now.advance(:days => 7)
    #@situationcondition.whendate = @situationcondition.whendate.strftime("%m/%d/%Y %I:%M%p")
    #@situationcondition.expires = DateTime.now.advance(:days => 7)
    #@situationcondition.whenkeyword = ''
    #@situationcondition.what = ''
    #@situation.situationcondition = @situationcondition

    bsc = @situation.build_situationcondition
    bsc.whendate = DateTime.now.advance(:days => 1)
    bsc.expires = DateTime.now.advance(:days => 1)

    if @situation.save
      @onliners = get_onliners
      if @onliners.count > 1
        UserMailer.notify_onliners(current_user, @onliners, @situation).deliver
      end

      begin
        @situationcondition.situation = @situation
        @situationcondition.update_attributes(params[:situationcondition])
        # @situationcondition.where = @situation.situationcondition.where

        # @situationcondition.situation_id = @situation.id
        # @situationcondition.where = request.location.city
        # @situation.save

        unless current_user.nil?
          Notification.create( :sender_id => @situation.user_id, :recipient_id => @situation.user_id,
                             :situation_id => @situation.id,
                             :notification_type => Notification::NOTIFICATION_TYPE[:urgent],
                             :event => Notification::EVENT[:posted_a_question],
                             :expires_at => @situation.created_at)

        end
      rescue
      ensure
        render :action => "next", :layout => "layouts/application2", :locals => {:big_answer_askbar => false, :situation_answer_askbar => true}
      end
    else
      render :action => "start", :layout => "layouts/application2", :locals => {:big_answer_askbar => false, :situation_answer_askbar => true}
    end
  end


  def listby

    if (!params[:situationsearch].nil?)
      content = params[:situationsearch].strip
      @escaped_searchstring = content
      #start search here as well
      #@situations = Situation.where("MATCH(content,keywords) AGAINST (?)", content)
      @situations = Situation.basic_search({content: content, keywords: content}, false)
    else
      @situations = Situation.limit(20);
    end

    render :layout => false

  end


  def quickshow
    @situation = Situation.find(params[:id])

    render :action => "quickshow", :layout => false, :locals => {:big_anwser_askbar => false}

  end

  # GET /situations/1
  # GET /situations/1.xml
  # GET /situations/1.json
  def show

    # @activity = PublicActivity::Activity.find(params[:id])

    #sanity check
    session.delete("redirect")

    @situation = Situation.find(params[:id])
    payload = format_situation(@situation)

    if !(@situation.situationcondition.nil?)
      payload[:situationcondition] = @situation.situationcondition
    end

    if @situation.plan
      payload[:plan] = format_plan(@situation.plan)
    end

    if @situation.plans.any?
      payload[:plans] = format_plans(@situation.plans)
    end

    @plan = Plan.new
    @plan.plancondition = PlanCondition.new
    @plan.plancondition.build

    if !@situation.situationcondition.nil?
      @plan.plancondition.where = @situation.situationcondition.where
      @plan.plancondition.what = @situation.situationcondition.what
      @plan.plancondition.whendate = @situation.situationcondition.whendate
    end

    @categories = Category.all
    @plan.new_situation_id = @situation.id
    @other_plans =  Plan.paginate(:page => params[:page], :per_page => 10)

    track(@situation)

    respond_to do |format|
      format.html { render :action => "show", :layout => "layouts/application2", :locals => {:big_answer_askbar => false} }
      format.json { render :json=> payload }
    end

  end

  def linkaplan
    @situation = Situation.find(params[:plan_b][:situation_id])
    @planbs = PlanB.where("situation_id = :situation_id AND plan_id = :plan_id", {:situation_id => params[:plan_b][:situation_id], :plan_id => params[:plan_b][:plan_id]})

    if (!params[:plan_b].nil? && @planbs.count == 0)
      @planB = PlanB.new
      @planB.situation_id = params[:plan_b][:situation_id]
      @planB.plan_id = params[:plan_b][:plan_id]
      @planB.keywords = params[:plan_b][:keywords]
      @planB.save!

      @plan = Plan.find(params[:plan_b][:plan_id])

      if (!@situation.user.nil?)
        UserMailer.add_answer(@situation.user, @situation, @plan, current_user).deliver
      end

      if (!current_user.nil?)
        User.increment_counter :link_plans, current_user.id
        if current_user.link_plans == 10
          #badge good samaritan
          badge = Badge.find(3)
          badge.grant_to(current_user)
          UserMailer.earn_badge(current_user, badge).deliver
          # Notification.create(
          #   :sender_id => nil,
          #   :recipient_id => current_user.id,
          #   :situation_id => nil,
          #   :note_id => nil,
          #   :plan_b_id => nil,
          #   :notification_type => Notification::NOTIFICATION_TYPE[:normal],
          #   :event => Notification::EVENT[:earned],
          #   :event_target => Notification::EVENT_TARGET[:the_name_badge],
          #   :expires_at => DateTime.now + 3.days
          # )

          Notification.create(:sender_id => nil, :recipient_id => current_user.id,
                              :situation_id => nil, :plan_b_id => plan.id,
                              :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                              :event => Notification::EVENT[:earned_new_badge],
                              :expires_at => DateTime.now + 3.days)
        elsif current_user.link_plans == 15
          #badge expert
          badge = Badge.find(4)
          badge.grant_to(current_user)
          UserMailer.earn_badge(current_user, badge).deliver
          # Notification.create(
          #   :sender_id => nil,
          #   :recipient_id => current_user.id,
          #   :situation_id => nil,
          #   :note_id => nil,
          #   :plan_b_id => nil,
          #   :notification_type => Notification::NOTIFICATION_TYPE[:normal],
          #   :event => Notification::EVENT[:earned],
          #   :event_target => Notification::EVENT_TARGET[:the_name_badge],
          #   :expires_at => DateTime.now + 3.days
          # )

          Notification.create(:sender_id => nil, :recipient_id => current_user.id,
                              :situation_id => nil, :plan_b_id => plan.id,
                              :notification_type => Notification::NOTIFICATION_TYPE[:normal],
                              :event => Notification::EVENT[:earned_new_badge],
                              :expires_at => DateTime.now + 3.days)
        end
      end
    end
    redirect_to :action => "show", :id => params[:plan_b][:situation_id]
  end

  # GET /situations/new
  # GET /situations/new.xml
  def new
    @situation = Situation.new
    @categories =  Category.all

    respond_to do |format|
      format.html { render :layout => "layouts/application2" } # new.html.erb
      format.xml  { render :xml => @situation }
    end
  end

  # GET /situations/1/edit
  def edit
    @situation = Situation.find(params[:id])

    @user = current_user
    if !@user || @user != @situation.user
      redirect_to root_url, :alert => 'You are not allowed to edit this question.'
      return
    end

    track(@situation)
    render :action => "edit", :layout => "layouts/application2",
      :locals => {:big_answer_askbar => false, :situation_answer_askbar => true}
  end

  # POST /situations
  # POST /situations.xml
  # POST /situations.json

  def create
    @situation = Situation.new(params[:situation])
    @situation.language = request.headers["accept-language"]

    if @situation.language == "" || @situation.language.nil?
      @situation.language = "en-us"
    end

    @situation.content = @situation.content.strip
    @situation.user = current_user
    @situation.build_situationcondition
    @situationcondition = @situation.situationcondition
    @situationcondition.whendate = DateTime.now.advance(:days => 7)
    #@situationcondition.whendate = @situationcondition.whendate.strftime("%m/%d/%Y %I:%M%p")
    @situationcondition.expires = DateTime.now.advance(:days => 7)
    @situationcondition.whenkeyword = ''

    respond_to do |format|
      if @situation.save
        response = @situation.attributes
        response[:situationcondition] = @situationcondition
        format.html { redirect_to(@situation, :notice => 'Question was successfully created.') }
        format.xml  { render :xml => @situation, :status => :created, :location => @situation }
        format.json { render :json => response }
      else
        format.html { render :action => "new" , :layout => "layouts/application2"}
        format.xml  { render :xml => @situation.errors, :status => :unprocessable_entity }
        format.json { render :json => @situation.errors, :status => :unprocessable_entity }
      end
    end

  end

  # redirect here complete post after signin
  def complete_after_sign_in
    id =  params[:situation_id]
    @situation = Situation.find(id)
    @situation.user_id = current_user.id
    @situation.save
    user = User.find(@situation.user_id)
    user_last_activity(user)

    Notification.create( :sender_id => @situation.user_id, :recipient_id => @situation.user_id,
                        :situation_id => @situation.id,
                        :notification_type => Notification::NOTIFICATION_TYPE[:urgent],
                        :event => Notification::EVENT[:posted_a_question],
                        :expires_at => @situation.situationcondition.whendate)

    redirect_to "/situations/#{id}"
  end

  # POST /situations
  # POST /situations.xml
  def start
    #@user = current_user
    #if @user
    @situation = Situation.new(params[:situation])
    @situation.content = @situation.content.strip
    @situation.user = current_user
    #sanity check
    session.delete("redirect")

    #start search here as well
    escaped_searchstring = @situation.content.gsub('%','\%').gsub('_', '\_')
    #@situations = Situation.where("MATCH(content,keywords) AGAINST (?)", escaped_searchstring).all
    @situations = Situation.basic_search({content: escaped_searchstring, keywords: escaped_searchstring}, false)

    render :action => "start", :layout => "layouts/application2", :locals => {:big_answer_askbar => false, :situation_answer_askbar => true}

    #else
    #  session[:redirect] = { :content => params[:situation][:content], :path => "/situations/resume_start"  }
    #  flash[:notice] = "You need to sign in first before continuing with your question!"
    #  redirect_to '/users/sign_in'
    #end
  end

  def updatecategory
    @situation = Situation.find(params[:id])
    @category = Category.find(params[:category_id])
    if (@situation && @category)
      if (!@situation.category.nil? && @situation.category.id != @category.id)
        @situation.categories << @category
      else
        @situation.category = @category
      end
      @situation.save
    end
    render :text => "OK"
  end


  # PUT /situations/1
  # PUT /situations/1.xml
  # PUT /situations/1.json
  def update
    @situation = Situation.find(params[:id])
    user_last_activity(current_user)

    if (params[:plan] && !params[:plan].nil?)
      @plan = Plan.new(params[:plan])
      @plan.content = @plan.content.gsub('%','\%').gsub('_', '\_')
      if (@plan.content != '')
        @plan.situation_id = @situation.id
        @plan.save
      end
    end

    sc = params[:situationcondition]
    if sc[:id].nil?
       @situationcondition = SituationCondition.new(params[:situationcondition])
       @situationcondition.situation = @situation
       @situationcondition.save
       @situation.situationcondition = @situationcondition
       @situation.situationcondition_id = @situationcondition.id
    else
      @situationcondition = SituationCondition.find_by_id(sc[:id])
      @situation.situationcondition = @situationcondition
      @situation.situationcondition_id = @situationcondition.id
      @situationcondition.update_attributes(params[:situationcondition] )
    end


    respond_to do |format|
      response = @situation.attributes
      if !@plan.nil? and !@plan.id.nil?
        @situation.plan_id = @plan.id
        @situation.plan = @plan
        response[:plan] = @plan
      end
      if !@situationcondition.nil?
        response[:situationcondition] = @situationcondition
      end
      if @situation.update_attributes(params[:situation] )
        #if current_user.nil?
        #  session[:new_situation] = @situation.id
        #  print 'new_situation: '
        #  puts session[:new_situation]
        #end

        if params[:redirect] && !params[:redirect].blank?
          session[:redirect] = { :path => "/situations/#{params[:id]}/complete_after_sign_in" }
          format.html { redirect_to params[:redirect] }
        else
          recipient = current_user.nil? ? nil : @situation.user.id

          Notification.create(:sender_id => recipient, :recipient_id => recipient,
                              :situation_id => @situation.id,
                              :notification_type => Notification::NOTIFICATION_TYPE[:urgent],
                              :event => Notification::EVENT[:updated_a_question],
                              :expires_at => @situation.created_at)

          format.html { redirect_to(@situation, :notice => 'Question was successfully updated.') }
          format.json { render :json => @situation }
        end
      else
        format.html { render :action => "edit", :layout => "layouts/application2" }
        format.json { render :json => @situation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /situations/1
  # DELETE /situations/1.xml
  def destroy
    @situation = Situation.find(params[:id])

    @user = current_user
    if !@user || (@user != @situation.user && !@user.admin_enabled)
      redirect_to root_url, :alert => 'You are not allowed to delete this question.'
      return
    end

    @situation.destroy

    respond_to do |format|
      format.html { redirect_to(situations_url) }
      format.xml  { head :ok }
    end
  end

  def track(situation)

    if (!situation || situation.nil?)
      return
    end

    @stat = situation.stat
    if (!@stat || @stat.nil?)
      @stat = Stat.new
      @stat.situation = situation
      @stat.viewcount = 0
      @stat.votecount = 0

      situation.stat = @stat
      situation.save
    end

    @stat.viewcount += 1
    @stat.save

  end

  def vote_up
    @situation = Situation.find(params[:id])
    if (!current_user.nil? && !@situation.nil?)
      current_user.up_vote!(@situation)
    end
    render :text => @situation.votes unless @situation.nil?
  end

  def vote_down
    @situation = Situation.find(params[:id])
    if (!current_user.nil? && !@situation.nil?)
      current_user.down_vote!(@situation)
    end
    render :text => @situation.votes unless @situation.nil?
  end

  def rate
    @situation = Situation.find(params[:id])
    @situation.rate(params[:stars], current_user) # or any user instance
    current_user.up_vote!(@situation)
    render :text => "OK"
  end

  def add_category_spaces(category)
    categoryname = category
    if (!category.nil? && !category.empty?)
      categoryname = category.sub("BeautyStyle", "Beauty & Style")
    end
    return categoryname
  end
end
