class ChatsController < ApplicationController
  
  # def room
  #   ## @faye_needed = true
  #   @user = current_user
  #   unless @user.nil?
  #     render :action => "room", :layout => "layouts/application2"
  #   else
  #     redirect_to '/users/sign_in'
  #   end
  # end

  # def new_message
  #   ##@faye_needed = true
  #   #  Check if the message is private
  #   if recipient = params[:message].match(/@(.+) (.+)/)
  #     # It is private, send it to the recipient's private channel
  #     @channel = "/messages/private/#{recipient.captures.first}"
  #     @message = { :username => current_user.username, :msg => recipient.captures.second }
  #   else
  #     # It's public, so send it to the public channel
  #     @channel = "/messages/public"
  #     @message = { :username => current_user.username, :msg => params[:message] }
  #   end
  #   respond_to do |f|
  #     f.js
  #   end
  # end

  def index
    @current_user = current_user
    unless @current_user.nil?
      @chats = Chat.all
      render :action => "index", :layout => "layouts/application2"
    else
      redirect_to '/users/sign_in'
    end
  end

  def create
    @current_user = current_user
    unless @current_user.nil?
      @chat = Chat.create!(params[:chat])
      respond_to do |f|
        f.js
      end
    else
      redirect_to '/users/sign_in'
    end
  end

end
