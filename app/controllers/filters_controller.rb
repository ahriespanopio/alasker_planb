class FiltersController < ApplicationController
  # GET /filters
  # GET /filters.xml
   def index
     if current_user
      @filters = Filter.where(user_id: current_user.id)
 
      respond_to do |format|
        format.html { render :action => "index", 
                             :layout => "layouts/application2"
                    } # index.html.erb
        format.xml  { render :xml => @filters }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /filters/1
  # GET /filters/1.xml
  def show
    if current_user
      @filter = Filter.find(params[:id])

      respond_to do |format|
        format.html { render :action => "show", 
                             :layout => "layouts/application2"
                    } # show.html.erb
        format.xml  { render :xml => @filter }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /filters/new
  # GET /filters/new.xml
  def new
    if current_user
      @filter = Filter.new

      respond_to do |format|
        format.html { render :action => "new", 
                             :layout => "layouts/application2"
                    } # new.html.erb
        # format.json  { render :json => @categories.map{|c| [c.name, c.id]} }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # GET /filters/1/edit
  def edit
    if current_user
      @filter = Filter.find(params[:id])
      # @categories = Category.where("name LIKE ?", "%#{params[:q]}%")

      respond_to do |format|
        format.html { render :action => "edit", :layout => "layouts/application2"  }
        # format.json { render :json => @categories.map{|c| [c.name, c.id]} }
      end
    else
      redirect_to '/users/sign_in'
    end
  end

  # POST /filters
  # POST /filters.xml
  def create
    @filter = Filter.new(params[:filter])
    @filter.user_id = current_user.id

    respond_to do |format|
      if @filter.save
        format.html { redirect_to(@filter, :notice => 'Filter was successfully created.') }
        format.xml  { render :xml => @filter, :status => :created, :location => @filter }
      else
        format.html { render :action => "new", :layout => "layouts/application2"  }
        format.xml  { render :xml => @filter.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /filters/1
  # PUT /filters/1.xml
  def update
    @filter = Filter.find(params[:id])

    respond_to do |format|
      if @filter.update_attributes(params[:filter])
        format.html { redirect_to(@filter, :notice => 'Filter was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit", :layout => "layouts/application2" }
        format.xml  { render :xml => @filter.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /filters/1
  # DELETE /filters/1.xml
  def destroy
    @filter = Filter.find(params[:id])
    @filter.destroy

    respond_to do |format|
      format.html { redirect_to(filters_url) }
      format.xml  { head :ok }
    end
  end
end
