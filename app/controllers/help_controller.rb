class HelpController < ApplicationController
  before_filter :last_act

  def last_act
    user_last_activity(current_user)
  end

  def index
   render :action => "index", :layout => "layouts/application2",
          :locals => {:big_answer_askbar => true}
  end
  
  def about
   render :action => "about", :layout => "layouts/application2",
          :locals => {:big_answer_askbar => true}
  end
  
  def terms
   render :action => "terms", :layout => "layouts/application2",
          :locals => {:big_answer_askbar => true}
  end

   def privacy
   render :action => "privacy", :layout => "layouts/application2",
          :locals => {:big_answer_askbar => true}
  end

  
end

