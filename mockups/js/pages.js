
App.HomeView = Backbone.View.extend({
    initialize: function() {
        console.log('HomeView initialize');
		this.isComposite = true;
        this.template = JST['home'];
		$(this.el).html(this.template());
    },
    render:function (eventName) {
		console.log('HomeView render');
		this._renderPartials();
        return this;
    },
	_renderPartials: function() {
		console.log('HomeView renderPartials ');
		var container = $(this.el).find('#links');
		var partial = !App.currentUser ? new App.GuestHomeLinksPartialView({ el:container }) : new App.HomeLinksPartialView({ el:container });
		partial.render();
	}
});

App.StartView = Backbone.View.extend({
    initialize: function() {
		var self = this;
		
		console.log('StartView initialize');
		this._continued = false;
		this.isComposite = true;
        this.template = JST['start'];
		$(this.el).html(this.template());
		//$(this.el).find('#search-text');
		this.filter = "";
		
		this.$el.bind('onshow', function(){ self.onshow(); });
	},
	onresize: function(){
		App.resize($('.scrollbar2'), 0, 190);
		App.resize($('.viewport'), 10, 190);
		this.plugins();
	},
	onshow: function(){
		console.log('[StartView onshow]');			
		$.mobile.loading('show');
		this.onresize();
	},
	events: {
		'change #search-text' : '_search',
		'click #continue-button' : '_continue'
	},
	_continue: function(e) {
		if(!this._continued) {			
			this._continued = true;
			if (this.filter) {
				$.mobile.loading('show');
				var newSituation = new App.Situation({ content: this.filter });
				var payload = { 'situation':newSituation.toJSON() };
				newSituation.save(payload, {
					async : true,
					dataType: 'json',
					success: function(model,data){ 
						console.log('*** continue success');
						App.NextSituationView.data = data;												
					},
					error: function(data, xhr) {
						console.log('*** continue error');
						App.NextSituationView.data = null;												
					},
					complete: function(){						
						$.mobile.loading('hide');
						if (App.NextSituationView.data){
							window.location.hash = "nextsituation";
						} else {
							window.location.hash = "";
						}
					}					
				});
			}
		}
	},
	_search: function(e){
		var self = this;
		var newFilter = $(e.currentTarget).val();
		if (newFilter !== this.filter) {
			this.filter = newFilter;
		}
		this._getSituations(function(x){ self._renderPartials(x); })
	},
	_getSituations : function(callback) {
		$.mobile.loading('show');
		
		var self = this;
		console.log("StartView _getSituations");
		var situations =  new App.Situations(); 
		situations.search(this.filter);
		situations.fetch({
			async : true,
			dataType: 'json',
			success: function(model,data){
				console.log('*** StartView fetch success');		
				self._isfetched = true;		
			},
            error: function(data, xhr) {
				console.log('*** StartView fetch error');
				self._isfetched = false;
				/*situations = new App.Situations([
					{'id':1, 'content':"Lorem ipsum dolor sit amet, consectetur adipiscing elit"},
					{'id':2, 'content':"Quisque id elit tincidunt elit pellentesque porttitor"},
					{'id':3, 'content':"In sit amet lectus at urna facilisis condimentum vel sed urna"},
					{'id':4, 'content':"Suspendisse id nulla viverra lacus mollis suscipit id non metus"},
					{'id':5, 'content':"Etiam et tortor vel arcu faucibus iaculis in non nunc"},
					{'id':6, 'content':"Cras non lorem sit amet dolor ornare vulputate a non ipsum"},
					{'id':7, 'content':"Vestibulum id dolor a risus placerat hendrerit at condimentum risus"},
					{'id':8, 'content':"Mauris laoreet lectus vitae nisi pulvinar egestas"},
					{'id':9, 'content':"Proin vel lacus quis neque ultrices consectetur non eget lorem"},
				]);	*/
            },
			complete: function(){
				console.log("*** StartView fetch complete");
				callback.call(null, situations);
			}
        });
	},
    render:function (eventName) {
		var self = this;
		console.log("StartView render");
		this._getSituations(function(x){ self._renderPartials(x); })
		return this;
    }, 
	_renderPartials: function(situations){
		console.log("StartView renderPartials");
		var container = $(this.el).find('#situation-list-container');
		var partial =  new App.SituationListPartialView({el:container, collection:situations});
		partial.render();
		//App.plugins(this);
		$.mobile.loading('hide');
		this.plugins();
	},
	plugins: function(){
		$('.scrollbar2').tinyscrollbar({sizethumb:'42'});
	}
});

App.SignUpView = Backbone.View.extend({
	events : {
		'submit form': 'signup',
		'change #username' : 'setUsername',
		'change #password' : 'setPassword',
		'change #email' : 'setEmail',
		'popupafterclose #error_message': 'clearForm'
	},
	initialize: function() {		
        this.template = JST['sign-up'];
		$(this.el).html(this.template());
		
		this.$username = $(this.el).find("#username");
		this.$password = $(this.el).find("#password");
		this.$email = $(this.el).find("#email");
		this.$error = $(this.el).find("#error_message");		
    },
	signup: function(e){
		var self = this;
		e.preventDefault();
		this.model.save( { user: this.model.toJSON() }, {
			success: function(session, response) {
				console.log('*** signup success');				
				App.currentUser = new App.User(response);
				window.location.hash = "";
			},
			error: function(session, response) {
				console.log('*** signup error');
				self.showError('Signup error. Please retry again!');
			}
		});
	},	
	setUsername: function(){
		this.model.set( { username : this.$username.val() } );
	},
	setPassword: function(){
		this.model.set( { password : this.$password.val() } );
	},
	setEmail: function(){
		this.model.set( { email : this.$email.val() } );
	},
	showError: function(message){
		this.$error.html('<p>'+message+'</p>');
		this.$error.popup('open');
	},
	clearForm: function(){
		this.model.set({login:'', password:''});
		this.$username.val('');
		this.$password.val('');		
		this.$email.val('');		
		this.$email.focus();
	},   
    render:function (eventName) {
        return this;
    }
});

App.LoginView = Backbone.View.extend({
   initialize: function() {		
        this.template = JST['login'];
		$(this.el).html(this.template());

		this.$username = $(this.el).find("#username");
		this.$password = $(this.el).find("#password");
		this.$popup = $(this.el).find("#login_error");		
    },
	events: {
		'submit form': 'login',
		'change #username' : 'setUsername',
		'change #password' : 'setPassword',
		'popupafterclose #login_error': 'onClosePopup'
	},
	onClosePopup: function(event, ui){
		this.clearForm();
	},
	setUsername: function(){
		this.model.set( { login : this.$username.val() } );
	},
	setPassword: function(){
		this.model.set( { password : this.$password.val() } );
	},
	showError: function(message){
		this.$popup.html('<p>'+message+'</p>');
		this.$popup.popup('open');
	},
	clearForm: function(){
		this.model.set({login:'', password:''});
		this.$username.val('');
		this.$password.val('');		
		this.$username.focus();
	},
	login: function(e) {
		var self = this;
	
		console.log('LoginView login');
		e.preventDefault();
		
		this.model.save( { user_login: this.model.toJSON() }, {
			success: function(session, response) {
				console.log('*** login success');
				App.currentUser = new App.User(response);
				window.location.hash = "";
			},
			error: function(session, response) {
				console.log('*** login error');
				try 
				{
					var err = JSON.parse(response.responseText);
					dump( err );
					self.showError(err.message);					
				}
				catch(e)
				{
					self.showError('<p>Server connection error!</p>');
				}
			}
		});
	},
	
    render:function (eventName) {
        return this;
    }
});

App.PlanView = Backbone.View.extend({
	initialize: function() {
		var self = this;		
		var content = this.model.get('content');
		var pc = this.model.get('plancondition');
		var pc_whendate = '', pc_what = '', pc_where = '';
		
		if ( pc ) {
			pc_whendate	= this.model.get('whendate');
			pc_what = this.model.get('what');
			pc_where = this.model.get('where');
		}
		
	    this.template = JST['view-plan'];
		
		$(this.el).html(this.template({content:content, what:pc_what, where:pc_where, whendate:pc_whendate}));
				
		this.$el.bind('onshow', function(){ self.onshow(); });
	},
	render: function(){
		var situations = this.model.get('situations');		
		
		var partial = new App.SituationsPagedView({collection:new App.Situations(situations)});
		partial.render();
		this.$el.find('#situations-part').append($(partial.el).html());
		return this;
	},
	onresize: function(){
		this.onshow();
	},
	onshow: function() {
		App.resize($('.scrollbar3'), 0, 130);
		App.resize($('.viewport'), 10, 130);
		$('#scrollbar1').tinyscrollbar({sizethumb:'42'});
	},	
});

App.SituationView = Backbone.View.extend({
	events :{
		"click #suggest-plan"  : "_suggest",
		"click #share-twitter" : "_shareOnTwitter",
		"click #share-facebook": "_shareOnFacebook",	
	},	
	initialize: function() {
		var self = this;
		
        this.template = JST['view-situation'];
		
		//this.$el.on('showplugins', function(){ self.plugins(); });
		
		var content = this.model.get('content');
		var sc_whendate = '';
		var sc_what = '';
		var sc_where = '';
		var plan_content = '';
				
		if (this.model.attributes.situationcondition){
			var condition = this.model.get('situationcondition');
			sc_whendate = condition['whendate'] || '';
			sc_what = $.trim(condition['what']) || '';
			sc_where = $.trim(condition['where']) || '';
			
			var plan = this.model.get('plan');
			if(plan){
				plan_content = plan['content'];
			}
		}
		$(this.el).html(this.template({content:content, whendate:sc_whendate, what:sc_what, where:sc_where, plan_content:plan_content}));
		
		this.$el.bind('onshow', function(){ self.onshow(); });
    },
	onresize:function(){
		this.onshow();
	},
	onshow: function(){	
		var top_content = $('.top_content').height()+50;		
		var subtitle = $('.title2').height();
		var num = top_content+subtitle;
		var k = 120-(num-219)/2.5;
		console.log('top_content:'+top_content);
		console.log('subtitle:'+subtitle);
		console.log('num:'+num);
		
		App.resize($('.scrollbar2'), 0, num+k);
		App.resize($('.viewport'), 10, num+k);
		this.plugins();
	},
    render:function (eventName) {
		var plans = new App.Plans(this.model.get('plans'));
		var $el = this.$el.find('#plans-part');
		var partial = new App.SuggestedPlansView({el: $el, collection:plans});
		partial.render();
        return this;
    },	
	plugins: function(){
		$('#scrollbar1').tinyscrollbar({sizethumb:'42'});		
		this.$el.unbind('showplugins');
	},
	getStatus : function(){
		var status = "*TEST* "+this.model.get('content');
		status += " #NeedAPlanB ";
		//status += "http://"+App.config.host+"/situations/"+this.model.get('id');
		status += "http://test.planbz.com/situations/"+this.model.get('id');
				
		return status;
	},
	_shareOnTwitter: function(){
		console.log('share on twitter');
		
		var status = this.getStatus();
		var tw = new Twitter();		
		tw.onAuthenticated = function(data){		
			console.log("Authenticated USER: "+data.screen_name);
			tw.tweet(status);
		};
		tw.authenticate();
	},
	_shareOnFacebook: function(){
		console.log('share on facebook');		
	},	
	_suggest: function() {
		var situation_id = this.model.get('id');		
		window.location.hash = "suggestplan/"+situation_id;
	}
});

App.SuggestPlanView = Backbone.View.extend({
	initialize: function(){		
		console.log('suggest a plan for situation:'+this.model.get('new_situation_id'));
		var self = this; 
		this.condition = new App.PlanCondition();
		this._finished = false;
		
		// set template
		this.template = JST['add-a-plan'];
		var $el = $(this.el);		
		$el.html(this.template());	

		// get dom references
		this.$content = $el.find('#plan_content');
		this.$what = $el.find('#pc_what');
		this.$where = $el.find('#pc_where');
		this.$whendate = $el.find('#pc_whendate');		
		
		// set listeners
		this.$content.change(function(e){
			self.model.set({content: $(e.currentTarget).val()});
		});
		this.$what.change(function(e){
			self.condition.set({what: $(e.currentTarget).val()});
		});
		this.$where.change(function(e){
			self.condition.set({where: $(e.currentTarget).val()});
		});
		this.$whendate.change(function(e){
			self.condition.set({whendate: $(e.currentTarget).val()});
		});
	},
	events: {
		'click #finish' : '_finish'
	},
	_finish: function() {
		if(!this._finished && this.model.get('content')){
			console.log('suggested plan:'+this.model.get('content'));
			this._finished = true;
			
			var id = this.model.get('new_situation_id');
			var	payload = { 'plan':this.model.toJSON() };
			payload['plancondition'] = this.condition.toJSON();
			
			this.model.save(payload, {			
				complete: function(){
					window.location.hash = "showsituation/"+id;
				}
			});
		}
	},
	render: function(){
		return this;
	}
});

App.NextSituationView = Backbone.View.extend({
    initialize: function() {		
		var self = this;
		this._completing = false;
		
		// set template
        this.template = JST['next-situation'];
		var $el = $(this.el);		
		$el.html(this.template());
		
		// get dom references
		this.$category = $el.find('#category_id');
		this.$content = $el.find('#content');
		this.$plan = $el.find('#plan_content');
		this.$what = $el.find('#sc_what');
		this.$whenkeyword = $el.find('#sc_whenkeyword');
		this.$whendate = $el.find('#sc_whendate');
		this.$where = $el.find('#sc_where');
		
		// set dom values
		this.$content.val(this.model.get('content'));		
		this.$what.val(this.options.condition.get('what'));
		this.$where.val(this.options.condition.get('where'));
		this.$whenkeyword.val(this.options.condition.get('whenkeyword')); 
		this.$whendate.val(this.options.condition.get('whendate'));
		var options = new App.Categories();
		options.fetch({
			success: function(model,data){
				self._renderPartial(options);
			},
            error: function(data, xhr) {
			}	
		});
		
		// set change listeners
		this.$category.change(function(e){
			self.model.set({category_id: $(e.currentTarget).val()});
		});
		this.$content.change(function(e){
			self.model.set({content: $(e.currentTarget).val()});
		});
		this.$plan.change(function(e){
			var content = $(e.currentTarget).val();
			if (content) {
				self.options.plan = new App.Plan({content:content});
			} else {
				delete self.options.plan;
			}
		});
		this.$what.change(function(e){
			self.options.condition.set({what: $(e.currentTarget).val()});
		});
		this.$where.change(function(e){
			self.options.condition.set({where: $(e.currentTarget).val()});
		});
		this.$whenkeyword.change(function(e){
			self.options.condition.set({whenkeyword: $(e.currentTarget).val()});
		});
		this.$whendate.change(function(e){
			self.options.condition.set({whendate: $(e.currentTarget).val()});
		});
		
		// get what keywords		
		App.get('/situations/getWhatKeywords', {searchstring:this.model.get('content')}, 
			function(data){ 
				self.$what.val(data); 
				self.$what.trigger('change');
			});
		
		// get when keywords
		App.get('/situations/getWhenKeywords', {searchstring:moment(new Date()).format('MMMM D, YYYY hh:mmA')}, 
			function(data){ 
				self.$whenkeyword.val(data); 
				self.$whenkeyword.trigger('change');
			});
			
		// get where keywords
		App.get('/situations/getWhereKeywords', {searchstring:this.model.get('content')}, 
			function(data){ 
				self.$where.val(data); 
				self.$where.trigger('change');
			});
    },
	events: {
		'click #finish' : '_finish'
	},
    render:function (eventName) {        
        return this;
    },
	_renderPartial: function(options) {
		var options = new App.CategoryOptionsPartialView({el:this.$category, collection:options});
		options.render();
	},
	_finish: function(){
		if (this._completing) 
			return;		
			
		this._completing = true;
				
		var payload = { 'situation':this.model.toJSON() };
		
		if (this.options.condition){
			payload['situationcondition'] = this.options.condition.toJSON();
		}
		
		if (this.options.plan){
			payload['plan'] = this.options.plan.toJSON();
		}
				
		this.model.url = App.url('situations/'+this.model.get('id')+'.json');
		this.model.save(payload, {			
			success: function(model,data){ 		
				App.CompletedView.data = data;
				window.location.hash = "showsituation/"+data['id'];
			},
			error: function(data, xhr) {
				console.log('*** finish error');
				window.location.hash = "";
			},						
		});
		console.log('*** _finish');
	}
});

App.CompletedView = Backbone.View.extend({
    initialize: function() {
        this.template = JST['completed'];
		$(this.el).html(this.template());
    },
    render:function (eventName) {       
        return this;
    }
});

App.ListSituationView = Backbone.View.extend({
	events: {		
		'click #sort_expiring' : '_getExpiring',
		'click #sort_newest' : '_getNewest',
		'click #sort_popular' : '_getPopular',		
	},
    initialize: function() {
		console.log('ListSituationView init');
		var self = this;
		// set template
        this.template = JST['list-situation'];
		$(this.el).html(this.template());		
		
		this.$el.bind('onshow', function(){ self.onshow(); });
		
		// init vars
		this._page = 0;		
		this._sortby = 'newest';	
    },
	onresize: function(){
		App.resize($('.scrollbar3'), 0, 130);
		App.resize($('.viewport'), 10, 130);
		this.plugins();
	},
	onshow : function(){
		console.log('[ListSituationView onshow]');
		$.mobile.loading('show');
		this.onresize();
	},
    render:function (eventName) {   
		this._getNewest();
        return this;
    },
	_hilite: function(event){
		this._scroller = false;
		this._canScroll = false;
		$('#situations-part').html('');
		if(event){
			$('#nav li[class="selected"]').removeClass('selected');
			var li = $(event.currentTarget).parent('li');
			li.addClass('selected');
		}
	},
	_getExpiring: function(event){
		this._hilite(event);
		this._pageOptions = {
			page:0,
			home:1,
			paginate:1,
			limit:15,
		};
		this._getPage();
	},
	_getNewest: function(event) {
		this._hilite(event);
		this._pageOptions = {
			sort: 'newest',
			by: 0,
			page:0,
			paginate:1,
			limit:15,
		};
		this._getPage();
	},
	_getPopular: function(event) {
		this._hilite(event);
		this._pageOptions = {
			sort: 'popular',
			by: 0,
			page:0,
			paginate:1,
			limit:15,
		};
		this._getPage();
	},
	_getPage: function(){	
		$.mobile.loading('show');
		
		var self = this;
		// get next page
		this._pageOptions.page++;
		var situations = new App.Situations();
		situations.url = App.url('plans/listby.json', this._pageOptions);
		situations.fetch({
			success: function(model,data){ 	
				console.log('*** success : ListSituationView._getPage ');
				self._renderPage(situations);				
			},
			error: function(data, xhr){
				console.log('*** error : ListSituationView._getPage ');
			},
			complete: function(){
				$.mobile.loading('hide');
			}
		});
	},
	_renderPage: function(situations) {		
		this._canScroll = situations.models.length >= this._pageOptions.limit;
		console.log('[ListSituationView canScroll]'+this._canScroll);
		var partial = new App.SituationsPagedView({collection:situations});
		partial.render();
		var lis = $(partial.el).html();
		$('#situations-part').append(lis);
		this.plugins();
	},
	_scroll: function() {
		console.log('*** load more');
		this._getPage();
	},
	plugins: function(){
		var self = this;		
		if(!this._scroller){			
			this._scroller = $('#scrollbar1').tinyscrollbar({sizethumb:'42'});
		} else {
			this._scroller.tinyscrollbar_update('relative');
		}
		if (this._canScroll) {
			$('#load_more').appear(function(){
				self._scroll();
			});					
		}
	}
});


