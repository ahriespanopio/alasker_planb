App.SuggestedPlansView = Backbone.View.extend({
	_ulPart: _.template([		
		"<% items.each(function(item) { %>",
        "	<%= part(item) %>",
		"<% }); %>",		
		].join('')
	),	
	_liPart: function(plan) {
		var template = _.template("<li><a href='#showplan/<%= id %>'><span class='go_button'>go</span><div style='overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'><%= content %></div></a></li>");
		return template({id : plan.get('id'), content : plan.get('content')});
	},
	render: function() {
		$(this.el).html('');
		var template = this._ulPart({ items:this.collection, part:this._liPart});
		$(this.el).append(template);
	}
});

App.SituationsPagedView = Backbone.View.extend({
	_ulPart: _.template([
		"<% items.each(function(item) { %>",
        "	<%= part(item) %>",
		"<% }); %>",		
		].join('')		
	),	
	_liPart: function(situation) {	
		var template = _.template("<li><a href='#showsituation/<%= id %>'><div style='overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'><%= content %></div></a></li>");
		var result = template({id : situation.get('id'), content : situation.get('content')});
		//console.log('*** part:'+result);		
		return result;
	},
	render: function() {		
		//console.log('*** SituationsPagedView render '+this.collection.length);
		var result = this._ulPart({ items:this.collection, part:this._liPart});
		//console.log(result);
		$(this.el).html(result);
	}
});

App.CategoryOptionsPartialView = Backbone.View.extend({
	_selectPart : _.template([
		//"<select>",
		"<% items.each(function(item) { %>",
        "	<%= part(item) %>",
		"<% }); %>",
		//"</select>"
		].join('')
	),
	_optionPart: function(category) {
		var template = _.template("<option value='<%= id %>'><%= name %></option>");
		return template({id:category.get('id'), name:category.get('name')});
	},
	render: function() {
		$(this.el).html('');
		var template = this._selectPart({ items:this.collection, part:this._optionPart});
		$(this.el).append(template);
	}
});

App.SituationListPartialView = Backbone.View.extend({
	_ulPart: _.template([
		"<ul>",
		"<% items.each(function(item) { %>",
        "	<%= part(item) %>",
		"<% }); %>",
		"</ul>"
		].join('')
	),	
	_liPart: function(situation) {
		var template = _.template("<li><a href='#showsituation/<%= id %>'><span class='go_button'>go</span><div style='overflow:hidden;text-overflow:ellipsis;white-space:nowrap;'><%= content %></div></a></li>");
		return template({id : situation.get('id'), content : situation.get('content')});
	},
	render: function() {
		$(this.el).html('');
		var template = this._ulPart({ items:this.collection, part:this._liPart});
		$(this.el).append(template);
	}
});

App.GuestHomeLinksPartialView = Backbone.View.extend({
	template: _.template([
		'Are you a member?&nbsp;<a href="#login">Sign in</a>&nbsp;',
		'<a href="#logintwitter">Signin with Twitter</a>&nbsp;',
		'<a href="#loginfacebook">Signin with Facebook</a><br/>',
		'Want to join the community?&nbsp;<a href="#signUp">Sign up</a>'
	].join('')),
	render: function() {
		$(this.el).html(this.template());
	}
});

App.HomeLinksPartialView = Backbone.View.extend({
	template: _.template([
		'You are signed in as <b><%= username %></b>!<br/>',
		'<a href="#logout">Logout</a>'
	].join('')),
	render: function() {
		var userName = App.currentUser.get('username');
		$(this.el).html(this.template({ username : userName }));
	}
});
