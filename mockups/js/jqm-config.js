$(document).bind("mobileinit", function () {
    $.mobile.ajaxEnabled = false;
    $.mobile.linkBindingEnabled = false;
    $.mobile.hashListeningEnabled = false;
    $.mobile.pushStateEnabled = false;	

    // Remove page from DOM when it's being replaced
 	$('div[data-role="page"]').live('pagehide', function (event, ui) {			
		console.log('*** hiding page');		
		$(event.currentTarget).remove(); 
	});
	
	$('div[data-role="page"]').live('pageshow', function (event, ui) {	
		console.log('*** showing page');
		if ( window.__page ) {			
			window.__page.$el.trigger('onshow');
		}
	});	
});