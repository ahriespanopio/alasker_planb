App.AjaxView = Backbone.View.extend({
    initialize: function() {
        console.log('homeview init');
        this.template =  _.template('<div id="json"/>');
		this.fetch();
    },
	
	ajax: function(){
		$.ajax({
            url: App.url("situations.json"),
            dataType: "json",
            async:true,
            type:'GET',
            data: null,
            success: function(p1,p2,p3){
				console.log('success');
			},
            error: function(p1,p2,p3){
				console.log('error');
			}
        });
	},
	
	request: function(){
		var self = this;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", App.url("situations.json"), true);
		xhr.onreadystatechange = function() {
		  if (xhr.readyState == 4) {
			$(self.el).find('#json').html(xhr.responseText);
		 }	
		}
		xhr.send();
	},
	
	fetch: function()	{
		var s = new App.Situations();
		s.fetch({
			async: true,
			dataType: 'json',
			success: function(p1,p2,p3){
				console.log('success');
			},
            error: function(p1,p2,p3){
				console.log('error');
			},
			complete: function(p1,p2,p3){
				//console.log(this.xhr.responseText);
			}
		});
	},

    render:function (eventName) {
		console.log('homeview render');
        $(this.el).html(this.template());
        return this;
    }
});
