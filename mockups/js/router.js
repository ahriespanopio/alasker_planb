App.Router = Backbone.Router.extend({

   routes: {
		""					: "home",
		"showsituation/:id"	: "showSituation",
		"suggestplan/:id"	: "suggestPlan",
		"showplan/:id"		: "viewPlan",
		"login" 			: "login",
		"logintwitter"		: "loginTwitter",
		"loginfacebook"		: "loginFacebook",
		"logout"			: "logout",
		"start"				: "start",
		"nextsituation" 	: "nextSituation",
		"completed" 		: "completedSituation",
		"list"  			: "listSituation",
		"signUp"  			: "signUp",		
		'*path'  			:'defaultRoute'
	},
	
	defaultRoute : function(){
		window.location.hash = "";
	},
	
    initialize:function () {
        this.firstPage = true;
    },

    home: function() {
        console.log('#home');
        this.changePage(new App.HomeView());
    },

    showSituation: function(id) {
		console.log('#viewSituation');
		var self = this;
        
		if(id){
			var situation = new App.Situation();
			situation.url = App.url('situations/'+id+'.json');
			$.mobile.loading('show');
			situation.fetch({
				success: function(model, data){
					self.changePage(new App.SituationView({model:situation}));
				},
				error:function(){
					window.location.hash = "";
				},
				complete:function(){
					$.mobile.loading('hide');
				}
			});
			
		} else {
			window.location.hash = '';
		}
    },
	
	logout: function() {
		console.log('#logout');
		var user = App.currentUser;
		
		/*
		$.ajax({
            url: App.url("users/sign_out.json"),			
            dataType: "json",
            async:true,
            type:'GET',
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Basic  " + btoa(user.get('username') + ":" + user.get('password')));
			},
			data: {user: {username: user.get('username'), password: user.get('password')}},
            success: function(p1,p2,p3){
				console.log('logout success');
				App.currentUser = false;
				window.location.hash = "";
			},
            error: function(p1,p2,p3){
				console.log('logout error');
				window.location.hash = "";
			}
        });*/
		
		$.ajax({
            url: App.url("mobile/logout"),			
            dataType: "json",
            async:true,
            type:'GET',
			data: {user_login: {login: user.get('username')}},
            success: function(p1,p2,p3){
				console.log('logout success');
				App.currentUser = false;
				window.location.hash = "";
			},
            error: function(p1,p2,p3){
				console.log('logout error');
				window.location.hash = "";
			}
        });
	}, 

    login:function() {
        console.log('#login');
        this.changePage(new App.LoginView({ model:new App.Login() }));
    },
	
	loginFacebook: function(){
		var self = this;
		var fb = new Facebook();
		var login = new App.Login();
		
		fb.onAuthenticated = function(data){					
			console.log("Facebook Authentication SUCCESS"); 
			
			fb.getUserData(
				function(data)
				{
					login.set( { uid : data['id'], provider : 'facebook', login : '', password : ''  } );			
					login.save( { user_login: login.toJSON() }, {
						success: function(session, response) {
							console.log('*** login success');
							dump ( response );
							App.currentUser = new App.User(response);
							window.location.hash = "";
						},
						error: function(session, response) {
							console.log('*** login error');				
							window.location.hash = "";
						}
					});
				}, 
				function()
				{
					window.location.hash = "";
				}
			);
			
		};
		fb.onError = function(data){
			console.log("Facebook Authentication ERROR"); 
			window.location.hash = "";
		};
		fb.authenticate();		
	},
	
	loginTwitter: function() {
		var self = this;
		var tw = new Twitter();	 
		var login = new App.Login();
		
		tw.onAuthenticated = function(data){					
			console.log("Twitter Authentication SUCCESS"); 
			login.set( { uid : data['id_str'], provider : 'twitter', login : '', password : ''  } );			
			login.save( { user_login: login.toJSON() }, {
				success: function(session, response) {
					console.log('*** login success');
					dump ( response );
					App.currentUser = new App.User(response);
					window.location.hash = "";
				},
				error: function(session, response) {
					console.log('*** login error');				
					window.location.hash = "";
				}
			});
		};
		tw.onError = function(data){
			console.log("Twitter Authentication ERROR"); 
			window.location.hash = "";
		};
		tw.authenticate();		
	},
	
	start:function () {
        console.log('#start');
        this.changePage(new App.StartView());
    },
	
	listSituation:function () {
        console.log('#listSituation');
        this.changePage(new App.ListSituationView());
    },
		
	nextSituation:function () {
        console.log('#nextSituation');
		var data = _.extend({}, App.NextSituationView.data);
		delete App.NextSituationView.data;
		
		var data2 = data.situationcondition;
		delete data.situationcondition;
		
		var situation = new App.Situation(data);
		var situationcondition = new App.SituationCondition(data2);
		
        this.changePage(new App.NextSituationView({model:situation, condition:situationcondition}));
    },
	
	completedSituation:function () {
        console.log('#completed');
		
		var data = _.extend({}, App.CompletedView.data);
		delete App.CompletedView.data;
		
		var data2 = data.situationcondition;
		delete data.situationcondition;		
		
		var data3 = data.plan;
		delete data.plan;
		
		var situation = new App.Situation(data);
		//var situationcondition = new App.SituationCondition(data2);
		//var plan = new App.Plan(data3);
		
        this.changePage(new App.CompletedView({model:situation}));
    },
	
	signUp:function () {
        console.log('#signUp');
		var newUser = new App.User();
        this.changePage(new App.SignUpView({model:newUser}));
    },
	
	suggestPlan: function(situation_id) {
		console.log('#suggestPlan');
		var newPlan = new App.Plan({new_situation_id:situation_id});
		this.changePage(new App.SuggestPlanView({model:newPlan}));
	},
	
	viewPlan: function(plan_id) {
		console.log('#viewPlan');
		var self= this;
		$.mobile.loading('show');
		var plan = new App.Plan({id:plan_id});
		plan.url = App.url('plans/'+plan_id+'.json');
		plan.fetch({
			success: function(model,data){ 	
				self.changePage(new App.PlanView({model:plan}));		
			},
			error: function(data, xhr){
				window.location.hash = '';
			},
			complete: function(){
				$.mobile.loading('hide');
			}
		});
		
	},

    changePage:function (page) {	
		window.__page = page;
		App.isrotate = false;

		// attach the page DOM element to document's body
		
        $(page.el).attr('data-role', 'page');
        page.render();
        $('body').append($(page.el));
	
		// animate page transition
        $.mobile.changePage($(page.el), {changeHash:false, transition: 'none'});
			
		//page.$el.trigger('onshow');
		
    }
});


