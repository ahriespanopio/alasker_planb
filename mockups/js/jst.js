window.JST = {}
JST['home'] 				= _.template($("#tpl-home").html());
JST['login'] 				= _.template($("#tpl-login").html());
JST['sign-up'] 				= _.template($("#tpl-sign-up").html());
JST['start'] 				= _.template($("#tpl-start").html());
JST['next-situation']		= _.template($("#tpl-next").html());
JST['view-situation'] 		= _.template($("#tpl-view-situation").html());
JST['list-situation'] 		= _.template($("#tpl-list-situation").html());
JST['completed'] 			= _.template($("#tpl-completed").html());
JST['add-a-plan'] 			= _.template($("#tpl-add-a-plan").html());
JST['view-plan'] 			= _.template($("#tpl-view-plan").html());