// ------------------------------
// Models
// ------------------------------
App.User = Backbone.Model.extend({
	url : App.url('users.json'),
	defaults : {
		username : "",
		password : "",
		email : "",
		website : "",
	},	
});

App.Situation = Backbone.Model.extend({
	url : App.url('situations.json'),
});

App.Plan = Backbone.Model.extend({
	url : App.url('plans.json'),
});

App.Category = Backbone.Model.extend({});

App.SituationCondition = Backbone.Model.extend({});

App.PlanCondition = Backbone.Model.extend({});

App.Login = Backbone.Model.extend({
	//url : App.url('users/sign_in.json'),
	url : App.url('mobile/login'),
	defaults : {
		login		: "",
		password 	: "",
		provider	: "",
		uid			: ""
	},	
});



// ------------------------------
// Collections
// ------------------------------
App.Situations = Backbone.Collection.extend({
	model : App.Situation,
	search: function(filter){
		if (filter) {
			this.url = App.url('situations.json', {'situationsearch' : '+'+filter});
		} else {
			this.url = App.url('situations.json', {});
		}
	},
});

App.Categories = Backbone.Collection.extend({
	model : App.Category,
	url : App.url('categories.json'),
});

App.Plans = Backbone.Collection.extend({
	model : App.Plan,
});


