# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130214195558) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "authentications", :force => true do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "badges_sashes", :force => true do |t|
    t.integer  "badge_id"
    t.integer  "sash_id"
    t.boolean  "notified_user", :default => false
    t.datetime "created_at"
  end

  add_index "badges_sashes", ["badge_id", "sash_id"], :name => "index_badges_sashes_on_badge_id_and_sash_id"
  add_index "badges_sashes", ["badge_id"], :name => "index_badges_sashes_on_badge_id"
  add_index "badges_sashes", ["sash_id"], :name => "index_badges_sashes_on_sash_id"

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.string   "language"
    t.integer  "category_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_private",  :default => false
  end

  create_table "categories_situations", :id => false, :force => true do |t|
    t.integer "category_id"
    t.integer "situation_id"
  end

  add_index "categories_situations", ["category_id", "situation_id"], :name => "index_categories_situations_on_category_id_and_situation_id"

  create_table "chats", :force => true do |t|
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "filter_categories_associations", :force => true do |t|
    t.integer  "category_id"
    t.integer  "filter_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "filters", :force => true do |t|
    t.integer  "user_id"
    t.decimal  "location_lat",    :precision => 10, :scale => 6
    t.decimal  "location_lng",    :precision => 10, :scale => 6
    t.integer  "location_radius"
    t.datetime "when_from"
    t.datetime "when_to"
    t.string   "what"
    t.integer  "expires_in"
    t.string   "location"
    t.integer  "by_category",                                    :default => 0
    t.integer  "by_location",                                    :default => 0
    t.boolean  "apply_filter",                                   :default => false
    t.integer  "distance",                                       :default => 5
  end

  create_table "merit_actions", :force => true do |t|
    t.integer  "user_id"
    t.string   "action_method"
    t.integer  "action_value"
    t.boolean  "had_errors"
    t.string   "target_model"
    t.integer  "target_id"
    t.boolean  "processed",     :default => false
    t.string   "log"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "merit_score_points", :force => true do |t|
    t.integer "score_id"
    t.integer "num_points", :default => 0
    t.string  "log"
  end

  create_table "merit_scores", :force => true do |t|
    t.integer "sash_id"
    t.string  "category", :default => "default"
  end

  create_table "messages", :force => true do |t|
    t.string   "subject"
    t.text     "content"
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.boolean  "read",        :default => false
    t.string   "label"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "notes", :force => true do |t|
    t.integer  "situation_id"
    t.integer  "user_id"
    t.text     "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "notes", ["situation_id"], :name => "index_notes_on_situation_id"
  add_index "notes", ["user_id"], :name => "index_notes_on_user_id"

  create_table "notifications", :force => true do |t|
    t.integer  "sender_id"
    t.integer  "recipient_id"
    t.integer  "situation_id"
    t.integer  "note_id"
    t.integer  "notification_type"
    t.integer  "event"
    t.datetime "expires_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "plan_b_id"
    t.boolean  "unseen",            :default => true
    t.integer  "event_target"
  end

  create_table "plan_bs", :force => true do |t|
    t.string   "keywords"
    t.integer  "situation_id"
    t.integer  "plan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plan_conditions", :force => true do |t|
    t.integer  "plan_id"
    t.datetime "whendate"
    t.string   "whenkeyword"
    t.string   "where"
    t.datetime "validuntil"
    t.string   "if"
    t.string   "what"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "plans", :force => true do |t|
    t.text     "content"
    t.string   "language"
    t.string   "keywords"
    t.integer  "situation_id"
    t.integer  "user_id"
    t.integer  "stat_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "category_id"
    t.integer  "plancondition_id"
    t.integer  "up_votes",                                        :default => 0, :null => false
    t.integer  "down_votes",                                      :default => 0, :null => false
    t.decimal  "location_lat",     :precision => 10, :scale => 6
    t.decimal  "location_lng",     :precision => 10, :scale => 6
    t.integer  "location_radius"
  end

  create_table "rates", :force => true do |t|
    t.integer  "rater_id"
    t.integer  "rateable_id"
    t.string   "rateable_type"
    t.integer  "stars",         :null => false
    t.string   "dimension"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rates", ["rateable_id", "rateable_type"], :name => "index_rates_on_rateable_id_and_rateable_type"
  add_index "rates", ["rater_id"], :name => "index_rates_on_rater_id"

  create_table "relationships", :force => true do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "relationships", ["followed_id"], :name => "index_relationships_on_followed_id"
  add_index "relationships", ["follower_id", "followed_id"], :name => "index_relationships_on_follower_id_and_followed_id", :unique => true
  add_index "relationships", ["follower_id"], :name => "index_relationships_on_follower_id"

  create_table "sashes", :force => true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "situation_clusters", :force => true do |t|
    t.integer  "centroid"
    t.string   "name"
    t.datetime "created"
    t.integer  "situation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "situation_conditions", :force => true do |t|
    t.integer  "situation_id"
    t.datetime "whendate"
    t.string   "whenkeyword"
    t.string   "where"
    t.datetime "expires"
    t.string   "what"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "situations", :force => true do |t|
    t.text     "content"
    t.string   "language"
    t.string   "keywords"
    t.integer  "category_id"
    t.integer  "user_id"
    t.integer  "plan_id"
    t.integer  "stat_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "situationcondition_id"
    t.integer  "up_votes",                                             :default => 0, :null => false
    t.integer  "down_votes",                                           :default => 0, :null => false
    t.decimal  "location_lat",          :precision => 10, :scale => 6
    t.decimal  "location_lng",          :precision => 10, :scale => 6
    t.integer  "location_radius"
    t.string   "address"
  end

  create_table "stats", :force => true do |t|
    t.integer  "viewcount"
    t.integer  "votecount"
    t.integer  "situation_id"
    t.integer  "plan_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                                              :default => "",    :null => false
    t.string   "encrypted_password",   :limit => 128,                                :default => "",    :null => false
    t.string   "reset_password_token"
    t.string   "remember_token"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                                      :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
    t.string   "about"
    t.boolean  "admin_enabled",                                                      :default => false
    t.integer  "up_votes",                                                           :default => 0,     :null => false
    t.integer  "down_votes",                                                         :default => 0,     :null => false
    t.integer  "sash_id"
    t.integer  "level",                                                              :default => 0
    t.string   "location"
    t.string   "website"
    t.integer  "update_plans",                                                       :default => 0,     :null => false
    t.integer  "link_plans",                                                         :default => 0,     :null => false
    t.string   "authentication_token"
    t.integer  "online_status"
    t.datetime "last_activity"
    t.decimal  "location_lat",                        :precision => 10, :scale => 6
    t.decimal  "location_lng",                        :precision => 10, :scale => 6
    t.integer  "location_radius"
    t.boolean  "location_enabled"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "image"
    t.integer  "use_uploadedPic",                                                    :default => 0
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  create_table "votings", :force => true do |t|
    t.string   "voteable_type"
    t.integer  "voteable_id"
    t.string   "voter_type"
    t.integer  "voter_id"
    t.boolean  "up_vote",       :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votings", ["voteable_type", "voteable_id", "voter_type", "voter_id"], :name => "unique_voters", :unique => true
  add_index "votings", ["voteable_type", "voteable_id"], :name => "index_votings_on_voteable_type_and_voteable_id"
  add_index "votings", ["voter_type", "voter_id"], :name => "index_votings_on_voter_type_and_voter_id"

end
