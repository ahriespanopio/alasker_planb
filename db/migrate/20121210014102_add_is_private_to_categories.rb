class AddIsPrivateToCategories < ActiveRecord::Migration
  def self.up
    add_column :categories, :is_private, :boolean, :default => false
  end

  def self.down
    remove_column :categories, :is_private
  end
end
