class CreateNotifications < ActiveRecord::Migration
  def self.up
    create_table :notifications do |t|
      t.integer :user_from_id
      t.integer :user_to_id
      t.references :situation
      t.references :note
      t.integer :type
      t.integer :event
      t.datetime :expires

      t.timestamps
    end
  end

  def self.down
    drop_table :notifications
  end
end
