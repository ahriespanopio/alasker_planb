class AddStatusToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :online_status, :integer
    add_column :users, :last_activity, :datetime
    add_column :users, :location_lat, :decimal, {:precision=>10, :scale=>6}
    add_column :users, :location_lng, :decimal, {:precision=>10, :scale=>6}
    add_column :users, :location_radius, :integer
    add_column :users, :location_enabled, :boolean
  end

  def self.down
    remove_column :users, :online_status
    remove_column :users, :last_activity
    remove_column :users, :location_lat
    remove_column :users, :location_lng
    remove_column :users, :location_radius
    remove_column :users, :location_enabled
  end
end
