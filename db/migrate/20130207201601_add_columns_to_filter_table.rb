class AddColumnsToFilterTable < ActiveRecord::Migration
  def self.up
    add_column :filters, :category, :string
    add_column :filters, :by_category, :integer, :default => 0
    add_column :filters, :by_location, :integer, :default => 0
  end

  def self.down
    remove_column :filters, :by_location
    remove_column :filters, :by_category
    remove_column :filters, :category
  end
end
