class CreateCategorySituations < ActiveRecord::Migration
  def self.up
    create_table :categories_situations, :id => false do |t|
      t.references :category, :situation
    end
    add_index :categories_situations, [:category_id, :situation_id]
  end

  def self.down
    drop_table :categories_situations
  end
end
