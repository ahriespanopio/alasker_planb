class AddUpVotesAndDownVotesToPlans < ActiveRecord::Migration
  def self.up
    add_column :plans, :up_votes, :integer, :null => false, :default => 0
    add_column :plans, :down_votes, :integer, :null => false, :default => 0
  end

  def self.down
    remove_column :plans, :down_votes
    remove_column :plans, :up_votes
  end
end
