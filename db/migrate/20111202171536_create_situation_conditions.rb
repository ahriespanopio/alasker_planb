class CreateSituationConditions < ActiveRecord::Migration
  def self.up
    create_table :situation_conditions do |t|
      t.references :situation
      t.datetime :whendate
      t.string :whenkeyword
      t.string :where
      t.datetime :expires
      t.string :what

      t.timestamps
    end
  end

  def self.down
    drop_table :situation_conditions
  end
end
