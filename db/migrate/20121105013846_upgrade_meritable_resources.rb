class UpgradeMeritableResources < ActiveRecord::Migration
  def self.up
    remove_column :users, :score
  end
end