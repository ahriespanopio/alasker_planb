class RemoveCategoryAddDistanceToFilter < ActiveRecord::Migration
  def self.up
    add_column :filters, :apply_filter, :boolean, :default => false
    add_column :filters, :distance, :integer, :default => 5
    remove_column :filters, :category
  end

  def self.down
    remove_column :filters, :apply_filter
    remove_column :filters, :distance
    add_column :filters, :category, :string
  end
end
