class AddLocationToFilter < ActiveRecord::Migration
  def self.up
    add_column :filters, :location, :string
  end

  def self.down
    remove_column :filters, :location
  end
end
