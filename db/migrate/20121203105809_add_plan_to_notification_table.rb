class AddPlanToNotificationTable < ActiveRecord::Migration
  def self.up
    change_table :notifications do |t|
      t.references :plan_b
    end
  end

  def self.down
    remove_column :notifications, :plan_b_id
  end
end
