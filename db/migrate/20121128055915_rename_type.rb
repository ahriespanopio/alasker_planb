class RenameType < ActiveRecord::Migration
  def self.up
    rename_column :notifications, :type, :notification_type
  end

  def self.down
  end
end
