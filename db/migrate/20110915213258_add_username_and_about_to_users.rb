class AddUsernameAndAboutToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :username, :string
    add_column :users, :about, :string
    add_column :users, :admin_enabled, :boolean, :default => false
    add_index :users, :username,                :unique => true
  end

  def self.down
  	remove_index :users, :username
    remove_column :users, :username
    remove_column :users, :about
  end
end
