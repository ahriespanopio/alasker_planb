class AddGeoToSituation < ActiveRecord::Migration
  def self.up
    add_column :situations, :location_lat, :decimal, {:precision=>10, :scale=>6}
    add_column :situations, :location_lng, :decimal, {:precision=>10, :scale=>6}
    add_column :situations, :location_radius, :integer
  end

  def self.down
    remove_column :situations, :location_lat
    remove_column :situations, :location_lng
    remove_column :situations, :location_radius
  end
end
