class AddUseUploadedPicToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :use_uploadedPic, :integer, :default => 0
  end

  def self.down
    remove_column :users, :use_uploadedPic
  end
end
