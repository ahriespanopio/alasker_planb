class CreateFilterCategoriesAssociations < ActiveRecord::Migration
  def self.up
    create_table :filter_categories_associations do |t|
      t.integer :category_id
      t.integer :filter_id

      t.timestamps
    end
  end

  def self.down
    drop_table :filter_categories_associations
  end
end
