class AddUnseenColumn < ActiveRecord::Migration
  def self.up
    add_column :notifications, :unseen, :boolean, :default => false
  end

  def self.down
    remove_column :notifications, :unseen
  end
end
