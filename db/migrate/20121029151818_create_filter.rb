class CreateFilter < ActiveRecord::Migration
  def self.up
      create_table :filters do |t|
        t.references :user
        t.decimal :location_lat, :precision => 10, :scale => 6
        t.decimal :location_lng, :precision => 10, :scale => 6
        t.integer :location_radius
        t.datetime :when_from
        t.datetime :when_to
        t.string :what
        t.integer :expires_in
      end
  end

  def self.down
    drop_table :filters
  end
end
