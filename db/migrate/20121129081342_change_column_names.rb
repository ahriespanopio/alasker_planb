class ChangeColumnNames < ActiveRecord::Migration
  def self.up
    change_table :notifications do |t|
      t.rename :user_from_id, :sender_id
      t.rename :user_to_id, :recipient_id
      t.rename :expires, :expires_at
    end
  end
end
