class CreatePlanConditions < ActiveRecord::Migration
  def self.up
    create_table :plan_conditions do |t|
      t.references :plan
      t.datetime :whendate
      t.string :whenkeyword
      t.string :where
      t.datetime :validuntil
      t.string :if
      t.string :what

      t.timestamps
    end
  end

  def self.down
    drop_table :plan_conditions
  end
end
