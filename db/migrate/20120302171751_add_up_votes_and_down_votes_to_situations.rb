class AddUpVotesAndDownVotesToSituations < ActiveRecord::Migration
  def self.up
    add_column :situations, :up_votes, :integer, :null => false, :default => 0
    add_column :situations, :down_votes, :integer, :null => false, :default => 0
  end

  def self.down
    remove_column :situations, :down_votes
    remove_column :situations, :up_votes
  end
end
