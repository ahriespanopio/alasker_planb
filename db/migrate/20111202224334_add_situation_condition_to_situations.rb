class AddSituationConditionToSituations < ActiveRecord::Migration
  def self.up
    add_column :situations, :situationcondition_id, :integer, :references=>"situationconditions"
  end

  def self.down
    remove_column :situations, :situationcondition
  end
end
