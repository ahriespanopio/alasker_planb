class AddCategoryToPlans < ActiveRecord::Migration
  def self.up
    add_column :plans, :category_id , :integer ,:references=>"categories"
  end

  def self.down
    remove_column :plans, :category
  end
end
