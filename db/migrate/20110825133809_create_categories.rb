class CreateCategories < ActiveRecord::Migration
  def self.up
    #drop_table :categories
    create_table :categories do |t|
      t.string :name
      t.string :language
      t.references :category
      t.timestamps
    end
  end

  def self.down
    drop_table :categories
  end
end

