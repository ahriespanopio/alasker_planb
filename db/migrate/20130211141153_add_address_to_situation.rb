class AddAddressToSituation < ActiveRecord::Migration
  def self.up
    add_column :situations, :address, :string
  end

  def self.down
    remove_column :situations, :address
  end
end
