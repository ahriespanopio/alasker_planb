class CreatePlanBs < ActiveRecord::Migration
  def self.up
    create_table :plan_bs do |t|
      t.string :keywords
      t.references :situation
      t.references :plan
      t.timestamps
    end
  end

  def self.down
    drop_table :plan_bs
  end
end
