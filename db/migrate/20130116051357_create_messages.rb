class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.string  :subject
      t.text    :content
      t.integer :sender_id
      t.integer :receiver_id
      t.boolean :read, :default => false
      t.string  :label # inbox or sent
      t.timestamps
    end
  end

  def self.down
    drop_table :messages
  end
end
