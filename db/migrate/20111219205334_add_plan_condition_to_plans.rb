class AddPlanConditionToPlans < ActiveRecord::Migration
  def self.up
    add_column :plans, :plancondition_id, :integer, :references=>"planconditions"
  end

  def self.down
    remove_column :plans, :plancondition
  end
end
