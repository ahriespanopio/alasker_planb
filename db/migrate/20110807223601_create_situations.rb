class CreateSituations < ActiveRecord::Migration
  def self.up
    #create_table :situations, :options => "ENGINE=MyISAM" do |t|
    create_table :situations do |t|
      t.text :content
      t.string :language
      t.string :keywords
      t.references :category
      t.references :user
      t.references :plan
      t.references :stat
      t.timestamps
    end
    #add_index :situations, [:content, :keywords], :fulltext => true, :name=> 'fulltext_content'
    #execute('CREATE FULLTEXT INDEX fulltext_content ON situations (content,keywords)')

    execute "CREATE INDEX ON situations USING gin(to_tsvector('english', content))"
    execute "CREATE INDEX ON situations USING gin(to_tsvector('english', keywords))"

  end

  def self.down
    #execute('DROP INDEX fulltext_content ON situations')

    execute "DROP INDEX situations_to_tsvector_idx ON situations"
    execute "DROP INDEX situations_to_tsvector_idx1 ON situations"

    drop_table :situations
  end
end
