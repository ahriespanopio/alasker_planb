class RenamePoints < ActiveRecord::Migration
  def self.up
    rename_column :users, :points, :score
  end

  def self.down
  end
end
