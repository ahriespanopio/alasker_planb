class UpdateUnseenDefault < ActiveRecord::Migration
  def self.up
    change_column :notifications, :unseen, :boolean, :default => true
  end

  def self.down
    change_column :notifications, :unseen, :boolean, :default => false
  end
end

