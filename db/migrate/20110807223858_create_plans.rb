class CreatePlans < ActiveRecord::Migration
  def self.up
    #create_table :plans, :options => "ENGINE=MyISAM" do |t|
    create_table :plans do |t|
      t.text :content
      t.string :language
      t.string :keywords
      t.references :situation
      t.references :user
      t.references :stat
      t.timestamps
    end
    #execute('CREATE FULLTEXT INDEX fulltext_plans_content ON plans (content,keywords)')

    execute "CREATE INDEX ON plans USING gin(to_tsvector('english', content))"
    execute "CREATE INDEX ON plans USING gin(to_tsvector('english', keywords))"
  end

  def self.down
    #execute('DROP INDEX fulltext_plans_content ON situations')

    execute "DROP INDEX plans_to_tsvector_idx ON situations"
    execute "DROP INDEX plans_to_tsvector_idx1 ON situations"

    drop_table :plans
  end
end
