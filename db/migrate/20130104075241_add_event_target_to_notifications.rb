class AddEventTargetToNotifications < ActiveRecord::Migration
  def self.up
    add_column :notifications, :event_target, :integer
  end

  def self.down
    remove_column :notifications, :event_target
  end
end
