class AddUpdatePlansAndLinkPlansToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :update_plans, :integer, :null => false, :default => 0
    add_column :users, :link_plans, :integer, :null => false, :default => 0
  end

  def self.down
    remove_column :users, :update_plans
    remove_column :users, :link_plans
  end
end
