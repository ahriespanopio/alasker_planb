class AddGeoToPlans < ActiveRecord::Migration
  def self.up
    add_column :plans, :location_lat, :decimal, {:precision=>10, :scale=>6}
    add_column :plans, :location_lng, :decimal, {:precision=>10, :scale=>6}
    add_column :plans, :location_radius, :integer
  end

  def self.down
    remove_column :plans, :location_lat
    remove_column :plans, :location_lng
    remove_column :plans, :location_radius
  end
end
