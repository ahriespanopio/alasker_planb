class CreateSituationClusters < ActiveRecord::Migration
  def self.up
    create_table :situation_clusters do |t|
      t.integer :centroid
      t.string :name
      t.timestamp :created
      t.references :situation

      t.timestamps
    end
  end

  def self.down
    drop_table :situation_clusters
  end
end
