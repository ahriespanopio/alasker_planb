class CreateNotes < ActiveRecord::Migration
  def self.up
    create_table :notes do |t|
      t.references :situation
      t.references :user
      t.text :content

      t.timestamps
    end

    add_index :notes, :user_id
    add_index :notes, :situation_id
  end

  def self.down
      drop_table :notes
  end
end
