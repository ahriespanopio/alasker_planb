# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ :name => 'Chicago' }, { :name => 'Copenhagen' }])
#   Mayor.create(:name => 'Daley', :city => cities.first)

Category.create([
{ :id=>1 , :name => 'Entertainment' },
{ :id=>2 , :name => 'Travel' },
{ :id=>3 , :name => 'Health' },
{ :id=>4 , :name => 'Computers' },
{ :id=>5 , :name => 'Cooking' },
{ :id=>6 , :name => 'Everyday' },
{ :id=>7 , :name => 'Beauty & Style' },
{ :id=>8 , :name => 'Cars' },
{ :id=>9 , :name => 'Electronics' },
{ :id=>10 , :name => 'Sports' },
{ :id=>11 , :name => 'Business' },
{ :id=>12 , :name => 'Games' },
{ :id=>13 , :name => 'Pets' },
{ :id=>14 , :name => 'Politics' },
{ :id=>15 , :name => 'Science' },

])

=begin
Situation.create([
  {:category_id=> 1, :content => "You're going to a birthday party to bring a gift!", :id => 1, :keywords => "gift;party" },
  {:category_id=> 2, :content => "You're traveling and lose your passport!", :id => 2, :keywords => "travel;passport" },
  {:category_id=> 2, :content => "You're flying and your flight gets canceled", :id => 3, :keywords => "travel;cancel" },
  {:category_id=> 11, :content => "My company provides products which are complex and have long lives, during which they are subject to change of configuration and modification. It is in both my and my customers interest to maintain good records of each delivered product and its history. However, the data is collected in too many places and nowhere is it brought together. As a consequence we do not get the feedback we need to improve our product or the support systems", :id => 4, :keywords => "travel;cancel" },
  {:category_id=> 11, :content => "We are changing/upgrading the systems that manage our product data. Yet we want to maintain a consolidated view of project data we share with customers and partners at the same time as making the changes internally. It would help too if there was a way to minimise the impact when partners change their systems. We cannot afford for such changes to delay collaborative projects.", :id => 5, :keywords => "travel;cancel" },
  {:category_id=> 11, :content => "My company has or is competing for export contracts with substantial offset elements. To satisfy the contract terms we have to collaborate with new companies in other countries but we do not want to or cannot give them access to our in-house systems. Some of the companies are small and lack sophisticated systems to match ours. We have to be able to work effectively with these other companies.", :id => 6, :keywords => "travel;cancel" },
  {:category_id=> 11, :content => "My company buys major sub-assemblies or components and related spares from suppliers whose products are (like ours) subject to continuous improvement and change, some of which we instigate. The business of tracking which is the right spare part to order and use is difficult due to these changes. The design and support arms of both my company and our suppliers seem to work at different speeds and the risk of being out of sync is high, leading to unwanted costs. Because several organizations are involved, we cannot achieve the potential savings.", :id => 7, :keywords => "travel;cancel" },
  {:category_id=> 11, :content => "My company has implemented a PDM system and/or ERP system. We have invested a lot of effort to ensure that our internal processes are supported by PDM/ERP. We have to use our internal systems as-is and at the same time participate effectively in collaborative projects where we know the partners will not adopt our processes or systems. We do not want to incur delay and repeated cost to adjust our internal systems for each new collaborative project.", :id => 8, :keywords => "" },
  {:category_id=> 11, :content => "My company works with American and other companies whose export controls require us to control access to information by individual. We also have to apply this policy to personnel from our suppliers. We have to know who has access to sensitive information on designs, manufacturing facilities and projects.", :id => 10, :keywords => "" },
  {:category_id=> 11, :content => "My company provides support services for our products. We need to track the individual configurations and service activities for products in operation. Poor control of this information is a barrier to effective support and limits our return on support services. In some cases the relevant work is being done by our customer or a third party and we need them to be able to access and input data in a controlled fashion.", :id => 11, :keywords => "" },
  {:category_id=> 1, :content => "I am at the coffee shop and they are out of coffee!", :id => 12, :keywords => "" },
  {:category_id=> 10, :content => "let's try thin agains", :id => 13, :keywords => "" },
  {:category_id=> 2, :content => "I am sailing, and I want to open a business", :id => 14, :keywords => "" },
  {:category_id=> 6, :content => "My first situation", :id => 15, :keywords => "" },
  {:category_id=> 5, :content => "let's me start by asking another situation", :id => 16, :keywords => "" },
  {:category_id=> 5, :content => "Need recipe for pumpkin spice cake", :id => 17, :keywords => "" },
  {:category_id=> 6, :content => "I need to turn water into wine", :id => 18, :keywords => "" },
  {:category_id=> 6, :content => "I need to turn wine into water", :id => 19, :keywords => ""},
  {:category_id=> 2, :content => "I am stuck at the airport and need to get a taxi to the upper east side", :id => 20, :keywords => "" },
  {:category_id=> 2, :content => "I am at the airport in Dubai.  I cannot get to Israel", :id => 21, :keywords => ""},
  {:category_id=> 4, :content => "I'm at the airport in EWR, and my flight just canceled", :id => 22, :keywords => ""},
  {:category_id=> 2, :content => "I have a pizza recipe but no flour", :id => 23, :keywords => "" },
  {:category_id=> 2, :content => "I need to change $20 to quarters but there's no bank around here.", :id => 24, :keywords => "" },
  {:category_id=> 7, :content => "I need to change $5 to quarters but there's no bank around here!", :id => 25, :keywords => "" },
  {:category_id=> 2, :content => "I have a GE Cafe dishwasher, that does not run the soap dispenser.", :id => 26, :keywords => "" },
  {:category_id=> 2, :content => "I have stuck in Kansas and I can't get out because of the bad weather and hurricanes.", :id => 27, :keywords => "" },
  {:category_id=> 2, :content => "This is the longest situation anyone can post because we only allow 140 characters. This makes it easy to tweet at some point. Ok, done!!!!!", :id => 28, :keywords => "" },
  {:category_id=> 2, :content => "hello from firefox", :id => 29, :keywords => "" },
  {:category_id=> 10, :content => "I am stuck at the airport in New York, and my flight is canceled due to bad weather.", :id => 30, :keywords => "" },
  {:category_id=> 2, :content => "I am stuck in Manila and can't find a taxi.", :id => 31, :keywords => "" },
  ])




 Plan.create([
  {:content => "then you need to look at around and see what the person likes", :id => 1},
  {:content => "Tea, but I don't like it.", :id => 2},
  {:content => "have you tried giving this again", :id => 3},
  {:content => "wanted to try something else", :id => 4},
  {:content => "Red velvet cupcakes, but they didn't taste right.", :id => 5},
  {:content => "Going to the bakery on the corner", :id => 6},
  {:content => "Try calling your mom.", :id => 7},
  {:content => "This is my plan.", :id => 8},
  {:content => "Try drinking it.", :id => 9},
  {:content => "this is a test of somethign.", :id => 10},
  {:content => "need to test", :id => 11},
  {:content => "1. tried getting a bus\r\n2. tried to get a cab\r\n3. tried to rent a car", :id => 12},
  {:content => "called my travel agent", :id => 13},
  {:content => "tried pulling on the door frame", :id => 14},
  {:content => "try to change", :id => 15},
  {:content => "I tried calling my travel agent", :id => 16},
  {:content => "you need to rebook your flight by calling an airline", :id =>17},
  {:content => "what about trying to break the door", :id =>18}
  ])
=end
