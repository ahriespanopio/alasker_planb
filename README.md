# How to Setup
1.  Create a new directory for your PlanB program.
2.  Open your terminal/command line, and type __`git clone`__, do not press Enter yet.
3.	Copy and paste `git@bitbucket.org:planb_dev/plan-b.git` after the `git clone` command, and press Enter.
This will copy the entire history of PlanB project so you have it locally and it will give you a working directory of the main branch of that project so you can look at the code or start editing it.
If you change into the new directory, you can see the .git subdirectory - that is where all the project data is.
4.	Make sure you have a mysql account created for system to work database: planb, _username:_ __planb__, _password:_ __bcj4324#&df__
5.	Once you’ve got mysql configured, go ahead and go to the directory you cloned and copied the files for the project, then do Rails server
6.	Now your home page should show up, you’ll have to get started with some data (you’ll also need to put a cookie in your system, it changes, but it should be http://localhost:3000?access_key=p1an6
7.	You’ll now need to get some data into your system, the admin menu contains a bunch of links to put some testing data into the system
8.  Make sure you setup an admin account so you get to the admin page (having an admin user allows you to create data). To do this, first signup on the site and create an account, then use mysql to find your user information and set the admin boolean to true

Go to the [Wiki](https://bitbucket.org/planb_dev/plan-b/wiki/Home) for more details or if you have any issues.

