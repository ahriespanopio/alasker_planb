package com.alasker.android;


import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.RenderPriority;
import android.widget.Toast;

import org.apache.cordova.*;

public class AlaskerActivity extends DroidGap {

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.setBooleanProperty("hideLoadingDialogOnPage", true);
        super.setIntegerProperty("splashscreen", R.drawable.splash);
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String action = intent.getAction();
        String url = "file:///android_asset/www/index.html";

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 1)
            	if (segments.get(0).toLowerCase().equals("situations")) {
                url += "#question/" + segments.get(1);
                        
	            Context context = getApplicationContext();
	            int duration = Toast.LENGTH_LONG;	
	            Toast toast = Toast.makeText(context, "Loading question...", duration);
	            
	            toast.show();
            }
        }
        
      
        super.loadUrl(url, 5000);        
        super.appView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        super.appView.getSettings().setRenderPriority(RenderPriority.HIGH);
        super.appView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
    }    
}
