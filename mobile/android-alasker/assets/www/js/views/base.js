App.SimpleView = Backbone.View.extend({
	show: function(){		
	},
	hide: function(){		
	},
	afterHide: function(){
        this.stopListening();
	},
	afterShow: function(){
        this.listenTo(App.dispatcher, 'app:backbutton', this.onBackButton.bind(this));
	},
    render: function(){
        return this;
    },
	onBackButton: function(e){
        if(e && e.stopPropagation()){
            e.stopPropagation();
        }
        console.log('handle back button');
		window.history.back();
	}
});


App.BaseView = App.SimpleView.extend({
	bringToFront: function(){
		this.controller.changePage( this );
	}
});


