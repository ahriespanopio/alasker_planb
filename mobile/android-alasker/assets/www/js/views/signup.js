
App.SignUpView = App.SimpleView.extend({
    className:'register',
	events : {		
		'change #txt_email' : 'setEmail',
		'change #txt_username' : 'setUsername',
		'change #txt_password' : 'setPassword',
		'change #txt_confirm_password' : 'setConfirm',
	},
	show: function(){
		//$('#wrapper').addClass('front');
		//this.$username.focus();
		var errors = this.payload.errors || this.errors;
		
		this.errors = null;
		
		if ( errors )
			this.showError( errors );
	},
	initialize: function() {		
		var fnSubmit = this.submit.bind(this),
			fnError = this.showError.bind(this),
			self = this;
			
		this.$el.html( JST['signup']() );
		
		this.payload = {username:'',email:'',password:'', password_confirmation:''};
				
		this.$username = this.$el.find('#txt_username');
		this.$password = this.$el.find('#txt_password');
		this.$email = this.$el.find('#txt_email');
		this.$confirm = this.$el.find('#txt_confirm_password');
		
		this.$el.find('#btn_submit').on('touchstart', function(e){
			e.stopPropagation();
			if (self.isPosting)
				return;
			
			console.log('submitting...');
			if ( $.trim(self.payload.username) === '' )
				fnError({ username:['can not be blank'] });
			else if ( $.trim(self.payload.email) === '' )
				fnError({ email:['can not be blank'] });			
			else if ( $.trim(self.payload.password) === '' )
				fnError({ password:['can not be blank'] });
			else if ( self.payload.password === self.payload.password_confirmation)
				fnSubmit();
			else
				fnError({ password:['needs confirmation'] });
		});	
				
		if (arguments.length > 0) {
			this.auth =  arguments[0].payload.auth;
			this.errors = arguments[0].payload.errors;
			this.payload.username = this.auth.info['username'];
			this.payload.email = this.auth.info['email'];
			this.payload.password = '';
			this.payload.password_confirmation = '';
			this.$username.val(this.payload.username);
			this.$email.val(this.payload.email);
		}
    },
	submit: function(){
		if ( this.isPosting )
			return;
			
		if ( this.auth ) 
			this.oauth()
		else
			this.signup();
	},	
	oauth: function(){
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
	
		this.isPosting = true;
	
		this.auth.info.username = this.payload.username;
		this.auth.info.email = this.payload.email;
		this.auth.info.password = this.payload.password;
		
		model.url = App.url('auth/'+this.auth.provider+'/callback.json');
		
		App.loader.show();
		
		model.save( {auth:this.auth}, {
			success: fnSuccess, 
			error: fnError,
			complete: fnComplete
		} );
	},
	signup: function(){
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
	
		this.isPosting = true;
			
		model.url = App.url('users.json');
		
		App.loader.show();
			
		model.save( { user: this.payload }, {
			success: fnSuccess, 
			error: fnError,
			complete: fnComplete
		});
	},	
	onComplete: function(){
		this.isPosting = false;
		App.loader.hide();
	},	
	onSuccess: function(model, response) {
		console.log('*** signup successful');				
		App.setUser( response );				
		window.location.hash = 'home';				
		setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
	},	
	onError:function(model, xhr) {
		console.log('*** signup failed');		
		if (xhr.response === ""){
			App.toast('Connection error!');
		} else {
			var payload = JSON.parse(xhr.response);
			this.showError(payload);				
		} 
	},
	setUsername: function(){
		this.payload['username'] = this.$username.val();
	},
	setPassword: function(){
		this.payload['password'] = this.$password.val();
	},
	setConfirm: function(){
		this.payload['password_confirmation'] = this.$confirm.val();
	},
	setEmail: function(){
		this.payload['email'] = this.$email.val();
	},
	showError: function(errors){
		var error = this.$el.find('#error').first();
		
		error.html('');				
		$.each(errors, function(k, v) {		
			console.log(k + ' ' + v[0]);
			error.append('<li>'+ k + ' ' + v[0] + '</li>');
		});		
		error.show();
	},
	clearForm: function(){
		this.payload = {};
		this.$username.val('');
		this.$email.val('');		
		this.$password.val('');		
		this.$confirm.val('');
	},   
    render:function (eventName) {
        return this;
    }
});