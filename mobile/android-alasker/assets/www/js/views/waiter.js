
App.WaiterPopup = Backbone.View.extend({
    className: 'waiter-page',

	initialize: function(){
		this.isVisible = false;
		this.render();
	},
	
	show: function(){
		if( !this.isVisible ){		
			$('body').append(this.$el);
			
			if( !this.cl ){
				this.cl =  new CanvasLoader('waiter');				
				this.cl.setColor('#14b0e0');
				this.cl.setDiameter(50);
				this.cl.setRange(1);			
			}
			
			this.cl.show();
			
			this.isVisible = true;
		}
	},
	
	hide: function(){
		if( this.isVisible ){
			this.cl.hide();
			this.el.parentNode.removeChild(this.el)
			this.isVisible = false;
		}
	},
	
	render: function(){
		this.$el.html( JST['waiter']() );
		
		return this;
	}
});