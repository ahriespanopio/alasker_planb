App.GotAQuestionView = App.BaseView.extend({
	name: 'App.GotAQuestionView',

    className:'got-question-page',
		
	initialize: function(controller){
		this.controller = controller;	
		this.$el.html( JST['got-a-question']() );
		this.$content = this.$el.find('#txt_content');
		this.$next = this.$el.find('#next-btn');
		this.$back = this.$el.find('#back-btn');
        this.$next.on('touchstart', this.onNextButton.bind(this));
        this.$back.on('touchstart', this.onBackButton.bind(this));

		this.invalidate();
	},
	
	invalidate: function(){
		this.$content.val('');
	},
	
	show: function(){
		this.$content.val(this.controller.newQuestion);
	},

	render: function() {
        return this;
    },

    _goBack: function(){
        console.log('goBack');
        this.controller.cancelPost();
    },
		
	onNextButton: function(e){
        e.stopPropagation();

        var content = this.$content.val();

		if ( content ){
            this.controller.suggest( content );
		}
	},

	onBackButton: function(e){
        e.stopPropagation();
		this._goBack();
	}
});


App.SuggestQuestionsView = App.BaseView.extend({
	name: 'App.SuggestQuestionsView',

    className: 'suggestion-page',
	
	initialize: function(controller){
		this.controller = controller;
		this.$el.html( JST['suggest-questions']() );
		
		this.$next = this.$el.find('#next-btn');
		this.$back = this.$el.find('#back-btn');
        this.$next.on('touchstart', this.onNextButton.bind(this));
        this.$back.on('touchstart', this.onBackButton.bind(this));
	},
	
	invalidate:function( data ){ 
		this.data = data;
	},
	
	show: function(){
        var self = this;

        this.list_scroll =  new iScroll(this.$el.find('.scrollable')[0], {
                y: self.y,
                hideScrollbar: true,
                fadeScrollbar: true
            });
	},

    hide: function(){
        if( this.list_scroll ) {
            this.y = this.list_scroll.y;
            this.list_scroll.destroy();
            this.list_scroll = null;
        }
    },

    render: function() {
		var $wrapper = this.$el.find('.suggestions-stream'),
			template = _.template([
				'<% items.each(function(item) { %>',
				'	<%= part( item ) %>',
				'<% }); %>'
			].join(''));
			
		$wrapper.html('');
		
		var part = _.template([		
					'<div class="suggestion">',
					'<a href="#question/<%= id %>"><span><%= content%><span></a>',
					'</div>'
					].join('')),
			partLast = _.template([		
					'<div class="suggestion last">',
					'<a href="#question/<%= id %>"><span><%= content%><span></a>',
					'</div>'
					].join(''));
			
		for(var i=0; i<this.data.length-1; i++)
			$wrapper.append( part(this.data[i]) );
		
		$wrapper.append( partLast(this.data[this.data.length-1]) );
								
        return this;		
    },
		
	onNextButton: function(e){
        e.stopPropagation();
		this.controller.showNewQuestionPage();
	},
	
	onBackButton: function(e){
        e.stopPropagation();
		this.controller.showGotQuestionPage();
	}
});

App.NewQuestionView = App.BaseView.extend({
	name: 'App.NewQuestionView',

    className: 'edit-question-page',
	
	initialize: function(controller){
		this.controller = controller;
		this.$el.html( JST['edit-question']() );
		
		this.$content = this.$el.find('#content');
		this.$cancel = this.$el.find('#cancel-btn');
		this.$post = this.$el.find('#post-btn');

        this.$cancel.on('touchstart', this.onCancel.bind(this));
        this.$post.on('touchstart', this.onPost.bind(this));

	},
	
	invalidate: function(){
		this.$content.val('');
	},
	
	show: function(){

		this.$content.val( this.controller.newQuestion );
	},
	
	render: function() {
        return this;
    },
	
	onCancel: function(e){
        e.stopPropagation();
		console.log('cancel post');
		
		this.controller.cancelPost();
	},
	
	onPost: function(e){
        e.stopPropagation();
		console.log('save post');	
		
		this.controller.savePost();
	}
});



App.QuestionView = App.SimpleView.extend({
	name: 'App.QuestionView',

    className: 'question-page',
	
	initialize: function(){
		this.$el.html( JST['question']() );

        this.$back = this.$el.find('#back-btn');
        this.$write = this.$el.find('#write-btn');
        this.$menu = this.$el.find('#menu-btn');

        this.$back.on('touchstart', this.onBackButton.bind(this));
        this.$write.on('touchstart', this.onWrite.bind(this));
        this.$menu.on('touchstart', this.onMenu.bind(this));
	},
	
	setData: function( question_id ){
        console.log( question_id );
		
		this.question_id = question_id;
    },
	
	render: function() {
		var question = App.getCache('q')[this.question_id];
		if( question ){
			this._renderQuestion( question );
		}else{
			this._getQuestion(this.question_id);
		}
        return this;
    },

    show: function(){
        var self = this;

        this.$el.find('.timestamp').timeago();
        this.list_scroll =  new iScroll(this.$el.find('.question-wrapper')[0], {
                y: self.y,
                hideScrollbar: true,
                fadeScrollbar: true
            });
    },

    hide: function(){
        if( this.list_scroll ) {
            this.y = this.list_scroll.y;
            this.list_scroll.destroy();
            this.list_scroll = null;
        }
    },
	
	_renderQuestion: function(question){
		this.$el.find('#stream-wrapper').html( JST['question-stream-info'](question) );
		this.$el.find('.timestamp').timeago();
		if( !question['plans'] )
			this._getAnswers(question);
		else
			this._renderAnswers( new App.Plans(question['plans']) );
	},
	
	_renderAnswers: function(answers){
        var items = [];

        answers.models.each(function (m) {
            var tmp = m.attributes;
            tmp.content = _.escape(m.get('content'));
            tmp.avatar = App.url(m.get('avatar'));

            items.push(JST['question-answer-wrapper']({ 'data': m.attributes }));
        });

        this.$el.find('.answers-stream').html( items.join('') );
        this.$el.find('.content').addClass('answered');
        this.$el.find('.timestamp').timeago();
    },
	
	_getQuestion: function(question_id){
		var self = this,
            json = new App.Json(),
            cache = App.getCache('q');
		
		json.fetch({
			url: App.url('situations/'+ question_id +'.json'),
			success: function(model, response){
                var tmp = model.toJSON();
                cache[tmp.id] = tmp;

                self._renderQuestion(tmp);
			},
			error: function(model, xhr){
                App.toast(xhr.responseText);
                
            },
            complete: function(){
                
            }
		});
	},

    _getAnswers: function(){
        var self = this,
            answers = new App.Plans();

        if ( this.fetching )
            return;

        this.fetching = true;

        answers.fetch({
            url: App.url('situations/'+ this.question_id +'/plans.json'),
            success: function(model, response){
                if ( answers.length>0 ){
                    self._renderAnswers(answers);
                    self.show();
                }else{
                    self.$el.find('.content').removeClass('answered');
                }
            },
            error: function(model, xhr){
                App.toast(xhr.responseText);

                self.$el.find('.content').removeClass('answered');
            },
            complete: function(){
                self.fetching = false;
            }
        });
    },

    onWrite: function(e){
        e.stopPropagation();
        console.log('#onWrite');
        window.location.hash = 'post';
    },

    onFilter: function(e){
        e.stopPropagation();
        // show filter view
        console.log('#onFilter');
    },

    onMenu: function(e){
        e.stopPropagation();
        console.log('#onMenu');
        window.location.hash = 'menu';
    }
});
