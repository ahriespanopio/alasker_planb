
App.AuthenticateView = App.SimpleView.extend({
    className: 'theme_blue',

	initialize: function(){
		this.$el.html( JST['authenticate']() );
	},
	
	show: function(){		
	},
	
	hide: function(){
	},
		
	render: function(){
		return this;
	},	
});

App.AutologinView = App.SimpleView.extend({
    className: 'theme_blue',

    initialize: function(options){
        this.credential = options.credential;

        this.$el.html( JST['autologin']() );
    },

    show: function(){
        var model = new App.Json();
            credential = this.credential;

        App.loader.show();

        if ( !credential )
            credential = App.getUser();

        model.save( { user_login: credential }, {
            url: App.url('mobile/login'),
            success: function(session, response) {
                console.log('login succeeded');
                App.setUser( response );

                setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );

                location.hash = 'landing/list';
            },
            error: function(session, response) {
                console.log('login failed');
                try
                {
                    var json = JSON.parse(response.responseText);
                    App.toast(json.message);
                }
                catch(e)
                {
                    App.toast('Connection error!');
                }
                finally
                {
                    location.hash = 'authenticate';
                }
            },
            complete: function(){
                App.loader.hide();
            }
        });

    },

    hide: function(){
    },

    render: function(){
        return this;
    }
});