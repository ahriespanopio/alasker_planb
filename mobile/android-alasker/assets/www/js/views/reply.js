
App.ReplyView = App.SimpleView.extend({
    className: 'reply-page',

    initialize: function(question){
        this.question = question;
        this.$el.html( JST['reply']() );

        this.$content = this.$el.find('#content');
        this.$cancel = this.$el.find('#cancel-btn');
        this.$post = this.$el.find('#post-btn');

        this.$cancel.on('touchstart', this.onCancel.bind(this));
        this.$post.on('touchstart', this.onPost.bind(this));
    },

    invalidate: function(){
        this.$content.val('');
    },

    onCancel: function(e){
        e.stopPropagation();
        console.log('cancel answer');

        this.invalidate();

        history.back();
    },

    onPost: function(e){
        var plan = new App.Json(),
            payload,
            content = this.$content.val(),
            self = this;

        e.stopPropagation();
		
		if( !content && this.posting )
			return;

		console.log('post answer');

        App.loader.show();
			
		this.posting = true;

        payload = { new_situation_id:this.question.id, content:content };

        plan.save( { plan: payload }, {
            url: App.url('plans.json'),
            success: function(model, response){
                App.toast('Your answer has been sent');

                history.back();
            },
            error: function(model, xhr){
                App.toast(xhr.responseText);
            },
            complete: function(){
                App.loader.hide();
                self.posting = false;
            }
        });
    }
});
