App.LoginView = App.SimpleView.extend({
   className: 'register',
   initialize: function() {				
		this.$el.html( JST['signin'] );
	
		this.$username = $(this.el).find('#txt_login').first();
		this.$password = $(this.el).find('#txt_password').first();	
		this.clearForm();
    },
	show: function(){
		//$('#wrapper').addClass('front');
		//this.$username.focus();
	},
	events: {
		'submit form': 'login',
		'change #txt_login' : 'setUsername',
		'change #txt_password' : 'setPassword',
	},
	setUsername: function(){
		this.payload['login'] =  this.$username.val();
	},
	setPassword: function(){
		this.payload['password'] = this.$password.val() ;
	},
	showError: function(payload){
		var error = this.$el.find('#error').first();
		
		error.html('');		
		error.append('<li>'+ payload.message + '</li>');				
		error.show();	
	},
	clearForm: function(){
		this.payload = { login:'', password:'' };
		this.$username.val('');
		this.$password.val('');		
	},
	login: function(e) {
		e.preventDefault();

		if ( this.isPosting )
			return;
			
		this.isPosting = true;
		
		App.loader.show();
		
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
		
		model.url = App.url('mobile/login');		
		model.save( { user_login: this.payload }, {
			success: fnSuccess,
			error: fnError,
			complete: fnComplete
		});
	},
	onSuccess: function(model, response) {
		console.log('login succeeded');
		App.setUser( response );
		window.location.hash = 'home';
		
		setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
	},
	onError:function(model, xhr) {
		console.log('login failed');
		try 
		{	
			var payload = JSON.parse(xhr.response);
			this.showError(payload);				
		} 
		catch(e) 
		{
			App.toast('Connection error!');
		} 
		finally 
		{
			setTimeout( this.clearForm.bind(this), 100 );					
		}		
	},
	onComplete:function(){
		App.loader.hide();
		this.isPosting = false;
	},
    render:function (eventName) {
        return this;
    }
});