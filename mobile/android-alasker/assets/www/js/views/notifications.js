App.NotificationsView = App.SimpleView.extend({
    className: 'notification-page',
	notifications: null,
	initialize: function(){
		this.$el.html( JST['notification']() );
		
		this.$back = this.$el.find('#back-btn');
		this.$menu = this.$el.find('#menu-btn');
		
		this.$back.on('touchstart', this.onBackButton.bind(this));
		this.$menu.on('touchstart', this.onMenu.bind(this));
		
		this.pageOptions = { limit:10, delta:null, pivot:'top' };
		
		this.y = 0;
        this.eof = true;
        this.bof = true;
	},
	
	hide: function(){
		if( this.list_scroll ) {
			this.y = this.list_scroll.y;
			this.list_scroll.destroy();
			this.list_scroll = null;
		}
	},

	show: function() {
		console.log('show App.NotificationsView');
		var fnSuccess = this.createList.bind(this),
			fnError = this.showError.bind(this),
			fnComplete = this.refresh.bind(this);
					
		if (!this.notifications){			
			this.notifications = new App.JsonCollection();
			this.fetchData(fnSuccess, fnError, fnComplete, this.pageOptions);		
		} else {
			this.createScroll();
		}		
	},
	
	refresh: function(){
		this.list_scroll.refresh();
		this.busy = false;
	},
	
	showError: function(){
		App.toast('Connection error!');	
	},
	
	fetchData: function(fnSuccess, fnError, fnComplete, options){
		this.busy = true;
		
		this.notifications.fetch({
			url: App.url('notifications/getLatest.json', options),
			success: fnSuccess,
			error: fnError,
			complete: fnComplete
		});
	},
	
	_format: function(x){
		switch(x['event'])
		{
			case 3:
				x.action = 'wrote an answer';
				break;
			case 8:
				x.action = 'is now following you';
				break;
			case 13:
				x.action = 'made a comment';
				break;
			case 16:
				x.action = 'sent you a message';
				break;
			default:
				x.action = '';
		}		
		x.selectors = [];
		x.content = _.escape(x['content']);
		x.avatar = App.url(x['avatar']);
		if (x['unseen'])
			x.selectors.push('new');
		
		return x;
	},
		
	
	pullDown: function(){
		if (this.busy)
			return;
			
		console.log('pullDown');
		var fnSuccess = this.prependList.bind(this),
			fnError = this.showError.bind(this),
			fnComplete = this.refresh.bind(this);
			
		this.pageOptions.pivot = 'top';
		
		if (this.pageOptions.topModel)
			this.pageOptions.delta = this.pageOptions.topModel.get('created_at');
	
		this.fetchData(fnSuccess, fnError, fnComplete, this.pageOptions);
	},
	
	pullUp: function(){
		if (this.busy)
			return;		
	
		console.log('pullUp');
		var fnSuccess = this.appendList.bind(this),
			fnError = this.showError.bind(this),
			fnComplete = this.refresh.bind(this);
		
		this.pageOptions.pivot = 'bottom';
		
		if (this.pageOptions.botModel)
			this.pageOptions.delta = this.pageOptions.botModel.get('created_at');
		
		this.fetchData(fnSuccess, fnError, fnComplete, this.pageOptions);
	},
	
	appendList: function(){
		var self = this,
			items = [];
		
		if ( this.notifications.length > 0 ) {		
			var botModel = this.notifications.last();
			
			this.notifications.forEach(function (m) {
				var	tmp = m.toJSON();			
				tmp = self._format(tmp);				
				if( m == botModel )
					tmp.selectors.push('bottom');
				tmp.selectorClass = tmp.selectors.join(' ');
				items.push(JST['notifications-stream-content']({ 'data': tmp }));					
			});
			
			this.$el.find('.notification-stream').prepend(items.join(''));			
		
			this.pageOptions.botModel = botModel;
		}
		
		this.$el.find('.timestamp').timeago();
	},
	
	prependList: function(){	
		var self = this,
			items = [];
		
		if ( this.notifications.length > 0 ) {		
			var topModel = this.notifications.models[0];

			this.notifications.forEach(function (m) {
				var	tmp = m.toJSON();			
				tmp = self._format(tmp);	
				if( m == topModel )
					tmp.selectors.push('top');
				tmp.selectorClass = tmp.selectors.join(' ');
				items.push(JST['notifications-stream-content']({ 'data': tmp }));					
			});

            if (this.bof)
			    this.$el.find('.notification-stream').html(items.join(''));
            else
                this.$el.find('.notification-stream').prepend(items.join(''));
		
			this.pageOptions.topModel = topModel;
			this.pageOptions.delta = topModel.get('created_at');
            this.bof = false;
		}
		
		this.$el.find('.timestamp').timeago();
	},
	
	createScroll: function(){
		var self = this,
			pullUpAction = this.pullUp.bind(this),
			pullDownAction = this.pullDown.bind(this);
			
		this.pullDownEl = this.$el.find('#pullDown')[0];
		this.pullDownOffset = this.pullDownEl.offsetHeight;
		this.pullUpEl = this.$el.find('#pullUp')[0];
		this.pullUpOffset = this.pullUpEl.offsetHeight;
		
		this.list_scroll = new iScroll(this.$el.find('.scrollable')[0], {			
			momentum: true,
			hideScrollbar: true,
			topOffset: this.pullDownOffset,
			y: self.y,
			onRefresh: function () {
				if (self.pullDownEl.className.match('loading')) {
					self.pullDownEl.className = '';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
				} else if (self.pullUpEl.className.match('loading')) {
					self.pullUpEl.className = '';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';						
				}
			},				
			onScrollMove: function () {					
				if (this.y > 5 && !self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = 'flip';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Release to refresh...';
					this.minScrollY = 0;
				} else if (this.y < 5 && self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = '';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
					this.minScrollY = -self.pullDownOffset;
				} else if (!self.eof && this.y < (this.maxScrollY - 5) && !self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = 'flip';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Release to refresh...';
					this.maxScrollY = this.maxScrollY;						
				} else if (!self.eof && this.y > (this.maxScrollY + 5) && self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = '';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
					this.maxScrollY = self.pullUpOffset;						
				}
			},				
			onScrollEnd: function () {
				if ( this.busy ) return;
								
				if (self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = 'loading';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Loading...';				
					pullDownAction();
				} else if (self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = 'loading';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Loading...';										
					pullUpAction();	
				}
			}
		});		
	},
	
	createList: function(){
		console.log('createList');
		
		var self = this,
			items = [],
			topModel, botModel;

        this.eof = this.notifications.length < this.pageOptions.limit;
			
		if (this.notifications.length > 0) {
			topModel = this.notifications.models[0];
			botModel = this.notifications.last();
			this.pageOptions.delta = topModel.get('created_at');

            this.notifications.forEach(function (m) {
                var	tmp = m.toJSON();
                self._format(tmp);
                if( m == botModel )
                    tmp.selectors.push('bottom');
                else if( m == topModel )
                    tmp.selectors.push('top');
                tmp.selectorClass = tmp.selectors.join(' ');
                items.push(JST['notifications-stream-content']({ 'data': tmp }));
            });
            this.$el.find('.notification-stream').html(items.join(''));
            this.bof = false;
        } else {
            this.$el.find('.notification-stream').html(JST['notifications-stream-empty']);
        }

        this.$el.find('#pullDown').show();
        if (this.notifications.length >= this.pageOptions.limit)
            this.$el.find('#pullUp').show();

        this.$el.find('.timestamp').timeago();

        this.createScroll();
	},

	render: function() {
		return this;
	},		
	
	onMenu: function(e){
		console.log('#onMenu');
        e.stopPropagation();
		location.hash = 'menu';
	}
});
