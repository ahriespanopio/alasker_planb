
App.MenuView = App.SimpleView.extend({
    className: 'menu-page',

	initialize: function(){
		this.$el.html( JST['menu']() );
		
        this.$el.find('#back-btn').on('touchstart', this.onBackButton.bind(this));
        this.$el.find('#notifications-menu').on('touchstart', this.onNotifications.bind(this));
        this.$el.find('#help-menu').on('touchstart', this.onHelp.bind(this));
        this.$el.find('#exit-menu').on('touchstart', this.onExit.bind(this));
    },

    onNotifications: function(e){
        e.stopPropagation();
        App.router.navigate('notifications', {trigger:true, replace:true})
    },

    onHelp: function(e){
        e.stopPropagation();
        App.router.navigate('help', {trigger:true, replace:true})
    },

    onExit: function(e){
        e.stopPropagation();
        App.router.navigate('exit', {trigger:true, replace:true})
    }

});


App.HelpView = App.SimpleView.extend({
    className: 'help-page',

    initialize: function(){
        this.$el.html( JST['help']() );

        this.$el.find('#back-btn').on('touchstart', this.onBackButton.bind(this));

        this.y = 0;
    },

    show: function(){
        var self = this;

        this.list_scroll = new iScroll('help-wrapper', {
            y: self.y,
            hideScrollbar: true,
            fadeScrollbar: true
        });
    },

    hide: function(){
        if( this.list_scroll ) {
            this.y = this.list_scroll.y;
            this.list_scroll.destroy();
            this.list_scroll = null;
        }
    }
});