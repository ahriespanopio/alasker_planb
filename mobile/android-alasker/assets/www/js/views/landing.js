
App.LandingView = App.SimpleView.extend({
    className: 'landing-page',
	situations: null,
	type: null,
	list_scroll: null, // TODO: put the list in this scope so that we can easily destroy it

	initialize: function(){
		this.$el.html( JST['landing']() );	
		
		this.$list = this.$el.find('#list-btn');
		this.$write = this.$el.find('#write-btn');
		this.$filter = this.$el.find('#filter-btn');
		this.$menu = this.$el.find('#menu-btn');
		
		this.$list.on('touchstart', this.onList.bind(this));
		this.$write.on('touchstart', this.onWrite.bind(this));
		this.$filter.on('touchstart', this.onFilter.bind(this));
		this.$menu.on('touchstart', this.onMenu.bind(this));

		this.situations = new App.Situations();		
		
		this.y = 0;
		this.loading = false;
		this.swapped = false;
		this.eof = true;
		
		this.scrollOptions = {};
		
		var cache = App.getCache('q');
		cache = [];
	},

	setType: function(type) {
		this.type = type;			
	},
	
	hide: function(){
		this.swapped = true;
		if( this.list_scroll ) {
			this.y = this.list_scroll.y;
			this.list_scroll.destroy();
			this.list_scroll = null;
		}
	},
	
	show: function() {
		console.log('show App.LandingView');
		var self = this;
			fnShowList = this.showList.bind(this),
			fnShowMap = this.showMap.bind(this);
		
		// fetch only once, then fetch from pullup and pulldown afterwards

		if (self.type == 'list') {
			if ( this.swapped ){
				this.$el.find('#stream-wrapper').html(JST['landing-stream-wrapper-pull']());
				fnShowList();
			}else{			
				self.situations.fetch({
					url:App.url('situations/getLatest.json', self.situations.pageOptions),
					success: function() {
						fnShowList();
					},
					error: function() {
						App.toast('Connection error!');
					},
				});
			}
		} else {
			/*self.situations.fetch({
				url:App.url('situations/getLatest.json', self.situations.pageOptions),
				success: function() {
					self.showMap();
				},
				error: function() {
					App.toast('Connection error!');
				},
			});*/
			fnShowMap();
		}
	},

	showList: function() {
		console.log('showList');

		var self = this;
		var items = [];
		//var items_start = self.situations.at(0).id;
		//var items_end = self.situations.at(self.situations.length - 1).id;
		//var items_downscroll_page = 1;
		var cache = App.getCache('q');
		
		self.situations.ready = true;
		
		self.eof = self.situations.length < self.situations.pageOptions.limit;
		
		if( self.situations.length > 0 ){			
			var last_item = self.situations.last();
			
			self.situations.pageOptions.delta = last_item.get('created_at');
		
			self.situations.models.each(function (m) {
					var tmp = m.attributes;
					tmp.content = _.escape(m.get('content'));
					tmp.avatar = App.url(m.get('avatar'));
					
					cache[m.id] = m.attributes;

					items.push(JST['landing-stream-content'](m.attributes));
			});

			//$('#stream-wrapper').html(JST['landing-stream-wrapper-pull']());
			$('#stream-wrapper .thelist').html(items.join(''));
			
			$('#pullDown').show();
			
			if ( self.eof ) 
				self.$el.find('#pullUp').hide();
			else 
				self.$el.find('#pullUp').show();
									
		}
		
		$('.timestamp').timeago();

		this.pullDownEl = document.getElementById('pullDown');
		this.pullDownOffset = this.pullDownEl.offsetHeight;
		this.pullUpEl = document.getElementById('pullUp');	
		this.pullUpOffset = this.pullUpEl.offsetHeight;

		function pullDownAction() {
			if ( self.loading )
				return;
				
	
			self.loading = true;
			
			cache = App.resetCache('q');
				
			console.log('pullDownAction');
			/*var pageOptions = {
				sort: 'newest',
				by: 0,
				page:0,
				paginate:0,
				limit:15,
				delta: null
			};*/

			//self.situations.url = App.url('situations/getLatest.json', pageOptions);
			var pageOptions = self.situations.pageOptions;
			pageOptions.delta = null;
			
			self.situations.fetch({
				url: App.url('situations/getLatest.json', pageOptions),
				success: function() {
					var items = [];

					self.situations.ready = true;
					
					if (self.situations.length > 0){
						var last_item = self.situations.last();
						
						self.situations.pageOptions.delta = last_item.get('created_at');
						
						self.eof = self.situations.length < self.situations.pageOptions.limit;
										
						self.situations.models.each(function (m) {
							//if (m.id > items_start) {
								var tmp = m.attributes;
								tmp.content = _.escape(m.get('content'));
								tmp.avatar = App.url(m.get('avatar'));
								
								cache[m.id] = m.attributes;

								items.push(JST['landing-stream-content'](m.attributes));
							//}
						});

						//items_start = self.situations.at(0).id;
						//items_end = self.situations.at(self.situations.length - 1).id;
						self.$el.find('#stream-wrapper .thelist').html(items.join(''));
						
						if( self.eof ) 
							self.$el.find('#pullUp').hide();
						else
							self.$el.find('#pullUp').show();
							
						self.pullUpEl.className = '';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
						
						self.list_scroll.refresh();
						
						self.$el.find('.timestamp').timeago();
					}
				},
				error: function() {
					App.toast('Connection error!');
				},
				complete: function(){
					console.log('completed pull down action');
					self.loading = false;
				}
			});
		}

		function pullUpAction() {
						
			if( self.loading || self.eof )
				return;
				
			console.log('pullUpAction');
			
			/*var pageOptions = {
				sort: 'newest',
				by: 0,
				page: items_downscroll_page,
				paginate:1,
				limit:15,
				delta: null
			};*/		

			//self.situations.url = App.url('situations/getLatest.json', pageOptions);			
			
			var pageOptions = self.situations.pageOptions;
			
			self.situations.fetch({
				url: App.url('situations/getLatest.json', pageOptions),
				success: function() {
					var items = [];

					self.situations.ready = true;
					console.log(pageOptions);
					console.log(self.situations.models);
					
					self.eof = self.situations.length < self.situations.pageOptions.limit;
					
					if( self.situations.length > 0 ){
						var last_item = self.situations.last();
						
						self.situations.pageOptions.delta = last_item.get('created_at');
					
					
						self.situations.models.each(function (m) {					
						
							//if (m.id < items_end) {
								tmp = m.attributes;
								tmp.content = _.escape(m.get('content'));
								tmp.avatar = App.url(m.get('avatar'));
								
								cache[m.id] = m.attributes;

								items.push(JST['landing-stream-content'](m.attributes));
							//}
						});

						//items_start = self.situations.at(0).id;
						//items_end = self.situations.at(self.situations.length - 1).id;
						//items_downscroll_page++;
						$('#stream-wrapper .thelist').append(items.join(''));
																						
						self.list_scroll.refresh();
						
						self.$el.find('.timestamp').timeago();
					}
					
					if ( self.eof ) {
						self.pullUpEl.className = '';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
						self.$el.find('#pullUp').hide();
						self.list_scroll.refresh();
					}
						
				},
				error: function() {
					App.toast('Connection error!');
				},
				complete: function(){
					console.log('completed pull up action');
					self.loading = false;
				}
			});			
			
		}
		
		self.list_scroll = new iScroll('stream-wrapper', {
			y: self.y,
            hideScrollbar: true,
            fadeScrollbar: true,
			topOffset: self.pullDownOffset,
			onRefresh: function () {
				if (self.pullDownEl.className.match('loading')) {
					self.pullDownEl.className = '';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
				} else if (self.pullUpEl.className.match('loading')) {
					self.pullUpEl.className = '';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
				}
			},
			onScrollMove: function () {
				if (this.y > 5 && !self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = 'flip';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Release to refresh...';
					this.minScrollY = 0;
				} else if (this.y < 5 && self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = '';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
					this.minScrollY = -self.pullDownOffset;
				} else if (!self.eof && this.y < (this.maxScrollY - 5) && !self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = 'flip';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Release to refresh...';
					this.maxScrollY = this.maxScrollY;
				} else if (!self.eof && this.y > (this.maxScrollY + 5) && self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = '';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
					this.maxScrollY = pullUpOffset;
				}
			},
			onScrollEnd: function () {
				if ( self.loading ) return;
				
				if (self.pullDownEl.className.match('flip')) {
					self.pullDownEl.className = 'loading';
					self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Loading...';				
					pullDownAction();
				} else if (self.pullUpEl.className.match('flip')) {
					self.pullUpEl.className = 'loading';
					self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Loading...';				
					pullUpAction();
				}
			}
		});
	},

	showMap: function() {
		console.log('#showMap');
		var self = this,
            showMapWindow = function (lat, lng){
                    console.log(lat);
                    console.log(lng);
                    self.infoWindow = new App.GoogleMapInfowindow({
                        el: self.$el.find('#stream-wrapper')[0],
                        lat: lat,
                        lng: lng,
                        zoom: 6,
                        markers: self.situations
                    });
            };

        if( App.currentPosition ){
            showMapWindow(App.currentPosition.latitude, App.currentPosition.longitude) ;
        } else {
            console.log('reading position');
            navigator.geolocation.getCurrentPosition(
                function(pos) {
                    console.log('position found');
                    App.currentPosition = pos.coords;
                    showMapWindow(App.currentPosition.latitude, App.currentPosition.longitude) ;
                },
                function(error) {
                    console.log('geolocation error');
                    console.log('code: ' + error.code);
                    console.log('message: ' + error.message);
                    window.location.hash = 'landing/list'
                }
            );
        }

		/*if (self.list_scroll) {
			self.list_scroll.destroy();
			self.list_scroll = null;
		}*/


	},

	render: function() {
		if ( this.swapped ){
			var stream = App.getCache('q').slice();					
			stream.sort(function(a,b){return b.id-a.id});
			var temp = [];
			_.each(stream, function(q){ temp.push(q) });
			this.situations = new App.Situations( temp );	
			this.situations.pageOptions.delta = temp[temp.length-1].created_at;			
		}
	
		return this;
	},	
	
	onList: function(e){
        e.stopPropagation();

		// hack hack hack
		console.log('#onList');

		console.log(this.type);

		if (this.type == 'list') {
			//App.router.navigate('landing/map', {trigger: true, replace: true});
            setTimeout(function(){window.location.hash = 'landing/map'}, 500);
		} else {
			//App.router.navigate('landing/list', {trigger: true, replace: true});
             setTimeout(function(){window.location.hash = 'landing/list'}, 500);
		}
	},
	
	onWrite: function(e){
        e.stopPropagation();
		console.log('#onWrite');
        window.location.hash = 'post';
	},
	
	onFilter: function(e){
        e.stopPropagation();
		// show filter view
		console.log('#onFilter');
	},
	
	onMenu: function(e){
        e.stopPropagation();
		console.log('#onMenu');
		window.location.hash = 'menu';
	}
});
