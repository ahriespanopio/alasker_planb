/* Models */

App.Json = Backbone.Model.extend({});

App.Notification = Backbone.Model.extend({});

App.SuggestedQuestions = Backbone.Model.extend({});

App.Situation = Backbone.Model.extend({});

/* Collections */

App.JsonCollection = Backbone.Collection.extend({
    model: App.Json,
    parse: function(response) {
        return response.items;
    }
});

App.Plans = Backbone.Collection.extend({
    model: App.Json
});

App.Situations = Backbone.Collection.extend({
	ready: false, // flag if JSON data is ready
	pageOptions: {
		sort: 'newest',
		by: 0,
		page: 0,
		paginate: 1,
		limit: 10,
		delta: null
	},
	// url : App.url('situations.json'),
	//url: App.url('situations/getLatest.json', this.pageOptions),
	model: App.Situation,
	type: '', // list or map

	// Just fetch manually to control the sync
	/*
	initialize: function() {
		var self = this;

		self.fetch({
			success: function () {
				self.ready = true;
			}
		});
	},
	*/
	
	eof: function(){
		return this.length == 0 || this.length < this.pageOptions.limit;
	},

	parse: function(response) {
		return response.items;
	},
	search: function(filter) {
		if (filter) {
			this.url = App.url('situations.json', {'situationsearch' : '+'+filter});
		} else {
			this.url = App.url('situations.json', {});
		}
	}
});
