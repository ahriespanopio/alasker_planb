JST = {};

tpl = {

    // Recursively pre-load all the templates for the app.
    // This implementation should be changed in a production environment. A build script should concatenate
    // all the template files in a single file.
    loadTemplates: function(names, callback) {

        var that = this;

       var loadTemplate = function (index) {
            var name = names[index];
            console.log('Loading template: ' + name);

            var data = $.ajax({type: 'GET', url: 'templates/' + name + '.html', async: false}).responseText;
			
			JST[name] = _.template(data);
			
            index++;
            if (index < names.length) {
                loadTemplate(index);
            } else {
                callback();
            }
        }

        loadTemplate(0);
    }
};

