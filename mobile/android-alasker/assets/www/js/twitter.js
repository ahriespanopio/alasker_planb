// ------------------------------
// Twitter
// ------------------------------
function Twitter(success, error) {
	var cb = App.getBrowser(),
		key = 'twitter',
		options = App.config.twitter,
		oauth = null,
		onSuccess = success || function(){},
		onError = error || function(){},
		tw = Object.create(Twitter.prototype, {		
			authenticate: {
				value: function(){		
					var self = this, 
						userData = this.getUserData();
					
					// verify stored credentials
					if ( userData ) {						 
						options.accessTokenKey = userData.accessTokenKey; 
						options.accessTokenSecret = userData.accessTokenSecret; 
						
						// give access to this user
						oauth = OAuth(options);
						oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
							function(data) { onSuccess( JSON.parse(data.text) ); },
							function(data) { self.request(); }
						);
					}
					else {
						self.request();
					}
				}
			},
			
			request: {
				value: function(){
					var self = this;
					
					console.log('obtain unathorized request token');
					// obtain unathorized request token
					options.accessTokenKey = null;
					options.accessTokenSecret = null;
					oauth = OAuth(options);
					oauth.get('https://api.twitter.com/oauth/request_token',
						function(data) {
							self.requestParams = data.text;									
							// direct user to twitter service
							cb.showWebPage('https://api.twitter.com/oauth/authorize?'+data.text);	
							cb.onClose = function(){
								onError({});
							};
							// handle redirect from twitter
							cb.onLocationChange = function(loc){ 
								self.callback(loc); 
							};
						},
						function(data) { onError(data); }
					);
				}
			},
				
			callback: {
				value: function(loc){
					var self = this;
					console.log('TWITTER REDIRECT: '+loc);
					
					// check if this is a twitter callback
					if (loc.indexOf(App.config.twitter.callbackUrl) >= 0) {
						
						// kill childBrowser events
						cb.onLocationChange = function(){};
						
						// we need oauth_verifier value in requesting for access token 
						var verifier = '';            
						var params = loc.substr(loc.indexOf('?') + 1);
						
						params = params.split('&');
						for (var i = 0; i < params.length; i++) {
							var y = params[i].split('=');
							if(y[0] === 'oauth_verifier') {
								verifier = y[1];
							}
						}
						
						if ( verifier ) {
							console.log('exchange request token for access token');
									
							// exchange request token for access token
							oauth.get('https://api.twitter.com/oauth/access_token?oauth_verifier='+verifier+'&'+self.requestParams,
								function(data) {    		
									var accessData = {};
									var qvars_tmp = data.text.split('&');
									for (var i = 0; i < qvars_tmp.length; i++) {
										var y = qvars_tmp[i].split('=');
										accessData[y[0]] = decodeURIComponent(y[1]);
									}
										
									// give access to user
									oauth.setAccessToken([accessData.oauth_token, accessData.oauth_token_secret]);				
									oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
										function(data) { 
											self.putUserData( accessData );
											onSuccess( JSON.parse(data.text) ); 											
										},
										function(data) { onError(data); }
									);
									
									// Since everything went well we can close our childBrowser!                             
									cb.close();
								},
								function(data) { onError(data); }
							);
						} else {
							console.log('cancelled');
							cb.close();
							onError({});
						}
					}
				}
			},
			
			getToken: {
				value: function(){
					var data = this.getUserData();
					if ( data ) 
						return data.accessTokenKey;
						
					return null;
				}
			},

			getUserData: {
				value: function(){	
					// read data from store
					var rawData = localStorage.getItem(key);
					
					// convert to JSON object
					if(rawData){
						return JSON.parse(rawData);
					}	
					
					// no data 
					return null;
				}
			},
			
			putUserData: {
				value: function(accessData){
					// get what we need
					var userData = {
						accessTokenKey : accessData.oauth_token,
						accessTokenSecret : accessData.oauth_token_secret
					};
					
					// store it 
					localStorage.setItem(key, JSON.stringify(userData));
				}
			},
			
			tweet: {
				value: function(status){				
					oauth.post('https://api.twitter.com/1.1/statuses/update.json',
						{ 
							'status' : status, 
							'trim_user' : 'true' 
						},
						function(data) {
							console.log( 'tweet succeeded:' /*+ data.text*/ );
						},
						function(data) { 
							console.log( 'tweet encountered error:');
						}
					);		
				}
			}		
		
		});
		
		tw.constructor = Twitter;
		
		return tw;
};


