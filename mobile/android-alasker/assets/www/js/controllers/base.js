App.ClassFactory = function() {
    return function() {
        if (this.initialize) {
            return this.initialize.apply(this, arguments);
        } else {
            throw ('initialize method is missing...');
        }
    }
}

App.ClassFactory.extend = function(base) {
	var target = App.ClassFactory();
	
    for (var prop in base) {
        target.prototype[prop] = base[prop];
    }
    return target;
};
