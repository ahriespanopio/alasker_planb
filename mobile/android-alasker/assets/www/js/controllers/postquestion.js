App.PostQuestionController = App.ClassFactory.extend({

	changePage: function(){},
	
	initialize: function(){
		this.views = 
		{
			gotaQ : new App.GotAQuestionView(this),
			suggestQ : new App.SuggestQuestionsView(this),
			newQ : new App.NewQuestionView(this),
		}
		
		this.invalidate();		
	},
	
	invalidate: function(){
		this.newQuestion = '';
		this.views.gotaQ.invalidate();
		this.currentView = this.views.gotaQ;
	},
	
	cancelPost: function(){
		this.invalidate();

        history.back();
	},
	
	savePost: function(){
		if( this.isPosting )
			return;
		
		App.toast('Sending your question...');
			
		this.isPosting = true;
	
		var self = this,		
			post = new App.Json(),
			payload = { content:_.escape(this.newQuestion) };
			
		App.loader.show();
		
		post.save( { situation:payload }, {
			url: App.url('situations.json'),
			success: function(model, response){
				App.toast('Your question has been sent');
				
				self.cancelPost();
			},
			error: function(model, xhr){
				App.toast(xhr.responseText);
			},			
			complete: function(){
				App.loader.hide();
				self.isPosting = false;				
			},
		});
	},
	
	suggest: function( newQuestion ){
		this.newQuestion = newQuestion;
		this._getSuggestions();
	},
	
	showView: function(){
		this.bringToFront( this.currentView );
	},
	
	showGotQuestionPage: function(){
		this.bringToFront( this.views.gotaQ );
	},
	
	showSuggestionsPage: function( data ){
		if (data.length ) {
			this.views.suggestQ.invalidate( data );
			this.bringToFront( this.views.suggestQ );
		}else{
			this.bringToFront( this.views.newQ );
		}
	},
	
	showNewQuestionPage: function(){
		this.bringToFront( this.views.newQ );
	},
	
	bringToFront: function( view ){
		this.currentView = view;
		view.bringToFront();
	},
	
	_getSuggestions : function() {
		var fnCallback = this.showSuggestionsPage.bind(this),
			situations =  new App.SuggestedQuestions(); 
		
		App.loader.show();
		
		situations.fetch({
			url:  App.url('situations.json', {'situationsearch' : '+'+this.newQuestion}),			
			success: function(model, data){		
				fnCallback( data );
			},
            error: function(data, xhr) {
				/*situations = new App.Situations([
					{'id':1, 'content':"Lorem ipsum dolor sit amet, consectetur adipiscing elit"},
					{'id':2, 'content':"Quisque id elit tincidunt elit pellentesque porttitor"},
					{'id':3, 'content':"In sit amet lectus at urna facilisis condimentum vel sed urna"},
					{'id':4, 'content':"Suspendisse id nulla viverra lacus mollis suscipit id non metus"},
					{'id':5, 'content':"Etiam et tortor vel arcu faucibus iaculis in non nunc"},
					{'id':6, 'content':"Cras non lorem sit amet dolor ornare vulputate a non ipsum"},
					{'id':7, 'content':"Vestibulum id dolor a risus placerat hendrerit at condimentum risus"},
					{'id':8, 'content':"Mauris laoreet lectus vitae nisi pulvinar egestas"},
					{'id':9, 'content':"Proin vel lacus quis neque ultrices consectetur non eget lorem"},
				]);	*/
            },
			complete: function(){
				//callback.call(null, situations);
				App.loader.hide();
			}
        });
	}	
});