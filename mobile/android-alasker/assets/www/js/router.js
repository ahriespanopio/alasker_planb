App.Router = Backbone.Router.extend({
   controllers: {},
   
   routes: {
        '' : 'main',
        'menu' : 'menu',
        'exit' : 'logout',
        'help' : 'help',
        'home' : 'landingList',
        'landing/list' : 'landingList',
        'landing/map' : 'landingMap',
        'notifications' : 'notifications',
        'signin' : 'signin',
        'signup' : 'signup',
        'logout' : 'logout',
        'questions': 'questions',
        'facebook' : 'facebook',
        'twitter' : 'twitter',
        'question/:id' : 'question',
        'question/:id/reply' : 'reply',
        'question/:id/chat': 'chat',
        'post' : 'postQuestion',
        'autologin' : 'autologin',
        'authenticate' : 'authenticate'
	},
	
	main: function() {
        console.log('#main');

        App.getUser();

        if ( App.currentUser ) {
            // automatically signin user
            //this.navigate('autologin', {trigger: false, replace: false})
            this.autologin();
        } else {
            // proceed to default
            //this.navigate('authenticate', {trigger: false, replace: false})
            this.authenticate();
        }
    },

    help: function(){
        console.log('#help');

        if ( !App.pages['help'] )
            App.pages.help = new App.HelpView();

        this.changePage( App.pages.help );
    },
	
	menu: function(){
		console.log('#menu');
		
		if ( !App.pages['menu'] )
			App.pages.menu = new App.MenuView();

		this.changePage( App.pages.menu );
	},
	
	question: function( id ){
		if ( !App.pages.question )
			App.pages.question = new App.QuestionView();
			
		App.pages.question.setData( id );
			
		this.changePage( App.pages.question );
	},
	
	landingList: function(){
		console.log('#landingList');

		if ( !App.pages['landing'] )
			App.pages['landing'] = new App.LandingView();
		
		App.pages['landing'].setType('list');
		this.changePage( App.pages['landing'] );	
	},

	landingMap: function(){
		console.log('#landingMap');

		if ( !App.pages['landing'] )
			App.pages['landing'] = new App.LandingView();
		
		App.pages['landing'].setType('map');
		this.changePage( App.pages['landing'] );	
	},

	notifications: function() {
		console.log('#notifications');

		if ( !App.pages['notifications'] )
			App.pages['notifications'] = new App.NotificationsView();
		
		this.changePage( App.pages['notifications'] );	
	},
	
	postQuestion: function(){
		console.log('#postQuestion');		
		
		if ( !App.controllers.postQuestion ) {
			// in the future we'll instantiate this from a container 
			App.controllers.postQuestion = new App.PostQuestionController();
			_.extend( App.controllers.postQuestion, { changePage:this.changePage.bind(this) }  )
		}
			
		App.controllers.postQuestion.showView();
	},
	
	reply: function( id ){	
		console.log('#reply');
		
		var cache = App.getCache('q');
		this.changePage( new App.ReplyView( cache[id] ) );		
	},
	
	chat: function( id ){
		console.log('#chat');		

		var cache = App.getCache('q');		
		this.changePage( new App.ChatPopup( cache[id] ) );
	},	

	authenticate: function(){
		console.log('#authenticate');
		this.changePage(new App.AuthenticateView());
	},
	
	questions: function(){		
		console.log('#questions');
		App.dispatcher.trigger('popup:close');	
		if ( !App.pages['questions'] )
			App.pages['questions'] = new App.QuestionsView();
		
		this.changePage( App.pages['questions'] );	
	},
	
	signup:function () {
        console.log('#signup');
        this.changePage(new App.SignUpView());
    },
	
	signin:function() {
        console.log('#signin');
        this.changePage(new App.LoginView());
    },
	
	autologin:function( credential ){
        console.log('#autologin');

        this.changePage(new App.AutologinView({credential:credential}));
	},

	logout: function() {
		console.log('#signout');
		var self = this,
            user = App.currentUser;

        if ( !user ){
            window.location.hash = '';
            return;
        }
		
		App.loader.show();
		
		$.ajax({
            url: App.url('mobile/logout'),			
            dataType: 'json',
            async:true,
            type:'DELETE',
			data: {user_login: {login: user.username}},
            success: function(p1,p2,p3){
				console.log('logout success');
				setTimeout(function(){ App.dispatcher.trigger('app:logout'); }, 500);

                window.location.hash = 'authenticate';
			},
            error: function(p1,p2,p3){
				console.log('Connection error!');
			},
			complete: function(){
				App.loader.hide();
			}
        });
	}, 
	
	fbComplete: function(data){
		console.log('#fbComplete');		
        this.changePage(new App.SignUpView( { payload:data } ));
	},
		
	facebook: function(){	
		var fnComplete = this.fbComplete.bind(this),		
			success = function(data) {					
				fb.getUserData(
					function(data)
					{											
						var	json = new App.Json();							
						json.url = App.url('auth/:facebook/callback.json');
						json.save( { auth:data }, {
							success: function(model, response){
								if( response.authenticated ){
									App.setUser( response );
									window.location.hash = 'home';
									setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
								}else{
									fnComplete(response);
								}
							},
							error: function(mode, xhr){
								window.location.hash = 'authenticate';
							}
						} );						
					}, 
					function()
					{
						window.location.hash = 'authenticate';
					}
				);				
			},
			error = function(data){
				window.location.hash = '';
				App.checkInternet();
			},		
			fb = new Facebook(success, error);
			
		fb.authenticate();		
	},
	
	twitter: function() {
		var fnComplete = this.fbComplete.bind(this),	
			success = function(data){					
					console.log('Twitter SUCCESS'); 
										
					payload = {
						provider:'twitter',
						uid: data.id,						
						info: {
							username: data.screen_name							
						},
						credentials: {
							token: tw.getToken()									
						}
					};
					
					var	json = new App.Json();							
						json.url = App.url('auth/:twitter/callback.json');
						json.save( { auth:payload }, {
							success: function(model, response){
								if( response.authenticated ){
									App.setUser( response );
									window.location.hash = 'home';
									setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
								}else{
									fnComplete(response);
								}
							},
							error: function(mode, xhr){
								window.location.hash = 'authenticate';
								App.toast( 'Connection error!' );
							}
						} );						
					
					window.location.hash = '';
				},
			error = function(data){
					console.log('Twitter ERROR'); 
					window.location.hash = 'authenticate';
					
					App.checkInternet();
				},
			tw = new Twitter(success, error);
		
		tw.authenticate();		
	},
		
    changePage: function (page) {						
		
		if( window.__page ){
			if ( window.__page.hide ) {
				window.__page.hide();
				window.__page.afterHide();
			}
				
			$('#page_wrapper')[0].removeChild(window.__page.el)
		}
				
		window.__page = page.render();	
	
		$('#page_wrapper')[0].appendChild(page.el);
		
		page.show();
		page.afterShow();
    }
});


