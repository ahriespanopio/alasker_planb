;(function( App ){

	var K_USER 		= 'user',
		K_SETTING 	= 'setting';

    var Db =  function(){
        var me = Object.create(Db.prototype, {
			__clear: {
				value: function( key ){
					localStorage.removeItem(key);
				}
			},
		
			__get: {
				value: function( key ) {
					var sval = localStorage.getItem(key);				
					if( sval ) {
						return JSON.parse(sval);
					} 
					return null;
				}
			},			
			
			__set: {
				value: function( key, oValue ) {
					if( oValue && 'object' === typeof oValue ){
						localStorage.setItem(key, JSON.stringify(oValue));										
					} else {
						localStorage.removeItem(key);
					}						
				}				
			},
			
			clearUser: {
				value: function(){
					this.__clear(K_USER);
					App.currentUser = null;
				}
			},
			
            getUser: {
                value: function(){
					if ( !App.currentUser ){
						App.currentUser = this.__get(K_USER);
					}					
                    return App.currentUser;
                }
            },

            setUser: {
                value: function( newUser ){					
					this.__set(K_USER, newUser);
					App.currentUser = this.__get(K_USER);
					return App.currentUser;
                }
            },

            getSettings: {
                value: function(){
                    return this.__get(K_SETTING);
                }
            },

            setSettings:
            {
                value: function( oValue ){
					this.__set(K_SETTING, oValue);
					return this.__get(K_SETTING);
                }
            }
        });

        me.constructor = Db;

        return me;
    }

    var PopupStack = function() {
        var data = [],
            me = Object.create(PopupStack.prototype, {
                peek: {
                    value: function(){
                        if( data.length > 0 ) {
                            return data[data.length-1];
                        }

                        return null;
                    }
                },
                push: {
                    value: function( state ){
                        data.push(state);
                    }
                },
                pop: {
                    value: function(){
                        if ( data.length > 0 ) {
                            return data.pop();
                        }

                        return null;
                    }
                }
            });

        me.constructor = PopupStack;

        return me;
    }

    var ChatProxy = function(){
        var situation_id,
            lastPolled,
            isPolling,
			isStopped,
            pulse,
			fnWin,
			fnFail,
			fnPoll,
            me = Object.create(ChatProxy.prototype, {
                start : {
                   value:  function(id, win, fail){
                       situation_id = id;
                       lastPolled = null;
                       isPolling = false;
					   isStopped = false;
					   fnPoll = this.poll.bind(this)                      				                       
					   fnWin = win;
					   fnFail = fail;
					   
					   this.poll();
                   }
                },

                stop: {
                    value: function(){
						console.log('stopping chat polling...');
						isStopped = true;
                        clearTimeout( pulse );
                    }
                },

                send: {
                    value: function(msg){
						clearTimeout( pulse );
						
                        var new_note = new App.Note({content:msg, situation_id:situation_id})
                        var attr = new_note.toJSON();
                        new_note.save({ note:attr }, {
							success: function(){												
								console.log('poll after send');																
								fnPoll();								
							}
						});
                    }
                },

                poll: {
                    value: function(){                       
                        if ( isPolling || isStopped ) 
							return;
						
						console.log('polling chat...');						
                        isPolling = true;
                        var delta = lastPolled;                
						var notes = new App.SituationNotes();
                        notes.poll( {id:situation_id, delta:delta, success:this.success.bind(this), error:this.error.bind(this), complete:this.complete.bind(this) } );
                    }
                },

                success: {
                    value: function(model,data){
                        console.log('chat successfully polled');
						if (data.count>0) {
							var last = data.items[data.items.length-1];
							lastPolled = last.created_at;
						}
						
						if( !isStopped )
							fnWin(model, data);
                    }
                },

                error: {
                    value: function(data, xhr){
                        console.log('error in polling chat');
						
						if( !isStopped )
							fnFail(data, xhr);
                    }					
                },
				
				complete: {
					value: function(){
                        isPolling = false;
						
						if( !isStopped )
							pulse = setTimeout( fnPoll, 5000 );
					}
				}
            });

        me.constructor = ChatProxy;

        return me;
    }

    var NotificationsProxy = function(){
        var isPolling,
			lastPolled,
			pulse,
			notifications,
            me = Object.create(NotificationsProxy.prototype, {
				start: {
					value: function(){						
						lastPolled = null;
						notifications = new App.LatestNotifications();
						pulse = setInterval( this.poll.bind(this), 15000 );
					}
				},

                stop: {
                    value: function(){
						clearInterval( pulse );
                    }
                },

                poll: {
                    value: function(){
						if ( !App.currentUser || isPolling ) 
							return;
						
						console.log('polling notifications...');
						 isPolling = true;
                        
                        notifications.poll( { delta:lastPolled, success:this.success.bind(this), error:this.error.bind(this) } );						
						isFirst = false;
                    }
                },

                success: {
                    value: function(){
                        console.log('notifications successfully polled');
						
						var result = notifications.models[0],
							count = result.get('count'),
							items = result.get('items'),
							unseen = result.get('unseen');		
						
						App.dispatcher.trigger('app:notifications', { items:items, count:count });
						
                        isPolling = false;
                    }
                },

                error: {
                    value: function(){
                        console.log('error in polling notifications');
                        isPolling = false;
                    }
                }
            });

        me.constructor = NotificationsProxy;

        return me;
    }
	
	var Geolocation = function(){		
		var	geo,
			fnSuccess = function(position){
				console.log(
					'Latitude: '          + position.coords.latitude          + '\n' +
					'Longitude: '         + position.coords.longitude         + '\n' +
					'Altitude: '          + position.coords.altitude          + '\n' +
					'Accuracy: '          + position.coords.accuracy          + '\n' +
					'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
					'Heading: '           + position.coords.heading           + '\n' +
					'Speed: '             + position.coords.speed             + '\n' +
					'Timestamp: '         + position.timestamp                + '\n');
			},
			fnError = function(){},
			options = {},
			me = Object.create(Geolocation.prototype, {
				initialize: {
					value: function(){
						//if( App.isDevice ) 
						//	geo = cordova.require('cordova/plugin/geolocation');
						//else
							geo = navigator.geolocation;
					}
				},
				
				getCurrentPosition: { 
					value : function(win, fail, opt) {
						win = win || fnSuccess;
						fail = fail || fnError;
						opt = opt || options;
						geo.getCurrentPosition(win, fail, opt);
					}										
				},				
			});
				
		me.constructor = Geolocation;
		
		me.initialize.apply(me, arguments);
		
		return me;	
	}
	
	
	var GoogleMap = function(){
		var map, el, lat, lng, latLng, 		
			me = Object.create(GoogleMap.prototype, {
				initialize: 
				{
					value: function(options){
						options = options || {};
						el = options.el || document.body;
						lat = options.lat || 0;
						lng = options.lng || 0;		
						zoom = options.zoom || 1;
						latLng = new google.maps.LatLng(lat, lng);				
						map = new google.maps.Map(el, {
							center: latLng,
							zoom: zoom,
							disableDefaultUI : true,
							scrollwheel: true,
							disableDoubleClickZoom: true,
							mapTypeId: google.maps.MapTypeId.ROADMAP
						});						
					}
				}
			});
		
		me.constructor = GoogleMap;	
			
		me.initialize.apply(me, arguments);
		
		return me;
	}

	var GoogleMapInfowindow = function() {
		var map, el, lat, lng, latLng;
		var me = Object.create(GoogleMap.prototype, {
			initialize: {
				value: function(options) {
					var options = options || {};
					var el = options.el || document.body;
					var lat = options.lat || 0;
					var lng = options.lng || 0;		
					var zoom = options.zoom || 1;
					var center = new google.maps.LatLng(lat, lng);				
					var map = new google.maps.Map(el, {
						center: center,
						zoom: 6, // zoom
						disableDefaultUI : true,
						scrollwheel: true,
						disableDoubleClickZoom: true,
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});
					var infowindow = new google.maps.InfoWindow();
					var markers = [];
					var globalMarker = [];

					var i = 0;
					options.markers.each(function(e) {
						if (!(e.get('location_lat') && e.get('location_lng'))) {
							return false;
						}

						marker = new google.maps.Marker({
							position: new google.maps.LatLng(
								e.get('location_lat'),
								e.get('location_lng')
							),
							draggable: true,
							raiseOnDrag: true,
							map: map,
							labelContent: JST['map-marker']({
								avatar_map: e.get('avatar'),
								user_name: e.get('user_name'),
								created_at: e.get('created_at'),
								category: e.get('category_id'),
								// ???
								// situationcondition_id: e.get('situationcondition_id'),
								situationcondition_id: e.get('id'),
								content: _.escape(e.get('content')),
							}),
							labelAnchor: new google.maps.Point(30, 0),
						});

						markers.push(marker);

						// weird, touchstart event doesn't work
						google.maps.event.addListener(markers[i], 'click', function(e) {
							var content = '';

							content = '<div id="map_wrapper"><div id="map_scroller"><ul id="map_list">';
							content += this.labelContent;
							content += '</div></div></ul>';

							infowindow.setContent(content);
							infowindow.open(map, this);
						});


						i++;
					});

					var mc = new MarkerClusterer(map, markers, { zoomOnClick: false });

					globalMarker = markers.slice();
					google.maps.event.addListener(mc, 'clusterclick', function (cluster) {
						console.log('clusterclick');

						var markers = cluster.getMarkers();

						//Get all the titles
						var titles= '<div id="map_wrapper"><div id="map_scroller"><ul id="map_list">';

						for (var i = 0; i < markers.length; i++) {
						    titles += markers[i].labelContent + "\n";
						}

						titles += '</div></div></ul>';

						infowindow.setContent(titles);
						infowindow.setPosition(cluster.getCenter());
						infowindow.open(map);

						// console.log($('#map_wrapper').html());
						myScroll = new iScroll('map_wrapper', {
							snap: true,
							momentum: false,
							hScrollbar: true,
							onScrollEnd: function () {
								// document.querySelector('#indicator > li.active').className = '';
								// document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
							}
						});
						$('#map_wrapper').find('.time').timeago();

						google.maps.event.addListener(map, 'zoom_changed', function () {
						    infowindow.close()
						});

					});
				}
			}
		});
		
		me.constructor = GoogleMapInfowindow;
			
		me.initialize.apply(me, arguments);
		
		return me;
	}

	App.Db = Db;
	App.PopupStack = PopupStack;
	App.ChatProxy = ChatProxy;
	App.NotificationsProxy = NotificationsProxy;
	App.Geolocation = Geolocation;
	App.GoogleMap = GoogleMap;
	App.GoogleMapInfowindow = GoogleMapInfowindow;
		
	App.__cache = {};
	//App.__cache['notifications'] = { unseen:0, items:[] };
	App.currentUser = null;

})( App );
