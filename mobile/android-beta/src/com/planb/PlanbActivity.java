package com.planb;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import org.apache.cordova.*;

public class PlanbActivity extends DroidGap {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        String action = intent.getAction();
        String url = "file:///android_asset/www/index.html";

        if (Intent.ACTION_VIEW.equals(action)) {
            final List<String> segments = intent.getData().getPathSegments();
            if (segments.size() > 1 && segments.get(0).toLowerCase().equals("situations")) {
                url += "#showsituation/" + segments.get(1);
                        
	            Context context = getApplicationContext();
	            int duration = Toast.LENGTH_LONG;	
	            Toast toast = Toast.makeText(context, "Loading situation...", duration);
	            
	            toast.show();
            }
        }
                        
        super.loadUrl(url);        
        //super.appView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        
    }    
}
