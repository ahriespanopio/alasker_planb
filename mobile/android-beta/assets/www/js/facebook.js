// ------------------------------
// Facebook
// ------------------------------
function Facebook(success, error) {	
	var cb = App.getBrowser(),
		key = 'facebook',
		options = App.config.facebook,
		oauth = null,
		onSuccess = success || function(){},
		onError = error || function(){}, 
		fb = Object.create(Facebook.prototype, {			
			authenticate: {				
				value: function(){
					var self = this,
						authorize_url = 'https://graph.facebook.com/oauth/authorize?';
						authorize_url += '&response_type=code';	
						authorize_url += '&client_id=' + options.apiId;
						authorize_url += '&redirect_uri=' + options.callbackUrl;
						authorize_url += '&scope=email,publish_stream,offline_access';	
							  			 
					cb.onLocationChange = function( loc ){
						self.callback( loc );
					};
					cb.onClose = function(){
						onError({});
					};
					
					cb.showWebPage(authorize_url);
				}
			},
			
			callback : {
				value: function( loc ) {
					if (loc.indexOf(options.callbackUrl) > -1) {	
					
						var code = loc.match(/code=(.*)$/)[1];
								
						if( code ) {			
							var graph_url = 'https://graph.facebook.com/oauth/access_token?';					
								graph_url += '&client_id=' + options.apiId;
								graph_url += '&client_secret=' + options.apiSecret;
								graph_url += '&redirect_uri=' + options.callbackUrl;
								graph_url += '&code='+code;
								
							$.ajax({
								url: graph_url,
								data: {},
								dataType: 'text',
								type: 'POST',
								success: function(data, status){
									console.log('success facebook');
									// We store our token localStorage 
									localStorage.setItem('facebook', data.split('&')[0].split('=')[1]);
									onSuccess( data );
									cb.close();		 
								},
								error: function(error) {
									cb.close();
									onError();
								}
							});
							
						} else {				
							cb.close();
							onError();				
						}						
					}
				}
			},

			getUserData : {
				value: function( success, error ){
					var	onSuccess = success || function(){},
						onError = error || function(){};
					var graph_url = 'https://graph.facebook.com/me/?access_token='+localStorage.getItem('facebook');
					$.ajax({
						url: graph_url,			
						data: {},
						dataType: 'text',
						type: 'GET',
						success: function(data, status){
							var json = JSON.parse(data),
								payload = {
									provider:'facebook',
									uid: json.id,
									info: json,
									credentials: {
										token: localStorage.getItem('facebook')										
									}
								};
							
							//dump( payload );
							onSuccess( payload );
						},
						error: function(error) {
							onError( error );
						}
					});
				}
			}		
		});
		
		fb.constructor = Facebook;
		
		return fb;
};


