App.Pulldown = Backbone.View.extend({
	el: "#pullDown",
	
	render: function(){
		this.$el.append('<span class="pullDownIcon"></span><span class="pullDownLabel">Pull down to refresh...</span>');
		
		return this;
	}
});

App.Pullup = Backbone.View.extend({
	el: "#pullUp",
	
	render: function(){
		this.$el.append('<span class="pullUpIcon"></span><span class="pullUpLabel">Pull up to refresh...</span>');
		
		return this;
	}
});

App.PostItem = Backbone.View.extend({	
	mapper: function(x){
		var obj = 
			{
				id: x.get('id'),
				username: x.get('user_name'),			
				whendate: x.get('created_at'),
				content: App.linkify(  App.encodeForHTML( x.get('content') ) ),
				avatar: App.url( x.get('avatar') ),
				lat: x.get('location_lat'),
				lng: x.get('location_lng'),
				radius: x.get('location_radius'),
			},
			cache = App.getCache('q');
		
		cache[obj.id] = obj;
		
		return obj;
	},
	
	render: function(){
		var template = _.template([
				'<% items.each(function(item) { %>',
				'	<%= part( mapper(item) ) %>',
				'<% }); %>'
			].join(''));
			
		var part = _.template([			
				'<li><div class="post new" data-id="<%= id %>">',
				'<div class="user_img"><img src="<%= avatar %>"></div>',
				'<div class="user_message">',
				'<div>',
				'<span class="user_name"><%= username %></span>',
				'<span class="time" title="<%= whendate%>"><%= whendate%></span>',
				'</div>',
				'<p><%= content %></p>',
				'</div>',
				'<br class="clear">',
				'</div></li>'
			].join(''));
			
		this.$el.append(template({ items:this.collection, part:part, mapper:this.mapper }));

		return this;
	}
});

App.NotificationItem = Backbone.View.extend({	
	mapper: function(x){
		var fnFormat = this.format.bind(this),
			obj = 
			{
				id: x.get('id'),
				username: x.get('sender_name'),			
				whendate: x.get('created_at'),
				content: fnFormat(x),
				avatar: App.url( x.get('avatar') ),
				modifier: x.get('unseen') ? 'new':''
			},	
			cache = App.getCache('n');
				
		cache[obj.id] = obj;
		
		return obj;
	},
	
	format: function(x){
		var args = {
				sender: x.get('sender_name'),				
				content: App.linkify( App.encodeForHTML( x.get('content') ) )
			},
			template;
			
		switch(x.get("event"))
		{
			case 3:
				args['action'] = 'wrote an answer';
				template = _.template('<span class="event"><%= action%></span>&nbsp;<span class="context"><%= content%></span>');
				break;
			case 8:
				args['action'] = 'is now following you';
				template = _.template('<span class="event"><%= action%></span>');
				break;
			case 13:
				args['action'] = 'made a comment';
				template = _.template('<span class="event"><%= action%></span>&nbsp;<span class="context"><%= content%></span>');				
				break;
			case 16:
				args['action'] = 'sent you a message';
				template = _.template('<span class="event"><%= action%></span>');
				break;
		}
		
		return  template(args);
	},
	
	render: function(){
		var template = _.template([
				'<% items.each(function(item) { %>',
				'	<%= part( mapper(item) ) %>',
				'<% }); %>'
			].join(''));
			
		var part = _.template([			
				'<li><div class="post <%= modifier%>" data-id="<%= id %>">',
				'<div class="user_img"><img src="<%= avatar %>"></div>',
				'<div class="user_message">',
				'<div>',
				'<span class="user_name"><%= username%></span>',
				'<span class="time" title="<%= whendate%>"><%= whendate%></span>',
				'</div>',
				'<p><%= content %></p>',
				'</div>',
				'<br class="clear">',
				'</div></li>'
			].join(''));
			
		this.$el.append(template({ items:this.collection, part:part, mapper:this.mapper.bind(this) }));
		
		//this.$el.find('.time').timeago();
		
		return this;
	}
});

App.ChatItem = Backbone.View.extend({	
	mapper: function(x){
		this.odd = !this.odd;
	
		var obj = 
			{
				id: x.get('id'),
				username: x.get('sender_name'),			
				whendate: x.get('created_at'),
				content:  App.linkify( App.encodeForHTML( x.get('content') ) ),
				avatar: App.url( x.get("avatar") ),
				className : this.odd ? 'chat1' : 'chat2'
			};
			
		return obj;
	},
	
	setRowType: function(even){
		this.odd = !even;
	},
	
	render: function(){
		
		var template = _.template([
				'<% items.each(function(item) { %>',
				'	<%= part( mapper(item) ) %>',
				'<% }); %>'
			].join(''));
			
		var part = _.template([		
				'<li class="<%= className%>">',
				'<div class="user_img"><img src="<%= avatar %>" ></div>',
				'<div class="user_chat">',
				'<div class="chat_bg_top">',
				'<div class="chat_bg_bottom">',
				'<div class="user_chat_message"><p><%= content%></p></div>',
				'</div></div></div>',
				'<br class="clear">	</li>'		
			].join(''));
		
		this.$el.append( template({ items:this.collection, part:part, mapper:this.mapper.bind(this) }) );
	}
});