var App = {
	getCache: function(key){
		if(! App.__cache[key]){
			App.__cache[key] = {}
		}		
		return App.__cache[key];
	},
	
	linkify: function(content){
		if (content) 
			return linkify(content);
			
		return content;
	},
	
	encodeForHTML: function(content){
		return $ESAPI.encoder().encodeForHTML(content);
	},
	
	url: function(path, params) {		
		var kv = '', url;
		
		// add coordinate values in query parameters
		if(App.currentPosition && params){
			var coord = {
					lat : App.currentPosition.latitude,
					lng : App.currentPosition.longitude
				}
				
			_.extend(params, coord);
		}
		
		if (params) {
			$.each(params, function(key, value){
				kv += '&' + key + '=' + encodeURI(value);
			});
			kv += '&_=' + (new Date()).getTime();
		}
		
		if ( !path.startsWith('http' ) ) {
				
			if (  path.startsWith('/') )
				path = path.substr(1);
		
			url = 'http://'+App.config.host+'/'+path+'?access_key=p1an6'+kv;		
		
			return url;
		};
		
		return path;
	},
	
	get: function(path, params, callback){		
		$.ajax({
            url:  App.url(path, params),
            dataType: 'text',
            async:true,
            type:'GET',
            data: null,
            success: function(data, textStatus, jqXHR){
				callback(data);
			}
		});
	},
	
	getUser: function(){
		return App.__db.getUser();
	},
	
	setUser: function( newUser ){
		App.__db.setUser( newUser );
	},
	
	clearUser: function(){
		App.__db.clearUser();
		App.currentPosition = null;
	},

	getLocation: function(success, error){			
		var timeoutVal = 10 * 1000 * 1000;
		success = success || function(){};
		error = error || function(){};
		
		App.__geo.getCurrentPosition(success, error, {enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0});		
	},
	
	pushState: function(state){
		App.__stack.push( state );
	},
	
	popState: function(){
		return App.__stack.pop();
	},
	
	peekState: function(){
		return App.__stack.peek();
	},
	
	run: function() {
		console.log('App::run');
		var cl = App.loader = new CanvasLoader('loader');
		cl.setColor('#14b0e0');
		cl.setDiameter(50);
		cl.setRange(1);

		App.__db = new App.Db();
		App.__stack = new App.PopupStack();
		App.__notifications = new App.NotificationsProxy();
    
		console.log('user agent:'+navigator.userAgent);
		if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {			
			document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
			// this is a hack to make swipe work
			document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
		} else {
			App._initialize(); 			
		}		
	},
	
	toast: function(message) {
		var toastDiv = $(document.createElement("div"));
        toastDiv.addClass("toast");

        var msgDiv = $(document.createElement("div"));
        msgDiv.addClass("message");
        msgDiv.text(message);
        msgDiv.appendTo(toastDiv);

        toastDiv.appendTo(document.body);
		
		setTimeout(function(){
				$(toastDiv).animate({ opacity : 0 }, 600, 'linear', function(){ console.log('done');$(toastDiv).remove(); });				
			}, 2000);				
	},
	
	checkInternet: function(){
		if ( App.isDevice ) {
			if ( navigator.connection.type != Connection.NONE ){
				App.onInternetOnline();
			} else {
				App.onInternetOffline();
			}			
		} else {
			$.ajax({
				url: "http://google.com",
				type:'GET',
				data: null,
				success: function(data, status, xhr){
					App.onInternetOnline();
				},
				error: function(xhr, errorType, error){
					App.onInternetOffline();
				}
			});
		}
	},
	
	loading: function(type){
		if(type === 'show'){
			$('#loader_container').addClass('open');
			App.loader.show();
		
		}else{		
			App.loader.hide();
			$('#loader_container').removeClass('open');	
		}
	},
	
	disposePopup: function(){
		// dispose of current popup
		var state = App.popState();
		if ( state ) {
			console.log('disposing popup...');
			state.dispose();
		}
		
		return state;
	},
	
	back: function(){
		// restore previous page
		//window.history.back();
		console.log('fire event app:goback');
		App.dispatcher.trigger('app:goback');
	},
	
	getBrowser: function(){
		if(App.isDevice){
			return window.plugins.childBrowser;
		}
		
		return new PopupBrowser();
	},
	
	size: function(){
		var h = 0, 
			w = 0;
		switch(window.orientation)
		{ 
			case -90:
				// landscape left
				h = Math.min(window.innerHeight, window.innerWidth);
				w = Math.max(window.innerHeight, window.innerWidth);
				break;
			case 90:
				// landscape right
				h = Math.min(window.innerHeight, window.innerWidth);
				w = Math.max(window.innerHeight, window.innerWidth);
				break;
			case 180:
				// portrait down
				h = Math.max(window.innerHeight, window.innerWidth);
				w = Math.min(window.innerHeight, window.innerWidth);
				break;
			default:
				// portrait
				h = Math.max(window.innerHeight, window.innerWidth);
				w = Math.min(window.innerHeight, window.innerWidth);
				break;
		}  
			
		return { height:h, width:w };
	},
	
	_resize: function() {
		console.log('resizing viewport');

		if( window.__page) {
			App.dispatcher.trigger('app:resize');
		}
	},
		
	_initialize : function() {
		org.owasp.esapi.ESAPI.initialize();
				
		App.logger = Log4js.getLogger('alasker');
		App.logger.addAppender(new Log4js.BrowserConsoleAppender());
		App.logger.setLevel(Log4js.Level.ALL);		
	
		App.pages = {};
		App.popup = {
			isSettings: false,
			isNotifications: false,
			isQuestion: false
		}
	
		App.__geo = new App.Geolocation();
	
		App.__subscribers = {};
		
		// create custom events dispatcher
		var dispatcher =  _.clone(Backbone.Events);
		
		dispatcher.on('app:login', this.onLogin.bind(this));
		
		dispatcher.on('app:logout', this.onLogout.bind(this));
	
		dispatcher.on('app:chat', function(){
			console.log('Event fired >> app:chat');			
		});
		
		dispatcher.on('app:offline', function() { 
				console.log('internet offline');
				App.toast( "Connection error!" );
				
				App.isOnline = false;
								
				if ( App._checkInterval ){
					clearInterval(App._checkInterval);
				}
				App._checkInterval = setInterval( App.checkInternet, 15000 );				
			});
			
		dispatcher.on('app:online', function() { 
				console.log('internet online');
				
				App.isOnline = true;
				
				if ( App._checkInterval ){
					clearInterval( App._checkInterval );
					App._checkInterval = null;
				}
			});
			
		if (App.isDevice){
			console.log('initializing on device...');
			document.addEventListener("backbutton", function(){	App.back();	}, true);
			document.addEventListener("online", this.onInternetOnline.bind(this), false);
			document.addEventListener("offline", this.onInternetOffline.bind(this), false);
			document.addEventListener("orientationChanged", this.onOrientationChanged.bind(this), false);

			// hack to give focus to next element in a webview when tab key is pressed
			/*$('body').on('keydown', 'input, select, textarea', function(e) {
				var focusable,
					next;
					
				//console.log('keydown: ' + e.keyCode);
					
				if (e.keyCode == 9) {				
					focusable = $(document).find('input,a,select,button,textarea');
					next = focusable.eq(focusable.index(this)+1);
					if (next.length) {
						//console.log('focusing on next input element...');
						next.focus();
					} 
					return false;
				}
			});
			*/
		}else{
			console.log('initializing on browser...');
			$('.back').on('click', App.back);
			dispatcher.on('resize', App._resize );
		}
		
		App.dispatcher = dispatcher;
		
		App.router = new App.Router();
		
		App.getUser();
		
		Backbone.history.start();	
	},
	
	onLogin:function(){
		//App.__notifications.start();
		App.getLocation( this.onPositionChange.bind(this) );
	},
	
	onLogout:function(){
		App.clearUser();
		window.location.hash = '';
	},
	
	onPositionChange: function(geoPosition){
		App.currentPosition = geoPosition.coords;
	},
	
	onInternetOnline: function(event) {
		App.dispatcher.trigger('app:online');
	},
	
	onInternetOffline: function(event) {
		App.dispatcher.trigger('app:offline');
	},

	onOrientationChanged: function() {
		console.log('change orientation');
		App.dispatcher.trigger('app:changeOrientation');
	},

	onDeviceReady:function(){
		App.isDevice = true;	
		
		App._initialize();
	}
	
	/*
	subscribe: function(eventKey, callback){
		if( ! App.__subscribers[eventKey] ){
			App.__subscribers[eventKey] = [];
		}
	
		App.__subscribers[eventKey].push(callback);
	},
	
	notify: function(eventKey, payload){
		$.each(App.__subscribers[eventKey], function(k, callback) {		
			callback( payload );
		});
	},*/
}
window.App = App;

function dump( obj ) {
	$.each(obj, function(k, v) {		
		console.log(k + ' is ' + v);
	});
}
