App.TestView = Backbone.View.extend({
	initialize: function(){
		this.template = JST['test'];				
		$(this.el).html(this.template());	
	},
	
	onChangeOrientation: function(){
		console.log('TestView::onChangeOrientation');
	},
	
	show: function(){
		console.log('TestView::show');			
		this.listenTo(App.dispatcher, 'app:changeOrientation', this.onChangeOrientation.bind(this)); 
	},
	
	hide: function(){
		this.stopListening();
	},
		
	render: function(){		
		return this;
	},	
});

App.PostQuestionPopup = Backbone.View.extend({
	el : '#question_pad',
	
	initialize: function(options){
		this.options = options;
		this.isPopup = true;		
		this.isPosting = false;

		// parse template
		this.$el.html( JST['postquestion']( ) );		
		
		// wire listeners
		this.$el.find('#cancel_button').tap( this.hide.bind(this) );		
		this.$el.find('#respond_button').tap( this.onRespond.bind(this) );
	},	
	
	hide: function(){
		// hide background layer
		console.log('hide post-a-q');
		var pb =  $('#popup_background');

		pb.removeClass();
		pb.off()
				
		this.$el.removeClass();
		this.stopListening();
		
		App.popup.isQuestion = false;
		
		window.location.hash = this.options.callback;
	},
	
	show: function(){	
		console.log('show post-a-q');
		var pb = $('#popup_background'),
			pc = this.$el;
			
		// show popup contents
		pc.addClass('popup');		

		// show the background layer. 
		setTimeout( function(){pb.addClass('popup');}, 0);
						
		// fire backbutton when background layer is swiped
		pb.swipe( this.hide.bind(this) );	
		
		//pc.find('#content').focus();
		
		this.listenTo(App.dispatcher, 'app:goback', this.hide.bind(this));
		this.listenTo(App.dispatcher, 'popup:close', this.hide.bind(this));
		
		App.popup.isQuestion = true;
	},
	
	render: function(){
		return this;
	},
	
	onRespond: function(e){
		e.preventDefault();
	
		if( this.isPosting )
			return;
			
		this.isPosting = true;
	
		var self = this,
			fnhide = this.hide.bind(this),
			content = this.$el.find('#content').first().val(),
			post = new App.Situation(),
			payload = { content:content };
			
		App.loading('show');
		
		post.save( { situation:payload }, {
			success: function(model, response){
				App.toast('Your question has been succesfully sent!');
				
				fnhide();
			},
			error: function(model, xhr){
				App.toast(xhr.responseText);
			},			
			complete: function(){
				App.loading('hide');
				self.isPosting = false;				
			},
		});
	}
});

App.SettingsPopup = Backbone.View.extend({
	//el : '#popup_container',
	el : '#popup_brd',
	
	initialize: function(options){
		this.busy = false;
		
		this.isPopup = true;
				
		var settings = App.__db.getSettings();
		if ( !settings ){
			settings = {
				email: '',
				username: '',
				description: '',
			};			
		}
		this.$el.html( JST['settings']( settings ) );	

		this.$el.find('.iphone_checkbox').switchOnOff();
		this.$el.find('.expando').collapsible();
	},
	
	hide: function(){
		console.log('hide settings');
		// hide background layer
		var pb =  $('#popup_background');
			
		this.busy = false;
			
		pb.removeClass();
		pb.off();
		
		this.$el.removeClass();	
		this.stopListening();
		
		window.location.hash = this.options.callback;
		
		App.popup.isSettings = false;
	},
	
	show: function(){
		console.log('show settings');
		var pb = $('#popup_background');
			
		// show popup contents
		this.$el.addClass('popup');		

		// show the background layer. 
		setTimeout( function(){pb.addClass('popup');}, 0);;
						
		// fire backbutton when background layer is swiped
		pb.swipe( this.hide.bind(this) );		
		
		this.listenTo(App.dispatcher, 'app:goback', this.hide.bind(this));		
		this.listenTo(App.dispatcher, 'popup:close', this.hide.bind(this));
		
		App.popup.isSettings = true;
	},	
	
	render: function(){			
		return this;
	}
});

App.QuickReplyPopup = Backbone.View.extend({
	//el : '#popup_container',
	el : '#plan_pad',
	
	events: {
		'change #plan_content' : 'setContent',
	},
	
	setContent: function(){		
		// this doesnt fire in mobile device???
		console.log('set content:'+this.$content.val());
		this.newPlan.set( { content : this.$content.val() } );
	},
	
	initialize: function( question ){
		this.isPosting = false;
		
		this.isPopup = true;
		
		this.question = question;				
		
		// parse template
		this.$el.html( JST['question']( question ) );		
		
		this.$content = this.$el.find('#plan_content').first();
		this.newPlan = new App.Plan({new_situation_id:question.id});
		this.planCondition = new App.PlanCondition();
		
		var plan = this.newPlan;
	
		// setup dom listeners
		this.$el.find('#respond_button').tap( this.onRespond.bind(this) );		
		this.$el.find('#view_button').tap( this.onView.bind(this) );
		
		
		this.$el.find('.time').timeago();		
	},	
	
	render: function(){										
		return this;
	},
	
	onRespond: function(e){
		e.preventDefault();
		
		var self = this,
			content = this.$content.val();
	
		if( !this.isPosting && content){
			this.isPosting = true;
			
			// make sure content is pulled
			this.newPlan.set( { content : this.$content.val() } );
				
			var	payload = { 'plan':this.newPlan.toJSON() };
			payload['plancondition'] = this.planCondition.toJSON();
			
			App.loading('show');
			
			this.newPlan.save(payload, {
				xhrFields: {
					withCredentials: true
				},
				error: function(data,response){
					//console.log(response.responseText);
					//var obj  = jQuery.parseJSON(response.responseText);
					App.toast('Connection error!');					
				},
				success: function(){
					App.toast('Your reply has been sent!');
					window.location.hash = 'questions';
				},
				complete: function(){
					this.isPosting = false;
					App.loading('hide');
				}
			});			
		}
	},
	
	onView: function(e){
		this.hide();
		window.location.hash = 'question/' + this.question.id + '/chat';
	},
	
	show: function(){
		console.log('show quick reply');
		var pb = $('#popup_background');
			
		// show popup contents
		this.$el.addClass('popup');

		// show the background layer. 
		setTimeout( function(){pb.addClass('popup');}, 0);		
						
		// fire backbutton when background layer is swiped
		pb.swipe( this.hide.bind(this) );	

		this.$el.find('.time').timeago();
		
		//this.$content.focus();		
		this.listenTo(App.dispatcher, 'app:goback', this.hide.bind(this));		
		this.listenTo(App.dispatcher, 'popup:close', this.hide.bind(this));
	},
		
	hide: function(){
		console.log('hide quick reply');
		// hide background layer
		var pb =  $('#popup_background');
		pb.removeClass();
		pb.off();
		
		this.busy = false;
		
		// clean up popup container
		this.$el.removeClass();
		//this.$el.children().remove();		

		this.stopListening();
		
		window.location.hash = 'questions';
	},	
});

App.RegisterView = Backbone.View.extend({
	initialize: function(){
		//$(window).on('page:show', this.onShow.bind(this));
		
		$(this.el).html( JST['register']() );	
	},
	
	show: function(){
		//$('#wrapper').addClass('front');
	},
		
	render: function(){
		return this;
	},	
});

App.SignUpView = Backbone.View.extend({
	events : {		
		'change #txt_email' : 'setEmail',
		'change #txt_username' : 'setUsername',
		'change #txt_password' : 'setPassword',
		'change #txt_confirm_password' : 'setConfirm',
	},
	show: function(){
		//$('#wrapper').addClass('front');
		//this.$username.focus();
		var errors = this.payload.errors || this.errors;
		
		this.errors = null;
		
		if ( errors )
			this.showError( errors );
	},
	initialize: function() {		
		var fnSubmit = this.submit.bind(this),
			fnError = this.showError.bind(this),
			self = this;
	
		this.$el.html( JST['signup']() );
		
		this.payload = {username:'',email:'',password:'', password_confirmation:''};
				
		this.$username = this.$el.find('#txt_username');
		this.$password = this.$el.find('#txt_password');
		this.$email = this.$el.find('#txt_email');
		this.$confirm = this.$el.find('#txt_confirm_password');
		
		this.$el.find('#btn_submit').tap(function(e){		
			e.stopPropagation();
			if (self.isPosting)
				return;
			
			console.log('submitting...');
			if ( $.trim(self.payload.username) === '' )
				fnError({ username:['can not be blank'] });
			else if ( $.trim(self.payload.email) === '' )
				fnError({ email:['can not be blank'] });			
			else if ( $.trim(self.payload.password) === '' )
				fnError({ password:['can not be blank'] });
			else if ( self.payload.password === self.payload.password_confirmation)
				fnSubmit();
			else
				fnError({ password:['needs confirmation'] });
		});	
				
		if (arguments.length > 0) {
			this.auth =  arguments[0].payload.auth;
			this.errors = arguments[0].payload.errors;
			this.payload.username = this.auth.info['username'];
			this.payload.email = this.auth.info['email'];
			this.payload.password = '';
			this.payload.password_confirmation = '';
			this.$username.val(this.payload.username);
			this.$email.val(this.payload.email);
		}
    },
	submit: function(){
		if ( this.isPosting )
			return;
			
		if ( this.auth ) 
			this.oauth()
		else
			this.signup();
	},	
	oauth: function(){
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
	
		this.isPosting = true;
	
		this.auth.info.username = this.payload.username;
		this.auth.info.email = this.payload.email;
		this.auth.info.password = this.payload.password;
		
		model.url = App.url('auth/'+this.auth.provider+'/callback.json');
		
		App.loading('show');
		
		model.save( {auth:this.auth}, {
			success: fnSuccess, 
			error: fnError,
			complete: fnComplete
		} );
	},
	signup: function(){
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
	
		this.isPosting = true;
			
		model.url = App.url('users.json');
		
		App.loading('show');
			
		model.save( { user: this.payload }, {
			success: fnSuccess, 
			error: fnError,
			complete: fnComplete
		});
	},	
	onComplete: function(){
		this.isPosting = false;
		App.loading('hide');
	},	
	onSuccess: function(model, response) {
		console.log('*** signup successful');				
		App.setUser( response );				
		App.router.navigate('landing/list', {trigger: true, replace: true});
		setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
	},	
	onError:function(model, xhr) {
		console.log('*** signup failed');		
		if (xhr.response === ""){
			App.toast('Connection error!');
		} else {
			var payload = JSON.parse(xhr.response);
			this.showError(payload);				
		} 
	},
	setUsername: function(){
		this.payload['username'] = this.$username.val();
	},
	setPassword: function(){
		this.payload['password'] = this.$password.val();
	},
	setConfirm: function(){
		this.payload['password_confirmation'] = this.$confirm.val();
	},
	setEmail: function(){
		this.payload['email'] = this.$email.val();
	},
	showError: function(errors){
		var error = this.$el.find('#error').first();
		
		error.html('');				
		$.each(errors, function(k, v) {		
			console.log(k + ' ' + v[0]);
			error.append('<li>'+ k + ' ' + v[0] + '</li>');
		});		
		error.show();
	},
	clearForm: function(){
		this.payload = {};
		this.$username.val('');
		this.$email.val('');		
		this.$password.val('');		
		this.$confirm.val('');
	},   
    render:function (eventName) {
        return this;
    }
});

App.LoginView = Backbone.View.extend({
   initialize: function() {				
		$(this.el).html( JST['signin'] );

		this.$username = $(this.el).find('#txt_login').first();
		this.$password = $(this.el).find('#txt_password').first();	
		this.clearForm();
    },
	show: function(){
		//$('#wrapper').addClass('front');
		//this.$username.focus();
	},
	events: {
		'submit form': 'login',
		'change #txt_login' : 'setUsername',
		'change #txt_password' : 'setPassword',
	},
	setUsername: function(){
		this.payload['login'] =  this.$username.val();
	},
	setPassword: function(){
		this.payload['password'] = this.$password.val() ;
	},
	showError: function(payload){
		var error = this.$el.find('#error').first();
		
		error.html('');		
		error.append('<li>'+ payload.message + '</li>');				
		error.show();	
	},
	clearForm: function(){
		this.payload = { login:'', password:'' };
		this.$username.val('');
		this.$password.val('');		
	},
	login: function(e) {
		e.preventDefault();

		if ( this.isPosting )
			return;
			
		this.isPosting = true;
		
		App.loading('show');
		
		var model = new App.Json(),
			fnSuccess = this.onSuccess.bind(this),
			fnError = this.onError.bind(this),
			fnComplete = this.onComplete.bind(this);
		
		model.url = App.url('mobile/login');		
		model.save( { user_login: this.payload }, {
			success: fnSuccess,
			error: fnError,
			complete: fnComplete
		});
	},
	onSuccess: function(model, response) {
		console.log('login succeeded');
		App.setUser( response );
		App.router.navigate('landing/list', {trigger: true, replace: true});
		
		setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
	},
	onError:function(model, xhr) {
		console.log('login failed');
		try 
		{	
			var payload = JSON.parse(xhr.response);
			this.showError(payload);				
		} 
		catch(e) 
		{
			App.toast('Connection error!');
		} 
		finally 
		{
			setTimeout( this.clearForm.bind(this), 100 );					
		}		
	},
	onComplete:function(){
		App.loading('hide');
		this.isPosting = false;
	},
    render:function (eventName) {
        return this;
    }
});

App.QuestionsView = Backbone.View.extend({
	coll : '', // hack, but this works for now
	hideSlider: function(){
		this.$footer.attr('class', 'open');
		this.$slider.attr('class', 'close');		
	},
	initialize: function(){
		this.$el.html(JST['questions-list']);
		
		this.$el.addClass('container');
	
		var self = this,		
			fnHideSlider = this.hideSlider.bind(this),
			footer = this.$footer = this.$el.find('#footer'),
			slider = this.$slider = this.$el.find('#slider'),
			fnClosePopups = function(){ setTimeout(function(){App.dispatcher.trigger('popup:close');}); };
			
		
		this.$notification_count = this.$el.find('#notification_count');
		
		footer.find('.slide_top').swipe(function(e){
			e.preventDefault();
			slider.attr('class', 'open');	
			footer.attr('class', 'hide');
			App.pushState({ dispose:fnHideSlider });
		});
		
		slider.swipe(function(e){
			e.preventDefault();
			var state = App.popState();
			state.dispose();
		});

		this.$el.find('#settings_button').tap(function(e){				
			e.preventDefault();
			if ( App.popup.isSettings ){
				fnClosePopups();		
			}else{						
				fnClosePopups();	
				setTimeout(function(){window.location.hash = 'settings/questions';});
				//window.location.hash = 'settings/questions';
			}
			e.stopPropagation();
		});
		
		this.$el.find('#notifications_button').tap(function(e){	
			e.preventDefault();		
			if ( App.popup.isNotifications ){
				fnClosePopups();			
			}else{								
				fnClosePopups();	
				setTimeout(function(){window.location.hash = 'notifications';});
				//window.location.hash = 'notifications';
			}
			e.stopPropagation();
		});

		this.$el.find('#list_button').tap(function (e) {
			e.preventDefault();
			fnClosePopups();			
			console.log('list button');

			// HACK HACK HACK; incorrect routes
			if (window.location.hash == '#questions') {
				App.router.navigate('landing/list', {trigger: true, replace: false});
			} else {
				App.router.navigate('questions', {trigger: true, replace: false});
			}
			// HACK HACK HACK; incorrect routes
			e.stopPropagation();
		});

		this.$el.find('#map_button').tap(function (e) {
			var lat, lng;

			e.preventDefault();
			fnClosePopups();			
			console.log('map button');

			$('#post_wrapper').html(JST['questions-map']);

			/*
			lat: 10.333583 services.js:318
			lng: 123.912467 services.js:319
			zoom: 15 
			*/

			// TODO: Use App.Geolocation
			// geo = new App.Geolocation();
			lat =  coll.models[0].get('location_lat');
			lng =  coll.models[0].get('location_lng');

			geo = navigator.geolocation.getCurrentPosition(
				function(pos) {
					lat = pos.coords.latitude;
					lng = pos.coords.longitude;
				},
				function(error) {
					console.log('geolocation error');
					console.log('code: ' + error.code);
					console.log('message: ' + error.message);
				}
			);

			console.log(lat);
			console.log(lng);

			new App.GoogleMapInfowindow({
				el:$('#post_wrapper')[0],
				lat: lat,
				lng: lng,
				zoom: 15,
				markers: coll
			});
			e.stopPropagation();
		});
		
		this.$el.find('#question_button').tap(function(e){			
			e.preventDefault();
			if ( App.popup.isQuestion ){
				fnClosePopups();												
			}else{							
				fnClosePopups();	
				setTimeout(function(){window.location.hash = 'question/questions/create';});
				//window.location.hash = 'question/questions/create';
			}
			e.stopPropagation();
		});
		
		this.initial = true;
		this.eof = true;
		
		App.loading('show');	

		
	},

	render: function(){		
		return this;
	},
	
	onPageNotifications: function(payload){
		console.log('QuestionsView::onPageNotifications');
		
		// display notification count display
		if(payload.unseen){
			this.$notification_count.html(payload.count+' ');
		}else{
			this.$notification_count.html('&nbsp;');
		}
	},
	
	onChangeOrientation: function(){		
		console.log('QuestionsView::onChangeOrientation');
	},
	
	hide: function(){		
		this.stopListening();
	},
	
	show: function(){
		console.log('QuestionsView::onShow');
		
		//this.$el[0].style.webkitTransform = 'translate(0,0)';
		
		//this.listenTo(App.dispatcher, 'app:notifications', this.onPageNotifications.bind(this));
		
		$('#page_wrapper').redraw();

		if ( this.scroll ) 
			return;		
		
		var self = this;
		
		this.pullDownEl = document.getElementById('pullDown');
		this.pullDownOffset = this.pullDownEl.offsetHeight;
		this.pullUpEl = document.getElementById('pullUp');	
		this.pullUpOffset = this.pullUpEl.offsetHeight;		
		
		this.scroll = new iScroll('post_wrapper',{
				momentum: true,
				useTransition: true,
				topOffset: self.pullDownOffset,	
				hideScrollbar: false,
				onBeforeScrollStart: function (e) { 
					e.preventDefault();
					//e.stopPropagation();
				},				
				onRefresh: function () {
					if (self.pullDownEl.className.match('loading')) {
						self.pullDownEl.className = '';
						self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
					} else if (self.pullUpEl.className.match('loading')) {
						self.pullUpEl.className = '';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';						
					}
				},				
				onScrollMove: function () {					
					if (this.y > 5 && !self.pullDownEl.className.match('flip')) {
						self.pullDownEl.className = 'flip';
						self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Release to refresh...';
						this.minScrollY = 0;
					} else if (this.y < 5 && self.pullDownEl.className.match('flip')) {
						self.pullDownEl.className = '';
						self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
						this.minScrollY = -self.pullDownOffset;
					} else if (!self.eof && this.y < (this.maxScrollY - 5) && !self.pullUpEl.className.match('flip')) {
						self.pullUpEl.className = 'flip';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Release to refresh...';
						this.maxScrollY = this.maxScrollY;						
					} else if (!self.eof && this.y > (this.maxScrollY + 5) && self.pullUpEl.className.match('flip')) {
						self.pullUpEl.className = '';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
						this.maxScrollY = self.pullUpOffset;						
					}
				},				
				onScrollEnd: function () {
					if ( this.busy ) return;
									
					if (self.pullDownEl.className.match('flip')) {
						self.pullDownEl.className = 'loading';
						self.pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Loading...';				
						self._pullDown();
					} else if (self.pullUpEl.className.match('flip')) {
						self.pullUpEl.className = 'loading';
						self.pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Loading...';										
						self._pullUp();	
					}
				},
			});	
			
		setTimeout( this._getNewest.bind(this), 10 );
	},
	
	_pullUp: function(){			
		if (!this.busy){		
			this.busy = true;			
			this._getPage();	
		}
	},
	
	_pullDown: function(){
		if (!this.busy){		
			this.busy = true;								
			//this._pageOptions.page = 0;						
			//this._getPage();			
			this._getNewest();
		}
	},
	
	_getNewest: function(event) {		
		this.busy = true;
		this._pageOptions = {
			sort: 'newest',
			by: 0,
			page:0,
			paginate:1,		
			limit:15,
			delta: null
		};
		this._getPage();
	},
	
	_getPage: function(){	
		var self = this;
		// get next page
		this._pageOptions.page++;
		var questions = new App.Situations();
		questions.url = App.url('situations/getLatest.json', this._pageOptions);
		questions.fetch({
			success: function(model,data){ 		
				if ( data.count > 0 ){
				
					var last = data.items[data.count-1];
					self._pageOptions['delta'] = last.created_at;
						
					coll = new App.Situations(data.items);
					
					//self.lastPage = self._pageOptions.page;
					
					self.eof = false;
					
					$('#pullUp').css('display', 'block');									
					
					self._renderItems(coll);				
				}
				
				if ( data.count < self._pageOptions.limit ) {
					//self._pageOptions.page = self.lastPage ;
					
					self.eof = true;
					$('#pullUp').css('display', 'none');
				}
			},
			error: function(data, xhr){	
				App.toast( 'Connection error!' );
			},
			complete: function(){
				App.loading('hide');				
				self.busy = false;				
				self.scroll.refresh();
			}
		});
	},

	_renderItems: function(coll){					
		var partial = new App.PostItem({collection:coll}),
			self = this;
			
		if( this.initial ){
			(new App.Pulldown()).render();
			(new App.Pullup()).render();
			
			this.initial = false;
		}
			
		partial.render();
		
		if(this._pageOptions.page == 1){
			this.$el.find('#posts').first().empty();
		}			
		
		this.$el.find('#posts').append(partial.$el.html());
		
		// important! will not update without this
		this.$el.find('.time').timeago();
				
		// required for ALASKER-244: Links should function correctly.
		this.$el.find('#posts li .post.new a').tap( function(e){

			e.stopPropagation();
			return true;
		});

		this.$el.find('#posts li .post.new').tap( function(e){
			window.location.hash = 'question/' + $(e.currentTarget).attr('data-id') + '/reply';		

			e.preventDefault();
			e.stopPropagation();
			return false;
		});
		
		this.$el.find('#posts .post.new').removeClass('new');
		
		this.scroll.refresh();		
				
		setTimeout(function(){
			self.$el.find('#posts').redraw();					
		}, 10);
	},
});

App.NotificationsPopup = Backbone.View.extend({
	el : '#notification_container',
	
	initialize: function(){
		this.isPopup = true;
		
		this.$el.html( JST['notifications']( ) );			

		this.scroller = new iScroll(this.$el.find('#notification_wrapper')[0],{
			bounce: false,
			hideScrollbar: false,
			onBeforeScrollStart: function (e) { 
				e.preventDefault();
				e.stopPropagation();
			},				
		});
				
		this.getLatest();		
	},
	
	hide: function(){	
		console.log('hide notifications');
		this.stopListening();
		this.$el.removeClass('popup');		
		App.popup.isNotifications = false;
		window.location.hash = 'questions';
	},
	
	show: function(){
		console.log('show notifications');
		this.$el.addClass('popup');		
		this.listenTo(App.dispatcher, 'app:goback', this.hide.bind(this));
		this.listenTo(App.dispatcher, 'popup:close', this.hide.bind(this));
		App.popup.isNotifications = true;
	},
	
	render: function(){
		return this;
	},
	
	getLatest: function(){
		App.loading('show');
		var notifications =  new App.Notifications();		
		notifications.getLatest( { success:this._success.bind(this), error:this._error.bind(this), complete:this._complete.bind(this) } );
	},
	
	_success: function(model,data){ 		
		var count = data.items.length;
		
		if( count > 0 ){
			this._renderItems(data.items);
		}
	},
	_error: function(data, xhr){	
		App.toast( 'Connection error!' );
	},
	
	_complete: function(){
		App.loading('hide');				
	},
		
	_renderItems: function(items){		
		console.log('render notification items');
		var partial = new App.NotificationItem({collection:new App.Notifications(items)});
			
		partial.render();
		
		this.$el.find('#posts').append(partial.$el.html());
		
		// important! will not update without this
		this.$el.find('.time').timeago();
		
		this.scroller.refresh();		
	}
});

App.ChatPopup = Backbone.View.extend({
	//el : '#popup_container',
	el : '#chat_pad',
	
	initialize: function( question ){
		this.maximizeMap = false;
		
		this.isPopup = true;
		
		this.question = question;		
		
		this.chat = new App.ChatProxy();

		this.$el.html( JST['chat']( question ) );	
		
		this.$el.find('.time').timeago();
		
		this.scroller = new iScroll(this.$el.find('#standard')[0],{
			bounce: false,
			hideScrollbar: true,
			onBeforeScrollStart: function (e) { 
				e.preventDefault();
				e.stopPropagation();
			},				
		});
				
		this.$el.find('#respond_button').tap( this.onRespond.bind(this) );		
		this.$el.find('#cancel_button').tap( this.hide.bind(this) );
		this.$el.find('#maximize_button').tap( this.maximize.bind(this) );
		
		this.count = 0;
	},	
	
	maximize: function(){
		console.log('ChatPopup::maximize');
		this.maximizeMap = !this.maximizeMap;
		
		if( this.maximizeMap )			
			this.$el.find('#map_wrapper').addClass('maximize');
		else
			this.$el.find('#map_wrapper').removeClass();					
	},
	
	onRespond: function(e){
		e.preventDefault();
		
		var content = this.$el.find('#respond_text').val();
		
		this.$el.find('#respond_text').val('');				
		this.chat.send( content );
	},
	
	show: function(){
		var pb = $('#popup_background'),
			pc = this.$el;

		// show the background layer. 
		setTimeout( function(){pb.addClass('popup');}, 0);
						
		// fire backbutton when background layer is swiped
		pb.swipe( this.hide.bind(this) );	
				
		// show popup contents
		pc.addClass('popup');		
		
		//pc.find('#respond_text').focus();
	
		this.chat.start(this.question.id, this._success.bind(this), this._error.bind(this) );
		
		pc.find('.time').timeago();
		
		this.listenTo(App.dispatcher, 'app:goback', this.hide.bind(this) );
		this.listenTo(App.dispatcher, 'popup:close', this.hide.bind(this) );
		
		new App.GoogleMap({el:this.$el.find('#map_wrapper')[0], lat:this.question.lat, lng:this.question.lng, zoom: 15, radius:this.radius});
	},
	
	hide: function(e){		
		e.preventDefault();
		// hide background layer
		var pb =  $('#popup_background');
		
		pb.removeClass();
		pb.off();
		
		this.busy = false;
		// clean up popup container
		this.$el.removeClass();
		
		this.stopListening();
		
		this.chat.stop();
		
		window.location.hash = 'questions';
	},
	
	render: function(){		
		return this;
	},
	
	_success: function(model,data){ 		
		var count = data.count;
		
		if( count > 0 ){
			this._renderItems(data.items);
		}
	},
	
	_error: function(data, xhr){	
		App.toast( 'Connection error!' );
	},
	
	_renderItems: function(items){					
		var partial = new App.ChatItem({collection:new App.SituationNotes(items)});
		
		partial.setRowType( (this.count+1) % 2 != 0 );
		
		this.count += items.length;
			
		partial.render();
		
		this.$el.find('#chat_container').append(partial.$el.html());
		
		this.scroller.refresh();		
		
		this.scroller.scrollToElement(this.$el.find('#end-of-list')[0], 50);
	}
});
