// ------------------------------
// Models
// ------------------------------
App.User = Backbone.Model.extend({
	url : App.url('users.json'),
	defaults : {
		username : '',
		password : '',
		email : '',
		website : '',
	},	
});

App.Situation = Backbone.Model.extend({
	url : App.url('situations.json'),
});

App.Plan = Backbone.Model.extend({
	url : App.url('plans.json'),
});

App.Category = Backbone.Model.extend({});

App.SituationCondition = Backbone.Model.extend({});

App.PlanCondition = Backbone.Model.extend({});

App.Login = Backbone.Model.extend({
	url : App.url('mobile/login'),
	defaults : {
		login		: '',
		password 	: '',
		provider	: '',
		uid			: ''
	},	
});

App.Note =  Backbone.Model.extend({
	url: App.url('notes.json'),
});

App.Json = Backbone.Model.extend({});

// ------------------------------
// Collections
// ------------------------------

App.Notifications = Backbone.Collection.extend({
	model: App.Json,
	
	getLatest: function(options){
		var success = options.success || function(){},
			error = options.error || function(){},
			complete = options.complete || function(){};
		
		// NOTE: auth_token needs to be used globally
		this.url = App.url('/notifications/getLatest.json', { 'auth_token': App.getUser().auth_token });
		
		this.fetch( { success:success, error:error, complete:complete } )
	}
});

App.LatestNotifications = Backbone.Collection.extend({
	model: App.Json,
	
	poll: function(options){
		var success = options.success || function(){},
			error = options.error || function(){};
		
		this.url = App.url('/notifications/getLatestCount.json', { 'auth_token': App.getUser().auth_token });
		
		this.fetch( { success:success, error:error } )
	}
});


App.SituationNotes = Backbone.Collection.extend({
	model: App.Note,		
	poll: function(options){
		var id = options.id || 0,
			delta = options.delta || null,
			win = options.success || function(){},
			fail = options.error || function(){},
			finish = options.complete || function(){};
			
		this.url = App.url('/situations/'+id+'/notes.json', { delta:delta });
		
		this.fetch( { success:win, error:fail, complete:finish } )
	},	
});

App.Situations = Backbone.Collection.extend({
	model : App.Situation,
	search: function(filter){
		if (filter) {
			this.url = App.url('situations.json', {'situationsearch' : '+'+filter});
		} else {
			this.url = App.url('situations.json', {});
		}
	},
});

App.Categories = Backbone.Collection.extend({
	model : App.Category,
	url : App.url('categories.json'),
});

App.Plans = Backbone.Collection.extend({
	model : App.Plan,
});


