App.Router = Backbone.Router.extend({

   routes: {
		'' : 'initialize',
		'landing/list': 'questions',
		'signin' : 'signin',
		'signup' : 'signup',
		'logout' : 'logout',		
		'register' : 'register',
		'questions': 'questions',
		'facebook' : 'facebook',
		'twitter' : 'twitter',
		'question/:id/reply' : 'reply',
		'question/:id/chat': 'chat',
		'question/:callback/create' : 'postquestion',
		'settings/:callback' : 'settings',
		'notifications' : 'notifications'
	},
	
	postquestion: function(cb){
		console.log('#post');		
		App.dispatcher.trigger('popup:close');	
		
		this.changePage( new App.PostQuestionPopup( {callback:cb}) );		
	},
	
	notifications: function(){
		console.log('#notifications');		
		App.dispatcher.trigger('popup:close');	
		this.changePage( new App.NotificationsPopup() );		
	},
	
	settings: function(cb){
		console.log('#settings');		
		App.dispatcher.trigger('popup:close');	
		this.changePage( new App.SettingsPopup( {callback:cb}) );		
	},
	
	reply: function( id ){	
		console.log('#question');
		
		var cache = App.getCache('q');
		console.log('cache');
		console.log(cache);
		this.changePage( new App.QuickReplyPopup( cache[id] ) );		
	},
	
	chat: function( id ){
		console.log('#chat');		

		var cache = App.getCache('q');		
		this.changePage( new App.ChatPopup( cache[id] ) );
	},	

	initialize: function() {	
		console.log('#initialize');
		App.getUser();
		
		if ( App.currentUser ) {
			// automatically signin user 			
			this.autologin();
		} else {
			// proceed to default 
			this.register();
		}
	},
	
	register: function(){
		console.log('#register');
		this.changePage(new App.RegisterView());
	},
	
	questions: function(){		
		console.log('#questions_list');
		App.dispatcher.trigger('popup:close');	
		this.changePage(new App.QuestionsView());
/*
		if ( !App.pages['questions-list'] )
			App.pages['questions-list'] = new App.QuestionsView();
		
		this.changePage( App.pages['questions-list'] );	
*/
	},

	signup:function () {
		console.log('#signup');		
		this.changePage(new App.SignUpView());
	},
	
	signin:function() {
		console.log('#signin');
		this.changePage(new App.LoginView());
	},
	
	autologin:function( credential ){
		var model = new App.Login();
		
		console.log('#autologin');
		
		if ( !credential ) 
			credential = App.getUser();
		
		App.loading('show');
		
		model.save( { user_login: credential }, {
			success: function(session, response) {
				console.log('login succeeded');
				App.setUser( response );
				App.router.navigate('landing/list', {trigger: true, replace: true});
				
				setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
			},
			error: function(session, response) {
				console.log('login failed');
				try 
				{	
					var json = JSON.parse(response.responseText);									
					App.toast(json.message);	
				} 
				catch(e) 
				{
					App.toast('Connection error!');
				} 
				finally
				{
					window.location.hash = 'register';
				}
			},
			complete: function(){
				App.loading('hide');
			}
		});
	},

	logout: function() {
		console.log('#signout');
		var user = App.currentUser;		
		
		$.ajax({
            url: App.url('mobile/logout'),			
            dataType: 'json',
            async:true,
            type:'DELETE',
			data: {user_login: {login: user.username}},
            success: function(p1,p2,p3){
				console.log('logout success');
				App.dispatcher.trigger('app:logout');	
			},
            error: function(p1,p2,p3){
				console.log('Connection error!');
			}
        });
	}, 
	
	fbComplete: function(data){
		console.log('#fbComplete');		
        this.changePage(new App.SignUpView( { payload:data } ));
	},
		
	facebook: function(){	
		var fnComplete = this.fbComplete.bind(this),		
			success = function(data) {					
				fb.getUserData(
					function(data)
					{											
						var	json = new App.Json();							
						json.url = App.url('auth/:facebook/callback.json');
						json.save( { auth:data }, {
							success: function(model, response){
								if( response.authenticated ){
									App.setUser( response );
									App.router.navigate('landing/list', {trigger: true, replace: true});
									setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
								}else{
									fnComplete(response);
								}
							},
							error: function(mode, xhr){
								window.location.hash = 'register';
							}
						} );						
					}, 
					function()
					{
						window.location.hash = '';
					}
				);				
			},
			error = function(data){
				window.location.hash = '';
				App.checkInternet();
			},		
			fb = new Facebook(success, error);
			
		fb.authenticate();		
	},
	
	twitter: function() {
		var login = new App.Login(),
			fnComplete = this.fbComplete.bind(this),	
			success = function(data){					
					console.log('Twitter SUCCESS'); 
					/*login.set( { uid : data['id_str'], provider : 'twitter', login : '', password : ''  } );			
					login.save( { user_login: login.toJSON() }, {
						success: function(session, response) {
							console.log('*** login success');
							App.setUser( response );
							App.router.navigate('landing/list', {trigger: true, replace: true});
							
							setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
						},
						error: function(session, response) {
							console.log('*** login error');				
							window.location.hash = '';
						}
					});*/
					
					payload = {
						provider:'twitter',
						uid: data.id,						
						info: {
							username: data.screen_name							
						},
						credentials: {
							token: tw.getToken()									
						}
					};
					
					var	json = new App.Json();							
						json.url = App.url('auth/:twitter/callback.json');
						json.save( { auth:payload }, {
							success: function(model, response){
								if( response.authenticated ){
									App.setUser( response );
									App.router.navigate('landing/list', {trigger: true, replace: true});

									setTimeout( function(){ App.dispatcher.trigger('app:login') }, 500 );
								}else{
									fnComplete(response);
								}
							},
							error: function(mode, xhr){
								window.location.hash = 'register';
								App.toast( 'Connection error!' );
							}
						} );						
					
					window.location.hash = '';
				},
			error = function(data){
					console.log('Twitter ERROR'); 
					window.location.hash = '';
					
					App.checkInternet();
				},
			tw = new Twitter(success, error);
		
		tw.authenticate();		
	},
		
	changePage: function (page) {						

		var showPage = function(){
		
			page.render();		

			if ( !page.isPopup ){	
				if( window.__page && window.__page.hide )
					window.__page.hide();
			
				window.__page = page;	
			
				$('#page_wrapper').html($(page.el));	
			}
			
			page.show();
		
		}
		
		setTimeout(showPage, 50);		
	},	
});


