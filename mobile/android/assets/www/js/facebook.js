// ------------------------------
// Facebook
// ------------------------------
function Facebook() {
	this.cb = App.browser();
	this.key = 'facebook';
	this.options = App.config.facebook;
	this.oauth = null;
	
	// overridable authentication callbacks
	this.onAuthenticated = function(entry){};
	this.onError = function(data){};
};
Facebook.prototype = {
	authenticate: function(){
		var self = this;
		
		var authorize_url = "https://graph.facebook.com/oauth/authorize?";
			authorize_url += "&response_type=code";	
			authorize_url += "&client_id=" + this.options.apiId;
			authorize_url += "&redirect_uri=" + this.options.callbackUrl;
			authorize_url += "&scope=email,publish_stream,offline_access";	
				  
 
		this.cb.onLocationChange = function( loc ){
			self.callback( loc );
		};
		this.cb.onClose = function(){
			self.onError({});
		};
		
		this.cb.showWebPage(authorize_url);
	},
	
	callback : function( loc ) {
		var self = this;
				
		if (loc.indexOf(App.config.facebook.callbackUrl) > -1) {	
		
			var code = loc.match(/code=(.*)$/)[1];
			
			console.log(code);
					
			if( code ) {			
				var graph_url = "https://graph.facebook.com/oauth/access_token?";					
					graph_url += "&client_id=" + this.options.apiId;
					graph_url += "&client_secret=" + this.options.apiSecret;
					graph_url += "&redirect_uri=" + this.options.callbackUrl;
					graph_url += "&code="+code;
					
				$.ajax({
					url: graph_url,
					data: {},
					dataType: 'text',
					type: 'POST',
					success: function(data, status){
						console.log('success facebook');
						// We store our token in a localStorage Item called facebook_token
						localStorage.setItem('facebook', data.split("&")[0].split("=")[1]);
	 
						self.cb.close();		 
						self.onAuthenticated( data );
					},
					error: function(error) {
						self.cb.close();
						self.onError();
					}
				});
				
			} else {				
				self.cb.close();
				self.onError();				
			}
			
		}
					
	},

	getUserData : function( onSuccess, onError ){
		onSuccess = onSuccess || function(){};
		onError = onError || function(){};
		var graph_url = "https://graph.facebook.com/me/?access_token="+localStorage.getItem('facebook');
		$.ajax({
			url: graph_url,			
			data: {},
			dataType: 'text',
			type: 'GET',
			success: function(data, status){
				var data = JSON.parse(data);
				dump( data );
				onSuccess( data );
			},
			error: function(error) {
				onError( error );
			}
		});
	}
}

