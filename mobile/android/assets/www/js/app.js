window.App = {
	url: function(path, params) {		
		var kv = "";
		if (params) {
			$.each(params, function(key, value){
				kv += '&' + key + '=' + encodeURI(value);
			});
			kv += "&_=" + (new Date()).getTime();
		}
		var url = "http://"+App.config.host+"/"+path+"?access_key=p1an6"+kv;
		console.log('App url: '+url);
		return url;
	},
	
	get: function(path, params, callback){		
		$.ajax({
            url:  App.url(path, params),
            dataType: "text",
            async:true,
            type:'GET',
            data: null,
            success: function(data, textStatus, jqXHR){
				callback(data);
			}
		});
	},
	
	resize: function(el, extra, pr) {		
		pr = pr || 1;
		
		var page = 0;
		
		if(App._isDevice){
			App.setHeight();
			page = App.pageHeight;
		}
		else{
			page = $(window).height();
		}
		
		console.log('*** page:'+page);
		
		console.log('*** extra:'+extra/page);		
		var header = $('[data-role="header"]').height();
		if(!header) header=0;		
		console.log('*** header:'+header);		
		var footer = $('[data-role="footer"]').height();
		console.log('*** footer:'+footer);		
		var ratio = 1-(extra+footer+header+pr)/page;
		
		el.height(page*ratio);					
		console.log('*** height:'+el.height());
	},
	
	run: function() {		
		App._isDevice = false;
		
		console.log('user agent:'+navigator.userAgent);
		if (navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry)/)) {			
			document.addEventListener("deviceready", App.ondeviceReady, false);	
		}else{
			App._initialize(); 			
		}		
	},
	
	ondeviceReady:function(){
		App._isDevice = true;	
		App._initialize();
	},
	
	browser: function(){
		if(App._isDevice){
			return window.plugins.childBrowser;
		}
		
		return new PopupBrowser();
	},		
	
	_resize: function() {
		console.log('resizing viewport');

		if( window.__page && window.__page.onresize) {
			window.__page.onresize();
		}
	},
	
	setHeight: function(){
		switch(window.orientation)
		{ 
			case -90:
				//options = [{landscapeleft:true}];
				App.pageHeight = Math.min(window.innerHeight, window.innerWidth);
				break;
			case 90:
				//options = [{landscaperight:true}];
				App.pageHeight = Math.min(window.innerHeight, window.innerWidth);
				break;
			case 180:
				//options = [{portraitdown:true}];
				App.pageHeight = Math.max(window.innerHeight, window.innerWidth);
				break;
			default:
				//options = [{portrait:true}];
				App.pageHeight = Math.max(window.innerHeight, window.innerWidth);
				break;
		}  
			
		if(App.isrotate)
			App.pageHeight -= 48;
	},
	
	_rotate: function() {
		App.isrotate = true;
		App._resize();
	},
	
	_initialize : function() {		
		if (App._isDevice){
			console.log('initializing on device');
			document.addEventListener("backbutton", App._goBack, false);
			$(window).bind("orientationchange", App._rotate);
		}else{
			console.log('initializing on browser');
			$('.back').bind('click', App._goBack);
			$(window).bind('resize', App._resize );
		}
		
		App.router = new App.Router();
		Backbone.history.start();		
	},
	
	_goBack : function(event) {
		window.history.back();
		return false;
	},
}

App.htmlHelper = {		
	to_link : function(arrayValue){
		var html = [		
			"<% for(var tag in tags) { %>",
			"<a href=''><%= tags[tag] %></a>",
			"<% } %>",
			].join('');

		return  _.template(html, { tags: arrayValue });
	},
}
