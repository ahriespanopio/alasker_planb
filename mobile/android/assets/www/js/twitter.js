// ------------------------------
// Twitter
// ------------------------------
function Twitter() {
	this.cb = App.browser();
	this.key = 'tweeter';
	this.options = App.config.twitter;
	this.oauth = null;
	
	// overridable authentication callbacks
	this.onAuthenticated = function(entry){};
	this.onError = function(data){};
};
Twitter.prototype = {
	authenticate: function(){		
		var self = this, userData = this.getUserData();
		
		// verify stored credentials
		if ( userData ) {						 
			this.options.accessTokenKey = userData.accessTokenKey; 
			this.options.accessTokenSecret = userData.accessTokenSecret; 
			
			// give access to this user
			this.oauth = OAuth(this.options);
			this.oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
				function(data) { self.onAuthenticated( JSON.parse(data.text) ); },
				function(data) { self.onError(data); }
			);
		}
		else {
			console.log('obtain unathorized request token');
			// obtain unathorized request token
			this.oauth = OAuth(this.options);
			this.oauth.get('https://api.twitter.com/oauth/request_token',
				function(data) {
					self.requestParams = data.text;									
					// direct user to twitter service
					self.cb.showWebPage('https://api.twitter.com/oauth/authorize?'+data.text);	
					self.cb.onClose = function(){
						self.onError({});
					};
					// handle redirect from twitter
					self.cb.onLocationChange = function(loc){ 
						self.callback(loc); 
					};
				},
				function(data) { self.onError(data); }
			);
		}
	},
		
	callback: function(loc){
		var self = this;
		console.log('TWITTER REDIRECT: '+loc);
		
		// check if this is a twitter callback
		if (loc.indexOf(App.config.twitter.callbackUrl) >= 0) {
			
			// kill childBrowser events
			self.cb.onLocationChange = function(){};
			
			// we need oauth_verifier value in requesting for access token 
			var verifier = '';            
			var params = loc.substr(loc.indexOf('?') + 1);
			
			params = params.split('&');
			for (var i = 0; i < params.length; i++) {
				var y = params[i].split('=');
				if(y[0] === 'oauth_verifier') {
					verifier = y[1];
				}
			}
			
			if ( verifier ) {
				console.log('exchange request token for access token');
						
				// exchange request token for access token
				this.oauth.get('https://api.twitter.com/oauth/access_token?oauth_verifier='+verifier+'&'+self.requestParams,
					function(data) {    		
						var accessData = {};
						var qvars_tmp = data.text.split('&');
						for (var i = 0; i < qvars_tmp.length; i++) {
							var y = qvars_tmp[i].split('=');
							accessData[y[0]] = decodeURIComponent(y[1]);
						}
							
						// give access to user
						self.oauth.setAccessToken([accessData.oauth_token, accessData.oauth_token_secret]);				
						self.oauth.get('https://api.twitter.com/1.1/account/verify_credentials.json?skip_status=true',
							function(data) { 
								self.putUserData( accessData );
								self.onAuthenticated( JSON.parse(data.text) ); 
							},
							function(data) { self.onError(data); }
						);
						
						// Since everything went well we can close our childBrowser!                             
						self.cb.close();
					},
					function(data) { self.onError(data); }
				);
			} else {
				console.log('cancelled');
				self.cb.close();
				self.onError({});
			}
		}
	},

	getUserData: function(){	
		// read data from store
		var rawData = localStorage.getItem(this.key);
		
		// convert to JSON object
		if(rawData){
			return JSON.parse(rawData);
		}	
		
		// no data 
		return null;
	},
	
	putUserData: function(accessData){
		// get what we need
		var userData = {
			accessTokenKey : accessData.oauth_token,
			accessTokenSecret : accessData.oauth_token_secret
		};
		
		// store it 
		localStorage.setItem(this.key, JSON.stringify(userData));
	},
	
	tweet:function(status){				
		this.oauth.post('https://api.twitter.com/1.1/statuses/update.json',
			{ 
				'status' : status, 
				'trim_user' : 'true' 
			},
			function(data) {
				console.log( 'tweet succeeded:' /*+ data.text*/ );
			},
			function(data) { 
				console.log( 'tweet encountered error:');
			}
		);		
	}
}

function twitterTest(){
	var tw = new Twitter();	 
	tw.onAuthenticated = function(data){		
		//console.log("Authenticated USER: "+data.screen_name);
		dump( data );
	};
	tw.onError = function(data){
		console.log("Authentication ERROR: " + data); 
	};
	tw.authenticate();	
	return tw;
}

function dump( obj ) {
	$.each(obj, function(k, v) {		
		console.log(k + ' is ' + v);
	});
}