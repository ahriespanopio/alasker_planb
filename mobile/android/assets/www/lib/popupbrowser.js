PopupBrowser = function() {	
	var self = this;
		
	var location = "about:blank";
	var hash = "";
	var prevLocation = "";
	var prevHash = "";	
	var interval = null;
	var popup = null;
  
	// removes the pound from the hash
	var cleanHash = function( hash ){
		return hash.substring(1, hash.length);	
	}
 
	// check changes in the window location
	var checkLocation = function(){	
		if (popup && popup.closed){
			// fire the closeWindow event
			$( self ).trigger("closeWindow");
			return;
		}
	
		// determine if the location has changed.
		if (location != popup.location.href){			
			// Store the new and previous locations.
			prevLocation = location;
			prevHash = hash;
			location = popup.location.href;
			hash = popup.location.hash;
			 
			// fire the changeLocation event
			$( self ).trigger(
				"changeLocation",	
				{
					currentHref: location,
					currentHash: cleanHash( hash ),
					previousHref: prevLocation,
					previousHash: cleanHash( prevHash )
				}
			);				 
		}
	}	
			
	var result = {
		showWebPage: function(url){
			// open the widow
			popup = window.open(url, '', "height=600,width=600");
			
			// we check location every given interval
			interval = setInterval( checkLocation, 100 );			
			
			// listen for changeLocation event
			$( self ).bind("changeLocation", function(objEvent, objData){
				result.onLocationChange( objData.currentHref );
			});
						
			// listen for closeWindow event
			$( self ).bind("closeWindow", function(objEvent, objData){
				result.close();
				result.onClose();
			});
		},	
		
		close: function(){
			console.log('[PopupBrowser close]');
			// stop checking for location
			clearInterval(interval);			
			// release event listerners
			$( self ).unbind("changeLocation");
			$( self ).unbind("closeWindow");
			// release the popup
			popup.close();
			popup = null;
		},
				
		onLocationChange: function( href ){  // override this in consumer code
		},
		
		onClose: function() {
		}
	};
	
	return result;
}