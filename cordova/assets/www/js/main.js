var AppRouter = Backbone.Router.extend({

   routes: {
		""					: "home",
		"situation/:id"		: "viewSituation",
		"login" 			: "login",
		"start"	: "start",
		"newsituation"  	: "newSituation",
		"nextsituation" 	: "nextSituation",
		"completesituation" : "completeSituation",
		"list"  			: "list",
		"newuser"  			: "newUser",		
	},

    initialize:function () {
        // Handle back button throughout the application
        $('.back').live('click', function(event) {
            window.history.back();
            return false;
        });
		
        this.firstPage = true;
    },

    home:function () {
        console.log('#home');
        this.changePage(new HomeView());
    },

    viewSituation:function () {
        console.log('#viewSituation');
        this.changePage(new viewSituation());
    },

    login:function () {
        console.log('#login');
        this.changePage(new loginView());
    },
	
	start:function () {
		
        console.log('#startSituation');
		
        this.changePage(new startSituationView());
	
    },
	
	newSituation:function () {
        console.log('#newSituation');
        this.changePage(new newSituationView());
    },
	
	
	nextSituation:function () {
        console.log('#nextSituation');
        this.changePage(new nextSituationView());
    },
	
	completeSituation:function () {
        console.log('#completeSituation');
        this.changePage(new completeSituationView());
    },
	
	
	
	
	list:function () {
		
        console.log('#list');
        this.changePage(new listView());
		
	

	
    },
	
	
	newUser:function () {
        console.log('#newUser');
        this.changePage(new newUserView());
    },

    changePage:function (page) {
		
        $(page.el).attr('data-role', 'page');
		
        page.render();
        $('body').append($(page.el));
	
		
        var transition = $.mobile.defaultPageTransition;
        // We don't want to slide the first page
        if (this.firstPage) {
            transition = 'none';
            this.firstPage = false;
        }
		
        $.mobile.changePage($(page.el), {changeHash:false, transition: transition});
		
		console.log($(page.el));
		$($(page.el)).live('pageshow',function(event, ui){scrollBar();});
    }

});


function scrollBar(){
	var oScrollbar = $('#scrollbar1');
oScrollbar.tinyscrollbar({sizethumb:'42'});
 }



function startApp() {
	tpl.loadTemplates(
    	['home', 'complete-situation', 'list-situations', 
    	'login', 'new-user', 'next-situation', 'start-situation', 'view-situation',	'new-situation' ], 
    	function() {
    	console.log('document ready');
    	app = new AppRouter();
    	Backbone.history.start();
    });	
  
	

}