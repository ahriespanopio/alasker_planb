window.HomeView = Backbone.View.extend({

   
    initialize: function() {
        console.log('homeview init');
        this.template = _.template(tpl.get('home'));
    },

    render:function (eventName) {
		console.log('homeview render');
        $(this.el).html(this.template());
		
        return this;
    }
	
	
});

window.viewSituation = Backbone.View.extend({

  initialize: function() {
        this.template = _.template(tpl.get('view-situation'));
    },


    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});

window.loginView = Backbone.View.extend({

   initialize: function() {
        this.template = _.template(tpl.get('login'));
    },

    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});



window.startSituationView = Backbone.View.extend({

      initialize: function() {
        this.template = _.template(tpl.get('start-situation'));
    },
	
    render:function (eventName) {
        $(this.el).html(this.template());
		
        return this;
    }
});


window.newSituationView = Backbone.View.extend({

     initialize: function() {
        this.template = _.template(tpl.get('new-situation'));
    },


    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});


window.nextSituationView = Backbone.View.extend({

    initialize: function() {
        this.template = _.template(tpl.get('next-situation'));
    },

    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});


window.completeSituationView = Backbone.View.extend({

    initialize: function() {
        this.template = _.template(tpl.get('complete-situation'));
    },

    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});



window.listView = Backbone.View.extend({

    initialize: function() {
        this.template = _.template(tpl.get('list-situations'));
    },

	    render:function (eventName) {
        $(this.el).html(this.template());
		
        return this;
		
    }
	
	
		
});


window.newUserView = Backbone.View.extend({

    initialize: function() {
        this.template = _.template(tpl.get('new-user'));
    },

    render:function (eventName) {
        $(this.el).html(this.template());
        return this;
    }
});


