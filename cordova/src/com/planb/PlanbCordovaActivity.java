package com.planb;

import android.os.Bundle;
import org.apache.cordova.*;

public class PlanbCordovaActivity extends DroidGap {
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setIntegerProperty("loadUrlTimeoutValue", 1200000);
        super.loadUrl("file:///android_asset/www/screen1.html");
    }
}