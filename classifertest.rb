 require 'classifier'
    b = Classifier::Bayes.new 'Interesting', 'Uninteresting'
    b.train_interesting "here are some good words. I hope you love them"
    b.train_uninteresting "here are some bad words, I hate you"
    

puts b.classify "I hate bad words and you" # returns 'Uninteresting'

puts b.classify "I love Sofia" # returns 'Uninteresting'

