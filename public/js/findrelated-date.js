$(document).ready(function() {
   	$("#situationcondition_whendate").datepicker({
        showOn: 'focus', buttonText: "Select", minDate: "-30", defaultDate: "1", dateFormat: "MM d, yy "+ date_obj_time,      
        onSelect: function(dateText, inst){
  						var theDate = new Date(Date.parse($(this).datepicker('getDate')));
  						var dateFormatted = $.datepicker.formatDate('MM d, yy', theDate);
  						window.location.replace('/categories/findrelated?search_whendate='+dateFormatted);
  					}
        
      });
      
      $(".ui-datepicker-trigger").append("<span></span>");         
});


