var xhr = null;
$(function() {
	$('.field, textarea').focus(function() {
    if(this.title==this.value) {
        this.value = '';}
  }).blur(function(){
    if(this.value=='') {
        this.value = this.title;}
  });

  if ($.browser.msie && $.browser.version.substr(0,1)<7) {
      DD_belatedPNG.fix('#logo a, #top-boxes .title');
      $('#navigation .dd').css({ width: ($('#navigation .dd ul').width())});
  }

  $('#slider').jcarousel({
   	scroll: 1,
		auto: 5,
		wrap: 'circular',
		loop: true,
		itemFirstInCallback : mycarousel_firstCallback,
    wrap: 'null',
      initCallback: function(jc, state) {
        if (state == 'init') {
          jc.startAutoOrig = jc.startAuto;
          jc.startAuto = function() {
            if (!jc.paused) {
              jc.startAutoOrig();
            }
          }
          jc.pause = function() {
            jc.paused = true;
            jc.stopAuto();
          };
          jc.play = function() {
            jc.paused = false;
            jc.startAuto();
          };
          $('li.jcarousel-item').mouseover(function() {
              jc.pause();
          });
          $('li.jcarousel-item').mouseout(function() {
              jc.play();
          });
        }
        jc.play();
      }
    });

    function mycarousel_firstCallback(carousel, item, idx) {
      if ( (idx) == $('#slider .jcarousel-list').children().length )
        $('.jcarousel-next').addClass('inactive');
      else
        $('.jcarousel-next').removeClass('inactive');
      if ( (idx) == 1 )
        $('.jcarousel-prev').addClass('inactive');
      else
        $('.jcarousel-prev').removeClass('inactive');
    }

    $('.posts li .post').live('mouseenter', function() {
      $(this).addClass("post-gradient");
    	$(this).find('.button').show();
    	$(this).find('.social-links').show();
    });

    $('.posts li .post').live('mouseleave', function() {
    	$(this).removeClass("post-gradient").find('.button').hide();
    	$(this).find('.button').hide();
    	$(this).find('.social-links').hide();
    });

    $('.posts li .post').hover(function(){
      if ($('.previewbox').length > 0) {
        var prevbox = $('.previewbox')[0].outerHTML;
        $(this).append(prevbox);
	    	$(this).find('.previewbox').show();
		   	cancelPreview();
	   	$('.previewboxcontent').html('Searching for answers...').delay(100);
        var showUrl = $(this).find('.button').attr('href') + "/quickshow";
	    	xhr = $.ajax({
    	    type: "GET",
    	    url: showUrl,
    	    success: function(msg){
            var content = $.trim(msg);
             if(content.length > 0){
               if ($('.previewbox').is(':visible')) {
                  $('.previewboxcontent').html(content);
             /*alert( "Data Saved: " + msg ); */
               }
             } else {
                 $('.previewboxcontent').html("Nothing found. Be the first one to answer!");
             }
    	    }
	    	});
    	}

    },function(){
      if ($(this).find('.previewbox').length > 0) {
    		$(this).find('.previewbox').remove();
        $(this).removeClass("post-gradient").find('.button').hide();
    		$(this).find('.button').hide();
    		$(this).find('.social-links').hide();
    		cancelPreview();
    	}
    });

    $('#navigation ul li').hover(function(){
      $(this).find('a:first-child').addClass('nav_hover');
    	$(this).find('.dd:first').stop(true, true).fadeIn();
      $('#navigation .dd').css({ width: (($('#navigation .dd').children().length - 1) * 37)});
    }, function(){
    	$(this).find('.dd:first').fadeOut();
      $(this).find('a:first-child').removeClass('nav_hover');
    });

	$(".select").selectbox();
	$('#footer .nav').css({ 'padding-left': ($('#footer .shell').width() - $('#footer .nav').width())/2 });
  $(".datepicker").datepicker({dateFormat: 'yy-mm-dd'});
});

function cancelPreview()
{
	if (xhr != null) {
		xhr.abort();
		xhr = null;
	}
}
