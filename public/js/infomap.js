  var myScroll;

  var GoogleMapInfowindow = function() {
    var map, el, lat, lng, latLng;
    var me = Object.create(GoogleMapInfowindow.prototype, {
      initialize: {
        value: function(options) {
          var options = options || {};
          var el = options.el || document.body;
          var lat = options.lat || 0;
          var lng = options.lng || 0;   
          var zoom = options.zoom || 1;
          var center = new google.maps.LatLng(lat, lng);        
          var map = new google.maps.Map(el, {
            center: center,
            zoom: 6, // zoom
            disableDefaultUI : true,
            scrollwheel: true,
            disableDoubleClickZoom: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          });
          var infowindow = new google.maps.InfoWindow();
          var markers = [];
          var globalMarker = [];

          var i = 0;

          // requires underscorejs
          var map_marker = _.template($('#map-marker-tpl').html());

          options.markers.each(function(i, e) {
            console.log(e);

            if (!(e.lat && e.lng)) {
              return false;
            }
              tmp = map_marker({
                avatar_map: e.avatar,
                user_name: e.user_name,
                created_at: e.when,
                category: e.category,
                // ???
                // situationcondition_id: e.get('situationcondition_id'),
                situationcondition_id: 0,
                content: e.content // needs to be escape
              }),

            marker = new google.maps.Marker({
              position: new google.maps.LatLng(
                e.lat,
                e.lng
              ),
              draggable: true,
              raiseOnDrag: true,
              map: map,
              labelContent: map_marker({
                avatar_map: e.avatar,
                user_name: e.user_name,
                created_at: e.when,
                category: e.category,
                // ???
                // situationcondition_id: e.get('situationcondition_id'),
                situationcondition_id: 0,
                content: e.content // needs to be escape
              }),
              labelAnchor: new google.maps.Point(30, 0),
            });

            markers.push(marker);

            // weird, touchstart event doesn't work
            google.maps.event.addListener(markers[i], 'click', function(e) {
              var content = '';

              content = '<div id="map_wrapper"><div id="map_scroller"><ul id="map_list">';
              content += this.labelContent;
              content += '</div></div></ul>';

              infowindow.setContent(content);
              infowindow.open(map, this);
            });


            i++;
          });

          var mc = new MarkerClusterer(map, markers, { zoomOnClick: false });

          globalMarker = markers.slice();
          google.maps.event.addListener(mc, 'clusterclick', function (cluster) {
            console.log('clusterclick');

            var markers = cluster.getMarkers();

            //Get all the titles
            var titles= '<div id="map_wrapper"><div id="map_scroller"><ul id="map_list">';

            for (var i = 0; i < markers.length; i++) {
                titles += markers[i].labelContent + "\n";
            }

            titles += '</div></div></ul>';


            infowindow.setContent(titles);
            infowindow.setPosition(cluster.getCenter());
            infowindow.open(map);

            console.log($('#map_wrapper').html());
            myScroll = new iScroll('map_wrapper', {
              snap: true,
              momentum: false,
              hScrollbar: true,
              onScrollEnd: function () {
                // document.querySelector('#indicator > li.active').className = '';
                // document.querySelector('#indicator > li:nth-child(' + (this.currPageX+1) + ')').className = 'active';
              }
            });

            // TODO: Decide if we should import timeago lib to Alasker
            // $('#map_wrapper').find('.time').timeago();

            google.maps.event.addListener(map, 'zoom_changed', function () {
                infowindow.close()
            });

          });
        }
      }
    });
    
    me.constructor = GoogleMapInfowindow;
      
    me.initialize.apply(me, arguments);
    
    return me;
  }
