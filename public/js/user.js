$(document).ready(function(){	
 
	$('#link_tab li a').click(function(){
		$('#link_tab li').removeClass('link_tab_selected');
		$(this).parent().addClass('link_tab_selected');
		var currentTab = $(this).attr('href');
		$('.link_tab_content').hide();
		$(currentTab).show();
		return false;
	});
	
	$('#link_tab li:first a').trigger("click");
	
	$('.arrow_up_down').click(function(){
									   
								   
		if($(this).parent().parent().next('.arrow_up_down_content').css("display") == "none"){
			
			$(this).parent().parent().next('.arrow_up_down_content').show();
			$(this).children('img').attr('src', '/css/images/arrow_down.png');

			
		}else{
			$(this).parent().parent().next('.arrow_up_down_content').hide();
		        $(this).children('img').attr('src', '/css/images/arrow.png');
			
		}
									   
		
										 
	});


        $(".meter > span").each(function() {
				$(this)
					.data("origWidth", $(this).width())
					.width(0)
					.animate({width: $(this).data("origWidth")}, 0);
	});
});