(function( $ ){
    var $el, marker, circle, map, address, lat, lng;
    var	radius = 1609.34/4; // quarter of a mile
    var params = {};


    var methods = {
        init: function( options ){
            $el =  this[0];

            params = $.extend({ show_location:1 }, options);

            if ( options && options.lat && options.lng ) {
                initialize({lat:options.lat, lng:options.lng, radius:options.radius || radius });
            }else{
                navigator.geolocation.getCurrentPosition(success);
            }
        },
        radius: function(new_radius){
            setRadius(new_radius);
        },
        address: function(new_address){
            setAddress(new_address);
        },
        location: function(new_lat, new_lng){
            setLocation(new_lat, new_lng);
        },
        attributes: function(){
            return getAttributes();
        }
    }

    $.fn.mapper = function( method )
    {
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.mapper' );
        }
    };

    function success( p ){
        initialize({lat:p.coords.latitude, lng:p.coords.longitude, accuracy:p.coords.accuracy });
    }

    function setRadius(new_radius)
    {
        radius = new_radius;
        circle.setRadius(new_radius);
        marker.setTitle("You are here! (at least within a "+radius+" meter radius)");

        $($el).trigger('change');
    }

    function setLocation(new_lat, new_lng){
        var latlng = new google.maps.LatLng(new_lat, new_lng);

        geo({ location:latlng }, function(locs){
            lat = locs[0].lat;
            lng = locs[0].lng;
            address = locs[0].address;

            $($el).trigger('change');
        });
    }

    function setAddress(new_address)
    {
        address = new_address;

        geo({address:new_address}, function(locs){
            lat = locs[0].lat;
            lng = locs[0].lng;
            address =  locs[0].address;

            var latlng = new google.maps.LatLng(lat, lng);

            map.setOptions({ center : latlng });
            marker.setPosition(latlng);

            $($el).trigger('change');
        });
    }

    function getAttributes()
    {
        return {
            'address' : address,
            'lat'     : lat,
            'lng'     : lng,
            'radius'  : radius
        };
    }

    function plot()
    {
        var bounds = new google.maps.LatLngBounds();

        $.each(params.data, function(index, value) {
            var p = params.data[index];
            var latLng = new google.maps.LatLng(p.lat, p.lng);

            /*var marker = new google.maps.Marker({
                position: latLng,
                map: map
            });*/

            var bubble = new  bubbleOverlay(p);

           bounds.extend(latLng);
        });

        //map.setCenter(bounds.getCenter());
        map.fitBounds(bounds);
    }

    function bubbleOverlay( options )
    {
        this.options = options;

        this.init();
    }
    bubbleOverlay.prototype = {
        init: function(){
            var div = document.createElement('div');
            div.style.border = "none";
            div.style.borderWidth = "0px";
            div.style.position = "absolute";

            this.el = div;

            var template = _.template([
                '<div>',
                    '<div class="avatar"><img alt="" height="50" src="<%= avatar %>" width="50"></div>',
                    '<div class="info">',
                        '<div class="content"><%= content %></div>',
                        '<ul>',
                        '<li>When:<span><%= when %></span></li>',
                        '<li>Category:<span><%= category %></span></li>',
                        '</ul>',
                    '</div>',
                '</div>'
            ].join(''));

            $(this.el).append(template(this.options));

            this.setMap(map);
        },

        draw: function(){
            var latLng =  new google.maps.LatLng(this.options.lat, this.options.lng);
            var projection = this.getProjection();
            var pos = projection.fromLatLngToDivPixel(latLng);

            var div = this.el;
            div.className = "map_bubble" ;
            div.style.width = '260px';
            div.style.height = '70px';

            div.style.left = (pos.x-220)+'px';
            div.style.top = (pos.y-112)+'px';
        },
        show: function(){
            this.draw();
        },
        onAdd: function(){
            var pane = this.getPanes().floatPane;
            pane.appendChild(this.el);
        }
    }
    _.extend(bubbleOverlay.prototype, new google.maps.OverlayView());

    function initialize( loc )
    {
        var latlng = new google.maps.LatLng(loc.lat, loc.lng);

        var self = this;

        radius = loc.radius || radius;

        geo({ location:latlng }, function(locs){
            lat = locs[0].lat;
            lng = locs[0].lng;
            address = locs[0].address;

            var latlng = new google.maps.LatLng(lat, lng);

            map = new google.maps.Map($el, {
                center: latlng,
                zoom:15,
                disableDefaultUI : false,
                scrollwheel: true,
                disableDoubleClickZoom: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            if ( params.show_location ) {

                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    draggable: true,
                    title:"You are here! (at least within a "+radius+" meter radius)"
                });

                google.maps.event.addListener(marker, 'dragend', function(e) {
                    var latlng = marker.getPosition();
                    lat = latlng.lat();
                    lng = latlng.lng();
                    $($el).trigger('change_marker');
                });

                circle = new google.maps.Circle({
                    map: map,
                    radius: radius,
                    fillColor: '#3473D9',
                    strokeOpacity: .5,
                    strokeColor: '#3473D9',
                    strokeWeight: 1
                });
                circle.bindTo('center', marker, 'position');

                $($el).trigger('change');

            }

            if ( params.data ) {
                plot();
            }
        });
    }

    function geo(request, callback)
    {
        var locs = [];

        if(request){
            // send a request to geocoder for the given address
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode( request, function(results, status)
            {
                console.log("Geocoder status:"+status);
                if (status == google.maps.GeocoderStatus.OK) {

                    // process results
                    if( results[0] ){
                        $.each(results, function(i,r){
                            var location = {
                                shortName   : r.address_components[0]['short_name'],
                                address     : r.formatted_address,
                                lat         : r.geometry.location.lat(),
                                lng         : r.geometry.location.lng(),
                                viewport    : r.geometry.viewport,
                                bounds      : r.geometry.bounds
                            }
                            locs.push(location);
                        });
                        callback(locs);
                    }
                }
            });
        }
    }
})( jQuery );
