 $(document).ready(function(){
	initThumbRating();
 });


function disableThumbs(){
	  
  //var selector = $("#"+event.target.id + " input[name='thumbsdown']");
  
  /*  
  $("input[name='thumbsup']").addClass("vote_up_disabled");
  $("input[name='thumbsup']").removeClass("vote_up");
  $("input[name='thumbsup']").removeClass("vote_up_hover");
  $("input[name='thumbsup']").unbind('click');
  $("input[name='thumbsup']").unbind('mouseover');
  $("input[name='thumbsup']").unbind('mouseout');
   
  $("input[name='thumbsdown']").addClass("vote_down_disabled");
  $("input[name='thumbsdown']").removeClass("vote_down");
  $("input[name='thumbsdown']").removeClass("vote_down_hover");
  $("input[name='thumbsdown']").unbind('click');
  $("input[name='thumbsdown']").filter("#"+event.target.id).unbind('mouseover');
  $("input[name='thumbsdown']").filter("#"+event.target.id).unbind('mouseout');*/
}

function initThumbRating(){
	$("input[name='thumbsup']").click(function(event){
		if (!$(this).hasClass("vote_up_disabled")) {
                       
			$(".votes_balance_" + event.target.id).load("/"+ event.target.alt +"/" + event.target.id+"/vote_up",disableThumbs);
		}
	});
	
	$("input[name='thumbsdown']").click(function(event){
		if (!$(this).hasClass("vote_down_disabled")) {
			$(".votes_balance_" + event.target.id).load("/"+ event.target.alt +"/" + event.target.id+"/vote_down",disableThumbs);
		}
	});
	
	$("input[name='thumbsup']").mouseover(function(){
		if (!$(this).hasClass("vote_up_disabled")) {
			$(this).removeClass("vote_up");
			$(this).addClass("vote_up_hover");
		}
	});
	
	$("input[name='thumbsup']").mouseout(function(){
		if (!$(this).hasClass("vote_up_disabled")) {		
			$(this).removeClass("vote_up_hover");
			$(this).addClass("vote_up");
		}
	});
	
	$("input[name='thumbsdown']").mouseover(function(){
		if (!$(this).hasClass("vote_down_disabled")) {
			$(this).removeClass("vote_down");
			$(this).addClass("vote_down_hover");
		}
	});
	
	$("input[name='thumbsdown']").mouseout(function(){
		if (!$(this).hasClass("vote_down_disabled")) {
			$(this).removeClass("vote_down_hover");
			$(this).addClass("vote_down");
		}
	});
}
