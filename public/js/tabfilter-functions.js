$(function() {  	  	
	function resort(sorttype)
	{
		var selected = $('#tabs').tabs().tabs('option', 'selected');
		$('#fragment-1').load('/plans/listby?newlayout=1&paginate=1&sort=' + sorttype + '&by=' + selected + "&searchstring=<%= @escaped_searchstring?  @escaped_searchstring  : '' %>");
	}
	
	
	$('#tabs').ajaxSend(function(e, xhr, settings) {
	    window.currentTabUrl = settings.url;
	});  	
	$('#tabs').tabs({
	    load: function(event, ui) {
		    $('#posts li .post').live('hover', function() {
		    	$(this).css({background: 'url(/css/images/post-hover.png) repeat-x 0 0'}).find('.button').show();
		    	$(this).find('.social-links').show();
		    });
	   }
	});    
});
