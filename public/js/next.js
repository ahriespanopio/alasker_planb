$(document).ready(function() {

  var currentTextAreaValue = $("textarea#plan_content").val();
    var refreshId = setInterval(function() {
    	var newTestAreaValue =  $("textarea#plan_content").val();   				
    	if (currentTextAreaValue != newTestAreaValue) {    		
   			$('#similarlist').load("/plans/listplansby?situationsearch=+"+ escape(newTestAreaValue));
    	}
    	currentTextAreaValue = newTestAreaValue;
	}, 6000);	
 	$.ajaxSetup({cache: false});

 	var situationContent = $("textarea#situation_content").val();
 	/*
 	$('#situationcondition_what').load("/situations/getWhatKeywords?searchstring=+"+ escape(situationContent), function(response, status, xhr) {
		if (status != "error")
		{
			var currentSituationConditionWhatValue = $("input#situationcondition_what").val();
			$('#situationcondition_what').val(combineStrings(response,currentSituationConditionWhatValue));		                        
        }
        $('#situationcondition_what').tagit();
 	});*/
        
    /* 
 	$('#situationcondition_whenkeyword').load("/situations/getWhenKeywords?searchstring=+"+ escape(situationContent), function(response, status, xhr) {
		if (status != "error")
		{
			var currentSituationConditionWhenValue = $("input#situationcondition_whenkeyword").val();
			$('#situationcondition_whenkeyword').val(response + currentSituationConditionWhenValue);		
		}

 	});*/
 	
 	$('#where_are_you').load("/situations/getWhereKeywords?searchstring=+"+ escape(situationContent), function(response, status, xhr) { 		
 	    var availableTags = "";
	    if (status != "error")
		{
			var currentSituationConditionWhereValue = $("input#where_are_you").val();
			availableTags = response;
		}
	    else
	    {
	    	response = "";
	    }
	    		
	    if(google.loader.ClientLocation) {
		      visitor_city = google.loader.ClientLocation.address.city;
		      visitor_region = google.loader.ClientLocation.address.region;
		      visitor_country = google.loader.ClientLocation.address.country;
		      visitor_countrycode = google.loader.ClientLocation.address.country_code;

	  			if(visitor_city == "") {
					visitor_city = "";
				} else {
					visitor_city = visitor_city + ', ';
				}
	
				if(visitor_region == "") {
					visitor_region = "";
				} else {
					visitor_region = visitor_region + ', ';
				}
				
				if (availableTags == "") {
					availableTags = "";
				} else {
					//availableTags = availableTags + ", ";
				}
									
	           availableTags = [availableTags, visitor_city  + visitor_region + visitor_country ];
	           $( "#where_are_you" ).autocomplete({ source: availableTags });
		 }
	    
		if ( currentSituationConditionWhereValue == "")
		{
			$('#where_are_you').val(response);
		}

		
		/*
		 * removed as confusing to tag 
        $('#situationcondition_where').tagit();
        */
 	});
});

function isEmptyString(strValue)
{
	if (strValue && strValue == '')
	{
		return true;
	}
	return false;
}

function combineStrings(response, currentValue)
{
	if (!isEmptyString(currentValue) && !isEmptyString(response))
	{
		return currentValue + ', ' + response;
	}

	return currentValue + response;
}
   

