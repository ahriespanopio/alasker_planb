countDownTimer(); // call now to update immediately  
// Start the timer 
intervalId = setInterval ( "countDownTimer()", 500 );

function countDownTimer()
{
  var expirations = document.querySelectorAll(".expirationText");
  var countdowns = document.querySelectorAll(".countDown");
  for (var i=0, max=countdowns.length; i < max; i++)
  {
    var expiredTime = countdowns[i].getAttribute("value");
    var cls = countdowns[i].parentNode.getAttribute("class");

    nowTime = expiredTime - new Date().getTime()/1000;
    
    // If parent class is "timer" use a clock countdown style
    if (cls && cls.indexOf("timer") != -1)
    {
    	if (nowTime > 0)
    	{
		    var seconds = Math.floor(nowTime % 60);
		    var minutes = Math.floor(nowTime / 60) % 60;
		    var hours = Math.floor(nowTime / 3600) % 24;
		    var days = Math.floor(nowTime/(24*3600));
	
		    var s = "";
		    if (days > 0)
		      s += days + ":";
		    if (hours > 0)
		      s += (hours < 10 ? "0" : "") + hours + ":";
	
	        s += (minutes < 10 ? "0" : "") + minutes + ":";
	        s += (seconds < 10 ? "0" : "") + seconds;
		
		    countdowns[i].innerHTML = s;
    	}
    	else
    	{
    		countdowns[i].innerHTML = "";
    		expirations[i].innerHTML = "";
    	}
    }
    else // otherwise use natural language
    {	
		if (nowTime > 0)
		{
		  if (nowTime < 24*3600)
		  {
		    var seconds = Math.floor(nowTime % 60);
		    var minutes = Math.floor(nowTime / 60) % 60;
		    var hours = Math.floor(nowTime / 3600);
		    var s = "";
		    if (hours > 0)
		      s += hours + " hour" + (hours==1 ? " " : "s ");
		    if (hours > 0 || minutes > 0)
		      s += minutes + " minute" + (minutes==1 ? " " : "s ");
		    if (hours == 0)
		      s += seconds + " second" + (seconds==1 ? " " : "s ");
		
		    countdowns[i].innerHTML = s;
		    if (hours == 0 && minutes == 0)
		      countdowns[i].style.color = "red";  
		  }
		  else
		  {
		    var days = Math.floor(nowTime/(24*3600));
		    countdowns[i].innerHTML = days + " day" + (days==1 ? "" : "s");
		  }
		}
		else
		{
		  countdowns[i].innerHTML = "";
		  expirations[i].innerHTML = "";
		  //expirations[i].style.color = "red";    
		}
    }
  }
}
