$(function() {
	
	var temptitle;
	$('.tabtooltipfix').hover( 
	   function() {
	      // remove the title		  
	      temptitle = $(this).attr('realtitle');	      
	      $(this).attr('title',temptitle);
	   },
	   function() {
	      // replace the title
	      $(this).attr('title','fragment-1');
	   }
	); 	    	
		    
	
	$('#tabs').ajaxSend(function(e, xhr, settings) {

	    window.currentTabUrl = settings.url;
          
	});  	
	$('#tabs').tabs({
	    load: function(event, ui) {
	    	/*
	    	$('#posts li .post').hover(function(){
		    	$(this).css({background: 'url(/css/images/post-hover.png) repeat-x 0 0'}).find('.button').show();
		    	$(this).find('.social-links').show();
		    },function(){
		    	$(this).css({background: 'transparent'}).find('.button').hide();
		    	$(this).find('.social-links').hide();
		    });
		    */
                 
		    
	    	/*  moved to functions.js
		    $('.posts li .post').live('mouseenter', function() {
		    	$(this).addClass("post-gradient");
		    	$(this).find('.button').show();
		    	$(this).find('.social-links').show();
		    });
		    
		    $('.posts li .post').live('mouseleave', function() {
		    	$(this).removeClass("post-gradient").find('.button').hide();
		    	$(this).find('.button').hide();
		    	$(this).find('.social-links').hide();
		    });
		    */		
		   $(".pagination a").live('click', function() {		   	
		       $('#fragment-1').load(this.href);
		       return false;
		    });	
		    var refreshId = setInterval(function() {		    	
      		   $('#fragment-1').load(window.currentTabUrl);
      		   //alert(window.currentTabUrl);
   			}, 60000);
   			$.ajaxSetup({cache: false});
	    }
	});    
});
