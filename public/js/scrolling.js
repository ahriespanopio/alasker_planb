        $(document).ready(function() {
            //initThumbRating();

            (function(){
                //var scroller = $('#scrollbar1').tinyscrollbar();

                var page = 1;

                var fetch = function(){
                    page++;

                    clearInterval( interval );

                    $.ajax({
                        url: "/?page="+page,
                        dataType: "html",
                        async:true,
                        type:'GET',
                        data: null,
                        success: function(data, textStatus, jqXHR){
                            console.log('success');
                            update(data);
                        },
                        error: function(p1,p2,p3){
                            console.log('error');
                        }
                    });
                };

                var isInView = function(id) {
                    var st = (document.documentElement.scrollTop || document.body.scrollTop),
                        ot = $(id).offset().top,
                        wh = (window.innerHeight && window.innerHeight < $(window).height()) ? window.innerHeight : $(window).height();
                    return ot > st && ($(id).height() + ot) < (st + wh);
                };

                var checkScroll = function(){
                    if ( isInView("#load_more") ){
                        console.log('fetching...');

                        fetch();
                    }
                };

                var update = function(data){
                    data = $.trim(data);
                    if (data) {
                        $("#posts_container").append(data);
                        //scroller.tinyscrollbar_update('relative');
                        interval = setInterval( checkScroll, 100 );
                    }
                };

                var interval = setInterval( checkScroll, 100 );

            })();

        });
