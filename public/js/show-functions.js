$(document).ready(function() {	
  /*<% if !params[:page].nil? || !params[:commit].nil? %>*/
  	/* makeAMatchPlan(true); */
  /*<% end %>*/
  
  var intervalId = 0;

  //$(".plan").css("display", "none");
  $(".plan").dialog({title: "Respond to Question",modal: true,  resizable: false, autoOpen: false, 
  	minWidth : 470, minHeight : 300,  overlay: {opacity: 0.8, 
  	background: 'black'},close: function() { $(".errormsg").hide(); }});

	/* .css("display", "none");*/
  $(".suggestedplans").dialog({title: "Search for Answers",modal: true,  resizable: false, autoOpen: false, 
  	minWidth : 500, minHeight : 450, overlay: {opacity: 0.8, background: 'black'}});
  
  $('#plancondition_what').tagit();
  
  $("#plancondition_whendate").datepicker({
		showOn: 'focus', buttonText: "Select", minDate: "0", defaultDate: "1", 
		dateFormat: "MM d, yy"
	  });

  $(".ui-datepicker-trigger").append("<span></span>");
  
   
/*** not needed
 *
 * 
  var words = $(".situation_text").text().split(" ");
  var text = words.join("</span> <span>");
  $(".situation_text").html("<span>" + text + "</span>");

  $(".situation_text span").click(function () {
	  	if ($(this).css("background-color") == "rgb(255, 255, 0)") {
	  		$(this).css("background-color", "white");
	  	} else {
	  		$(this).css("background-color", "yellow");
	        $("#plan_planB_keywords").val($("#plan_planB_keywords").val() + " " + $(this).text());          	
	  	}  	
	});
  
  $(".situation_text span").hover(function () {
      $(this).css({'font-style' : 'italic'});
        }, function () {
          $(this).css({'font-style' : 'normal'});
  });
 ***/ 
 
	   
	$(".link-a-plan a").click(function (e) {
		$(".link-a-plan form").submit();
		e.preventDefault();	
	});	 		 

	$(".link-a-plan form").submit(function (e) {
		var searchField  = $('#search_field').val();		
		$('#planlistupdate').load("/plans/listplansby?newlayout=1&situationsearch="+
			encodeURIComponent(searchField), function(r, s, x) {
			$(this).html(r);
		    $('#planlistupdate li .post').live('mouseenter', function() {
		    	$(this).addClass("post-gradient");
		    	$(this).find('.button').show();
		    });
		    
		    $('#planlistupdate li .post').live('mouseleave', function() {
		    	$(this).removeClass("post-gradient").find('.button').hide();
		    	$(this).find('.button').hide();
		    });
		});
		return false;	
	});	 		 
	
    $(".suggestedplans .situations_normal a").live('click', function (e) {
		var hiddenElement = $(this).attr("internal_id");
		e.preventDefault();
		
		$("#plan_b_plan_id").val(hiddenElement);
		$("#plan_b_keywords").val($("#plan_planB_keywords").val());
		$("#new_plan_b").submit();
    });       
});

function addAPlan()
{
	$(".plan").dialog("open");
}

function makeAMatchPlan(forceDisplay)
{
  $(".suggestedplans").dialog("open");
}

(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s);js.id = id;
	js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=154728984618224";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/*** FB is not defined
FB.init({ 
	appId:'154728984618224', cookie:true, 
	status:true, xfbml:true, oauth:true
});

***/

function postFB()
{
	FB.api('/me/alasker:post' + 
			'?situation=<%= request.url %>','post',
			function(response) {
		if (!response || response.error) {
			alert('Error occured');
		} else {
			alert('Post was successful! Action ID: ' + response.id);
		}
	});
}

function tweetwindowopen(url,width,height){
    var left=parseInt((screen.availWidth/2)-(width/2));
    var top=parseInt((screen.availHeight/2)-(height/2));
    var prop="width="+width+",height="+height+",left="+left+",top="+
    top+",screenX="+left+",screenY="+top;window.open(url,'tweetwindow',prop);
}

function popitup(url) {
	newwindow=window.open(url,'','height=500,width=650');
	if (window.focus) {newwindow.focus()}
	return false;
}