 /*$(document).ready(function(){
   updateCharCount();
 });
   */
 
function updateCharCount(target)
{
  var $el = $(target);
  var inputLength = $el.val().length;
  var charLeft = $el.attr("maxlength") - inputLength;
  if (inputLength == 0)
    $("#char_count").text("");
  else
    $("#char_count").text(charLeft + " characters left");
};

$(".my-textarea, .field").keyup(function(){
	 //var str = $("#situation_content").val();
     //str = str.replace(/(^\s+)|(\s+$)/g,' ');
     //$("#situation_content").val(str);
     updateCharCount($(this));
  });

 $(".my-textarea, .field").blur(function(){
    // remove leading and trailing blanks
    var $el = $(this);
    var str = $el.val();
    str = str.replace(/(^\s+)|(\s+$)/g,'');
    $el.val(str);
    updateCharCount($el);
 });

