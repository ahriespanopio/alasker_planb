$(document).ready(function() {
   	$("#situationcondition_whendate").datepicker({
      showOn: 'focus', buttonText: "Select", minDate: "0", defaultDate: "1", dateFormat: "MM d, yy "+ date_obj_time
    });

    $(".ui-datepicker-trigger").append("<span></span>");

    $("#sign_up_and_finish").click(function(){
      $('input[name=redirect]').val('/users/sign_in');
      $(this).closest('form').submit();
    });

    $("#where_map").mapper();

    $("#where_map").bind('change', function(){
        console.log('where_map change');
        var attr = $("#where_map").mapper('attributes');

        $("#situation_location_lat").val(attr.lat);
        $("#situation_location_lng").val(attr.lng);
        $("#situation_location_radius").val(attr.radius);
        $("#where_are_you").val(attr.address);
    })

    $("#where_are_you").change(function(){
        $('#map_canvas').mapper('address', $(this).val());
    });

    $("#where_map").bind('change_marker', function(){
        var attr = $("#where_map").mapper('attributes');

        $("#where_map").mapper('location', attr.lat, attr.lng);
    })

});


