var Message = {
  dialog_message : function (id) {
            
    $.get('me/dialog', { id: id }, function(data){
      var butts = [      
	      { text: $.i18n._('close'),
	        click: function() {
	        $( this ).remove();
	      	}
	      },
	        
	      { text: $.i18n._('send'),
	        click: function() {
	        $.post('/me',{
	        	message: 
		          { subject: $('#subject').val(),
		            body: $('#body').val(), 
		            user_id: $('#user').val()
		          }
		        });

	        $(this).remove();
	      	}
	      }
      ];
        
      $($(data)).dialog({
        autoOpen: true,
        height: 325,
        width: 400,
        modal: true,
        buttons: butts,
      });

    });

  } //end dialog_message
} //end message
