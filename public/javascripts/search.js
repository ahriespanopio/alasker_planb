$(function() {
  $("#filter_c").tokenInput("/categories.json", {
    crossDomain: false,
    prePopulate: $("#filter_c").data("pre"),
    theme: "facebook"
  });
});

$(function() {
  $("#filter_u").tokenInput("/users.json", {
    crossDomain: false,
    prePopulate: $("#filter_u").data("pre"),
    theme: "facebook"
  });
});