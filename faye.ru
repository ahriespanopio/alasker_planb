require 'rubygems'
require 'bundler'
Bundler.require
require 'faye'

require File.expand_path('../config/initializers/faye_token.rb', __FILE__)

class ServerAuth
  def incoming(message, callback)
    if message['channel'] !~ %r{^/meta/}
      if message['ext']['auth_token'] != FAYE_TOKEN
        message['error'] = 'Invalid authentication token'
      end
    end
    callback.call(message)
  end
end

faye_server = Faye::RackAdapter.new(
      :mount => '/faye', 
      :timeout => 25,
      :engine => {
        :type => 'redis',
        :host => 'dory.redistogo.com',
        :port => '9565',
        :password => 'ef548f8528c0c9206955dd22bb6b52c2',
        :database => 1
      })
faye_server.add_extension(ServerAuth.new)
run faye_server
