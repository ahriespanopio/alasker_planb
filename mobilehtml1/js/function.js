
$(function(){
	 		var windowHeight = $(window).height();
			var headerHeight = $('#header').height();
			var slidebuttonHeight = $('#header').height();
			var slider = $( "#slide_content" );
		
			$( "#slide_button" ).click(
				function( event ){
				
					event.preventDefault();
 					slider.css('height',windowHeight - headerHeight - slidebuttonHeight +"px");
					if (slider.is( ":visible" )){
						slider.slideUp( 1000 );
 					} else {
 						slider.slideDown( 1000 );
 
					}
				}
			);
			
			
	$('input:text, textarea').each(function(){
									  
	 $(this).attr('title', $(this).val());					  
	})

	$('input:text,textarea').focus(function() {
								   
								   
	  if ($(this).val() == $(this).attr('title')) { 
	  	$(this).val('');

	   $(this).css('color','#656565');
	  } 
	 
	});
	
	
	$('input:text,textarea').blur(function() {
			  
		if ($(this).val() == '') 
		{
			 $(this).css('color','#e7e6e7');
			$(this).val($(this).attr('title'));
		}
	
 	})
	
	$(".delete_input").live("click", function() {  
					
				
			 	$(this).next('input').val('');
				$(this).next('input').css('color','#656565');	
				$(this).next('input').focus();
		});	
	
	
	$(".questions .post").live("click", function() {  
						
			if($('.popup_post').length > 0){
					var $name = $(this).find('.user_name').text();
					var $pic = $(this).find('.user_img').html();
					var $time = $(this).find('.time').html();
					var $question = $(this).find('.user_question').text();
				
					$('.popup_post').addClass(this.id);
					
					$("."+ this.id).addClass('popup_open');
					$("."+ this.id).find('.user_name').html($name);
					$("."+ this.id).find('.user_img').html($pic);
					$("."+ this.id).find('.time').html($time);
					$("."+ this.id).find('.post_popup_content h3').html($question);
					$("."+ this.id).find('.time img').attr("src", "images/time1.png");
					
					$("."+ this.id).dialog({
						height: 840,
						width:550,
						modal: true
					});	
					$(".ui-widget-overlay").addClass('popup_post_overlay');
			}
		});	
				
				
		$(".popup_post_overlay").live("click", function() {  
			$(".popup_open").dialog("close");	
			$(".ui-widget-overlay").removeClass('popup_post_overlay');
			$(".popup_post").removeClass('popup_open');
		});	
		
		
		$(".vote").live("click", function() {  
										  
		  $( ".vote_popup" ).dialog({
					height: 340,
					width:600,
					modal: true
					
				});
		  $(".ui-widget-overlay").addClass('vote_overlay');
		  
		 });
		
		$(".vote_overlay").live("click", function() {  
		
			$(".vote_popup").dialog("close");
			$(".ui-widget-overlay").removeClass('vote_overlay');
			
		 });
		
		$("#setting_link").live("click", function() {  
										  
	/*	  $(this).addClass("selected");*/
		  $( "#settings-popup" ).dialog({
					height: 840,
					width:500,
					modal: true
					
				});
		  
		   $('.on_off :checkbox').iphoneStyle();
			
			$(".ui-widget-overlay").addClass('settings_overlay');
		 });
			 
			
		$(".settings_overlay").live("click", function() {  
			$("#setting_link").removeClass("selected");
			$("#settings-popup").dialog("close");
			$(".ui-widget-overlay").removeClass('settings_overlay');
			
		 });
			 
		
		$(".popup_ul li a").toggle(function(){
											

				$(this).addClass('arrow_down');

		}, function () {

				$(this).removeClass('arrow_down');

		});
		
		$(".popup_ul li a").click(function() {
				$(this).next('.fields').slideToggle('fast');
						return false;
			});
		   
	
 })
	