# note_spec.rb

require 'spec_helper'

describe Note do
  let(:note) { FactoryGirl.build(:note) }

  context 'when new' do
    it { should be_an_instance_of(Note) }

    it { should ensure_length_of(:content).is_at_most(140) }

  end

  context 'when viewed' do
    pending
  end
  
  context 'when deleted' do
    pending
  end
end