# plan_spec.rb

require 'spec_helper'

describe Plan do
  let(:plan) { Plan.new }

  context 'when new' do
    it 'creates new plan' do
      plan.should be_an_instance_of(Plan)
    end

    it 'belongs to user, situation, category, plancondition, stat' do
      should belong_to(:user)
      should belong_to(:situation).class_name(Situation)
      should belong_to(:category)
      should belong_to(:plancondition).class_name(PlanCondition)
      should belong_to(:stat)
    end

    it 'has many planbs, situations' do
      should have_many(:planbs).class_name(PlanB)
      should have_many(:situations).through(:planbs)
    end

    it 'validates content' do
      should validate_presence_of(:content)
      should ensure_length_of(:content).is_at_least(5)
      should validate_uniqueness_of(:content).scoped_to(:id)
    end

  end # end of context 'when new'

end

