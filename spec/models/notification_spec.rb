# notification_spec.rb

require 'spec_helper'

describe Notification do
  let(:notification) { FactoryGirl.build(:notification) }

  context 'when new' do
    it { should be_an_instance_of(Notification) }
    it { should validate_presence_of(:recipient_id) }
    it { should validate_presence_of(:notification_type) }
    it { should validate_presence_of(:event) }
  end

  context 'when viewed' do
    pending
  end
  
  context 'when deleted' do
    pending
  end
  
end
