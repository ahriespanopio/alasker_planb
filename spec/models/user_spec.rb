# user_spec.rb

require 'spec_helper'

describe User do
  let(:user) { FactoryGirl.build(:valid_user) }
  let(:other_user) { FactoryGirl.build(:valid_user2); other_user.save!}
    # User.create!(:username => 'myusername', :email => 'myemail@example.com',
    #                         :password => 'mypassword', :password_confirmation => 'mypassword') }

  context 'when new' do
    it 'returns a new user' do
      should be_an_instance_of(User)
    end

    it 'takes four to six parameters and return a user' do
      lambda {User.new(:username, :email, :password, :password_confirmation, :about, :admin_enabled)}.should raise_exception ArgumentError
    end

    it 'validates email format' do 
      user.email.should match(/^([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})$/i)
    end

    it 'validates username format' do
      uname = user.username
      uname.gsub!(/\./, "")
      uname.should match(/^[a-zA-Z0-9]*([_]?[a-zA-Z0-9])*$/i)
    end

    it { should ensure_length_of(:username).is_at_least(5).is_at_most(15) }
    it { should ensure_length_of(:password).is_at_least(6).is_at_most(20) }

    it 'ensures password confirmation is equal to password' do
      user.password_confirmation.should eql(user.password)
    end

    it 'validates format of website' do
      user.website.should match(/^(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?$/ix)
    end

    it { should ensure_length_of(:location).is_at_most(50) }

    it {should have_many(:plans)}
    it {should have_many(:situations)}
    it {should have_many(:authentications)}
  end

  it { should respond_to(:relationships) }
  it { should respond_to(:followed_users) }
  it { should respond_to(:reverse_relationships) }
  it { should respond_to(:followers) }
  it { should respond_to(:following?) }
  it { should respond_to(:follow!) }
  it { should respond_to(:unfollow!) }

  context "follows another user" do
    before { user.follow!(other_user) }

    it { should be_following(other_user) }
    its(:followed_users) { should include(other_user) }
  end
  
  context "is followed by others" do
    before { other_user.follow!(user) }
    subject { other_user }

    its(:followers) { should include(other_user) }
    it { should_not be_following(user) }
  end

  context "does not follow anyone" do
    before { user.unfollow!(other_user) }

    it { should_not be_following(other_user) }
    its(:followed_users) { should_not include(other_user) }
  end
  
end

