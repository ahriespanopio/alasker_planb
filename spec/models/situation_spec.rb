# situation_spec.rb

require 'spec_helper'

describe Situation do
  let(:situation) { Situation.new }

  context 'when new' do
    it 'creates new situation' do
      situation.should be_an_instance_of(Situation)
    end

    it 'belongs to user, category, stat and plan' do
      should belong_to(:user)
      should belong_to(:category)
      should belong_to(:stat)
      should belong_to(:plan)
      should belong_to(:situationcondition).class_name('SituationCondition')
    end

    it { should have_many(:planbs).class_name('PlanB') }

    it { should have_many(:plans).through(:planbs)}

    it 'validates content' do
      should validate_presence_of(:content)
      ensure_length_of(:content).is_at_least(5)
      should validate_uniqueness_of(:content)
    end

  end # end of context 'when new'

end

