# category_specs.rb

require 'spec_helper'

describe Category do
  let(:category) { Category.create!(:name => 'Books', :language => 'English') }

  context 'when new' do
    it 'returns a new category' do
      category.should be_an_instance_of(Category)
    end

    it 'validates uniqueness of name' do
      category.should validate_uniqueness_of(:name).scoped_to(:category_id)
      should ensure_length_of(:name).is_at_least(3).is_at_most(20)
    end

    it 'has many subcategories, situations' do
      should have_many(:subcategories)
      should have_many(:situations)
    end

    #it 'belongs to a main category' do
    #  should belong_to(:maincategory).class_name('Category')
    #end

  end # end of context 'when new'

end

