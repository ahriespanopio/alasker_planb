require "spec_helper"

describe "user sign in" do
  it "allows new users to register with username, email and password" do
    visit "/users/sign_in"

    fill_in "Username",							 :with => "shichichloe"
    fill_in "Email",                 :with => "shichichloe@yahoo.com"
    fill_in "Password",              :with => "shichichloe"
    fill_in "Password confirmation", :with => "shichichloe"

    click_button "Sign in"

    page.should have_content("You are successfully signed in.")
  end
end