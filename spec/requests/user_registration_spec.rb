require "spec_helper"

describe "user registration" do
  it "allows new users to register with username, email and password" do
    visit "/users/sign_up"

    fill_in "Username",							 :with => "shichichloe"
    fill_in "Email",                 :with => "shichichloe@yahoo.com"
    fill_in "Password",              :with => "shichichloe"
    fill_in "Password confirmation", :with => "shichichloe"

    click_button "Create My PlanB Account"

    page.should have_content("Please confirm your account.")
  end
end