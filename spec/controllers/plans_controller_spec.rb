# plans_controller_spec.rb

require 'spec_helper'

describe PlansController do

  describe "GET index" do
    it "assigns all plans as @plans" do
      pending
      get :index

    end
  end # GET index

  describe "GET show" do
    it "assigns the requested plans as @plan" do
      pending
      get :index

    end
  end # GET show

  describe "GET new" do
    it "assigns a new plan as @plan" do
      pending
      get :new

    end
  end # GET new

  describe "GET edit" do
    it "assigns the requested plan as @plan" do
      pending
      get :edit, :id => "1"

    end
  end # GET edit

  describe "POST create" do

    describe "with valid params" do
      it "assigns a newly created plan as @plan" do
        pending
        post :create

      end

      it "redirects to the created plan" do
        pending
        post :create

      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved plan as @plan" do
        pending
        post :create

      end

      it "re-renders the 'new' template" do
        pending
        post :create

      end
    end

  end # POST create

  describe "POST start" do


  end # POST start

  describe "PUT update" do

    describe "with valid params" do
      it "updates the requested plan" do

        pending
        put :update, :id => "1"
      end

      it "assigns the requested plan as @plan" do

        pending
        put :update, :id => "1"
      end

      it "redirects to the plan" do

        pending
        put :update, :id => "1"
      end
    end # PUT update

    describe "with invalid params" do
      it "assigns the plan as @plan" do

        pending
        put :update, :id => "1"
      end

      it "re-renders the 'edit' template" do

        pending
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end

  end # PUT update

  describe "DELETE destroy" do
    it "destroys the requested plan" do
      pending

      delete :destroy, :id => "1"
    end

    it "redirects to the plans list" do
      pending

      response.should redirect_to(plans_url)
    end

  end # DELETE destroy

end

