# me_controller_spec.rb

require 'spec_helper'

describe MeController do

  describe "GET index" do
    # it "assigns all users as @users" do
    #   user = User.create
    #   get :index
    #   assigns(:users).should eq([user])
    # end

    it "renders the index template" do
      get :index
      response.should render_template("index")
    end
  end # GET index

  describe "GET show" do
    it "assigns the current user as @user" do
      pending
      get :show

    end
  end # GET show

end

