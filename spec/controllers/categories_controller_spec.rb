# categories_controller_spec.rb

require 'spec_helper'

describe CategoriesController do

  describe "GET index" do
    it "assigns all categories as @" do
      pending
      get :index

    end
  end # GET index

  describe "GET show" do
    it "assigns the requested categories as @category" do
      pending
      get :index

    end
  end # GET show

  describe "GET new" do
    it "assigns a new category as @category" do
      pending
      get :new

    end
  end # GET new

  describe "GET edit" do
    it "assigns the requested category as @category" do
      pending
      get :edit, :id => "1"

    end
  end # GET edit

  describe "POST create" do

    describe "with valid params" do
      it "assigns a newly created category as @category" do
        pending
        post :create

      end

      it "redirects to the created category" do
        pending
        post :create

      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved category as @category" do
        pending
        post :create

      end

      it "re-renders the 'new' template" do
        pending
        post :create

      end
    end

  end # POST create

  describe "PUT update" do

    describe "with valid params" do
      it "updates the requested category" do

        pending
        put :update, :id => "1"
      end

      it "assigns the requested category as @category" do

        pending
        put :update, :id => "1"
      end

      it "redirects to the category" do

        pending
        put :update, :id => "1"
      end
    end # PUT update

    describe "with invalid params" do
      it "assigns the category as @category" do

        pending
        put :update, :id => "1"
      end

      it "re-renders the 'edit' template" do

        pending
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end

  end # PUT update

  describe "DELETE destroy" do
    it "destroys the requested category" do
      pending

      delete :destroy, :id => "1"
    end

    it "redirects to the categories list" do
      pending

      response.should redirect_to(categories_url)
    end

  end # DELETE destroy

end

