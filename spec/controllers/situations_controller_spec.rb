# situations_controller_spec.rb

require 'spec_helper'

describe SituationsController do
  before(:all) do
    @situation = FactoryGirl.build(:valid_situation)
  end

  after(:all) do
    @situation.delete
  end

  describe "GET index" do
    it "assigns all situations as @situations" do
      pending
      get :index

    end

    it 'renders view' do
      
    end
  end # GET index

  describe "GET show" do
    it "assigns the requested situations as @situation" do
      pending
      get :show, :id => @situation.id
    end
  end # GET show

  describe "GET new" do
    it "assigns a new situation as @situation" do
      pending
      get :new

    end
  end # GET new

  describe "GET edit" do
    it "assigns the requested situation as @situation" do
      pending
      get :edit, :id => "1"

    end
  end # GET edit

  describe "POST create" do

    describe "with valid params" do
      it "assigns a newly created situation as @situation" do
        pending
        post :create

      end

      it "redirects to the created situation" do
        pending
        post :create

      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved situation as @situation" do
        pending
        post :create

      end

      it "re-renders the 'new' template" do
        pending
        post :create

      end
    end

  end # POST create

  describe "POST start" do


  end # POST start

  describe "PUT update" do

    describe "with valid params" do
      it "updates the requested situation" do

        pending
        put :update, :id => "1"
      end

      it "assigns the requested situation as @situation" do

        pending
        put :update, :id => "1"
      end

      it "redirects to the situation" do

        pending
        put :update, :id => "1"
      end
    end # PUT update

    describe "with invalid params" do
      it "assigns the situation as @situation" do

        pending
        put :update, :id => "1"
      end

      it "re-renders the 'edit' template" do

        pending
        put :update, :id => "1"
        response.should render_template("edit")
      end
    end

  end # PUT update

  describe "DELETE destroy" do
    it "destroys the requested situation" do
      pending

      delete :destroy, :id => "1"
    end

    it "redirects to the situations list" do
      pending

      response.should redirect_to(situations_url)
    end

  end # DELETE destroy

end

