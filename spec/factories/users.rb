require 'factory_girl'

FactoryGirl.define do
  factory :valid_user, :class => 'User' do
    username  'my.account'
    email     'my@email.com'
    password  'mypassword'
    password_confirmation 'mypassword'
    website		'http://www.passport.com.ph'
    location  'internet'
    #admin     false
  end

  factory :valid_user2, :class => 'User' do
    username  'myaccount2'
    email     'my2@email.com'
    password  'mypassword2'
    password_confirmation 'mypassword2'
    website		'http://www.railsgirls.com/manila'
    location  'finland'
    #admin     false
  end
end