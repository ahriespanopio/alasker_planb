# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :filter_categories_association do
    category_id 1
    filter_id 1
  end
end
