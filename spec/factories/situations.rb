require 'factory_girl'

FactoryGirl.define do
  factory :valid_situation, :class => 'Situation' do
    category_id   4
    content       "I'm at the airport in EWR, and my flight just canceled"
    id            22
    keywords      'airport'
  end
end